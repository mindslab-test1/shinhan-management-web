import React, { useEffect, useState } from 'react'
import { icoSlash } from 'assets/images/icons'
import { ConciergeStandbyDetailInfo } from 'typings/conciergeDTO'
import { useSelector } from 'react-redux'
import PaginationTable from 'components/table/PaginationTable'
import { Link, Redirect, useHistory, useLocation } from 'react-router-dom'
import { rootState } from 'store'

type standbyListType = { name: string; imageYn: string; textRGB: string }

const ConciergeStandbyDetail = () => {
  const location = useLocation()
  const history = useHistory()
  if (location.state === undefined || location.state === null || location.state === '' || history.action === 'POP') {
    return <Redirect to="/web/manageConcierge/standby/list" />
  }

  const headOptions = ['No.', '파일 종류', '대기화면 선택', '텍스트 색상']

  const screenInfo = useSelector((state: rootState) => state.standbyScreenReducer.screenInfo)
  const screenDetailInfo = useSelector((state: rootState) => state.standbyScreenReducer.screenDetailInfo)

  const heads = (
    <tr>
      {headOptions.map((value) => (
        <th key={value}>
          <span>{value}</span>
        </th>
      ))}
    </tr>
  )

  const [standbyData, setStandbyData] = useState<ConciergeStandbyDetailInfo>()

  const bodys = standbyData?.standbyList.map((value: standbyListType, index: number) => (
    <tr key={index.toString()}>
      <td>{index + 1}</td>
      <td>{value.imageYn === 'Y' ? 'image' : 'video'}</td>
      <td className="padlr24">
        <span className="oftxt">{value.name}</span>
      </td>
      <td>
        <div className="div_text_color" style={{ background: 'rgb(' + value.textRGB + ')' }}></div>
      </td>
    </tr>
  ))

  const convertIndexToType = (index: number) => {
    switch (index) {
      case 0:
        return 'TYPE A'
      case 1:
        return 'TYPE B-1'
      case 2:
        return 'TYPE B-2'
      default:
        return 'TYPE A'
    }
  }

  useEffect(() => {
    setStandbyData(screenDetailInfo)
  }, [screenDetailInfo])

  return (
    <div className="content">
      <div className="cnt_div">
        <div id="snb">
          <p>컨시어지 관리</p>
          <img src={icoSlash} alt="/" />
          <Link
            to={{
              pathname: '/web/manageConcierge/standby/edit',
              state: { state: '/web/manageConcierge/standby/edit' },
            }}
          >
            대기화면 관리
          </Link>
        </div>

        <div className="stn_top">
          <form action="">
            <div className="cont">
              <h2 className="sta">
                대기화면 관리 <span>보기</span>
              </h2>
            </div>
          </form>
        </div>

        <div className="stn_cont">
          <div className="stn_lineBox type02">
            <div className="lineBox_cnts">
              <ul className="lineBox_cnts_ul">
                <li className="lineBox_cnts_tit">지점명</li>
                <li className="lineBox_cnts_cont">
                  <span>{screenInfo.branchName}</span>
                </li>
              </ul>
              <ul className="lineBox_cnts_ul">
                <li className="lineBox_cnts_tit">지점 번호</li>
                <li className="lineBox_cnts_cont">
                  <span>{screenInfo.branchNumber}</span>
                </li>
              </ul>
              <ul className="lineBox_cnts_ul">
                <li className="lineBox_cnts_tit">기기 번호</li>
                <li className="lineBox_cnts_cont">
                  <span>{screenInfo.deviceNumber}</span>
                </li>
              </ul>
            </div>
          </div>
          <PaginationTable
            tableClass="table_standby_detail"
            heads={heads}
            bodys={bodys}
            isScrollable={false}
            colLength={headOptions.length}
          />
        </div>
        <div className="cont_btm_box">
          <div className="cnt_checkBox">
            <p className="cb_title">화면전환 타입</p>
            {standbyData?.typeList.map((value, index) => (
              <div key={index.toString()} className="input_checkBox">
                <input
                  type="checkbox"
                  className="type02"
                  id={'type' + index.toString()}
                  name="checkType"
                  disabled
                  checked={value}
                />
                <label htmlFor={'type' + index.toString()}>
                  <span className="checkLabel"></span>
                  <span className="text">{convertIndexToType(index)}</span>
                </label>
              </div>
            ))}
          </div>
        </div>
        <div className="stn_btm">
          <div className="btm_btn_cont flex_fr">
            <div className="btm_btn_right">
              <Link
                to={{
                  pathname: '/web/manageConcierge/standby/edit',
                  state: { state: '/web/manageConcierge/standby/edit' },
                }}
                className="btn_blue btn_register"
              >
                수정하기
              </Link>
              <Link to="/web/manageConcierge/standby/list" className="btn_cancel btn_gray btn_md_close">
                목록으로
              </Link>
            </div>
          </div>
        </div>
      </div>
    </div>
  )
}
export default ConciergeStandbyDetail
