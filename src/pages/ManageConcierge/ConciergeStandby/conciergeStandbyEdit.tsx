import React, { useEffect, useState } from 'react'
import DebouncedColorPicker from 'components/debouncedColorPicker'
import { icoSlash, icoPlus, icoArrowBlackUp, icoArrowBlackDown, icoMinus } from 'assets/images/icons'
import { StandbySetInfo } from 'typings/conciergeDTO'
import PaginationTable from 'components/table/PaginationTable'
import { Link, Redirect, useHistory, useLocation } from 'react-router-dom'
import { useSelector } from 'react-redux'
import { rootState } from 'store'
import ConciergeStandbyRepository from 'repository/Concierge/conciergeStandbyRepository'
import GroupSelectBox from 'components/SelectBox/groupSelectBox'

type screenInfo = {
  deviceNumber: number
  branchNumber: number
  branchName: string
}

type standbyList = {
  imageYn: string
  name: string
  textRGB: string
}

type screenDetailInfo = {
  typeList: boolean[]
  standbyList: standbyList[]
}

type screenDataType = {
  screenInfo: screenInfo
  screenDetailInfo: screenDetailInfo
}

const convertIndexToType = (index: number) => {
  switch (index) {
    case 0:
      return 'TYPE A'
    case 1:
      return 'TYPE B-1'
    case 2:
      return 'TYPE B-2'
    default:
      return 'TYPE A'
  }
}

const convertReturnColortoRGB = (ReturnColor: { r: number; g: number; b: number; a: number }) => {
  try {
    const RGBString = 'rgba(' + ReturnColor.r + ',' + ReturnColor.g + ',' + ReturnColor.b + ',1)'
    return RGBString
  } catch (e) {
    return 'rgba(0,0,0,1)'
  }
}

const convertRGBtoReturnColor = (RGBString: string) => {
  try {
    const RGB = RGBString.split(',')
    const ReturnColor = { r: RGB[0], g: RGB[1], b: RGB[2] }
    return ReturnColor
  } catch (e) {
    return { r: 1, g: 1, b: 1 }
  }
}

const ConciergeStandbyEdit = () => {
  const location = useLocation()
  const history = useHistory()
  if (location.state === undefined || location.state === null || location.state === '' || history.action === 'POP') {
    return <Redirect to="/web/manageConcierge/standby/list" />
  }

  const headOptions = ['', 'No .', '파일 종류', '대기화면 선택', '텍스트 색상', '행 추가/삭제']
  const heads = (
    <tr>
      {headOptions.map((value) => (
        <th key={value}>
          <div>
            <span>{value}</span>
          </div>
        </th>
      ))}
    </tr>
  )

  const setStandbyVideoList = () => {
    ConciergeStandbyRepository.getStandbyFileList('N')
      .then((res) => {
        const videoOption = res.data.fileList
          .map((data: any) => {
            return data.name
          })
          .reverse()
        setVideoSelectOption(videoOption)
      })
      .catch((res) => {
        alert('대기화면 목록 가져오기 실패')
      })
  }

  const setStandbyImageList = () => {
    ConciergeStandbyRepository.getStandbyFileList('Y')
      .then((res) => {
        setImageSelectOption(
          res.data.fileList
            .map((data: any) => {
              return data.name
            })
            .reverse(),
        )
      })
      .catch((res) => {
        alert('대기화면 목록 가져오기 실패')
      })
  }

  const screen = useSelector((state: rootState) => state.standbyScreenReducer)

  const [standbyData, setStandbyData] = useState<screenDataType>({
    screenInfo: screen.screenInfo,
    screenDetailInfo: screen.screenDetailInfo,
  })

  const [colors, setColors] = useState<{ r: number; g: number; b: number; a: number }[]>(
    screen.screenDetailInfo.standbyList.map((value: standbyList) => convertRGBtoReturnColor(value.textRGB)),
  )
  const [isColorOpen, setIsColorOpen] = useState<boolean[]>(screen.screenDetailInfo.standbyList.map((_: any) => false))
  const [isMenuOpen, setIsMenuOpen] = useState<boolean[]>(screen.screenDetailInfo.standbyList.map((_: any) => false))
  const [videoSelectOption, setVideoSelectOption] = useState([])
  const [imageSelectOption, setImageSelectOption] = useState([])
  const [selectBoxIndex, setSelectBoxIndex] = useState([])

  const convertStandbyDataToSetInfo = (standbyData: screenDataType) => {
    return {
      deviceNo: standbyData.screenInfo.deviceNumber.toString(),
      branchNo: standbyData.screenInfo.branchNumber.toString(),
      typeList: standbyData.screenDetailInfo.typeList,
      standbyList: standbyData.screenDetailInfo.standbyList.map((value, index) => {
        return {
          name:
            value.imageYn === 'Y' ? imageSelectOption[selectBoxIndex[index]] : videoSelectOption[selectBoxIndex[index]],
          textR: colors[index].r,
          textG: colors[index].g,
          textB: colors[index].b,
        }
      }),
    }
  }

  const onEditButtonClick = async (standbySetInfo: StandbySetInfo) => {
    if (standbySetInfo.standbyList.length === 0) {
      alert('값이 없습니다')
    } else if (!standbySetInfo.standbyList.every((value) => value.name !== undefined)) {
      alert('설정하지 않은 파일이 있습니다')
    } else if (standbySetInfo.typeList.every((value) => !value)) {
      alert('타입을 1개 이상 선택해야 합니다')
    } else {
      await ConciergeStandbyRepository.setStandbyList(standbySetInfo)
        .then((res) => {
          if (res.data.code !== 0) {
            alert(res.data.message)
          }
          history.replace('/web/manageConcierge/standby/list')
        })
        .catch(() => {
          alert('대기화면 수정 실패')
          history.replace('/web/manageConcierge/standby/list')
        })
    }
  }
  useEffect(() => {
    setStandbyVideoList()
    setStandbyImageList()
  }, [])

  useEffect(() => {
    if (screen.screenDetailInfo !== undefined) {
      setStandbyData(screen)

      setColors(
        screen.screenDetailInfo.standbyList.map((value: standbyList) => {
          const rgb = value.textRGB.split(',')
          const newColor = Object({
            r: parseInt(rgb[0], 10),
            g: parseInt(rgb[1], 10),
            b: parseInt(rgb[2], 10),
            a: 1,
          })
          return newColor
        }),
      )

      setIsColorOpen(screen.screenDetailInfo.standbyList.map((_: any) => false))
      setIsMenuOpen(screen.screenDetailInfo.standbyList.map((_: any) => false))
    }
  }, [screen.screenDetailInfo])

  useEffect(() => {
    if (videoSelectOption.length !== 0 && imageSelectOption.length !== 0) {
      setSelectBoxIndex(
        screen.screenDetailInfo.standbyList.map((value: standbyList) => {
          if (value.imageYn === 'Y') return imageSelectOption.findIndex((imageValue) => imageValue === value.name)
          else return videoSelectOption.findIndex((videoValue) => videoValue === value.name)
        }),
      )
    }
  }, [videoSelectOption, imageSelectOption])

  const bodys =
    standbyData.screenDetailInfo.standbyList.length < 1
      ? [
          <tr key={0}>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td>
              <div style={{ height: '72px' }}></div>
            </td>
            <td>
              <button
                type="button"
                className="btn_plus_row type02"
                onClick={() => {
                  setStandbyData((standbyData) => {
                    const newStandbyData = {
                      screenInfo: standbyData.screenInfo,
                      screenDetailInfo: {
                        ...standbyData.screenDetailInfo,
                        standbyList: [
                          ...standbyData.screenDetailInfo.standbyList,
                          {
                            imageYn: 'Y',
                            name: '이미지1',
                            textRGB: '125,131,100',
                          },
                        ],
                      },
                    }
                    return newStandbyData
                  })
                  setColors((colors) => [...colors, { r: 0, g: 0, b: 0, a: 1 }])
                  setIsColorOpen((isColorOpen) => [...isColorOpen, false])
                  setIsMenuOpen((isMenuOpen) => [...isMenuOpen, false])
                }}
              >
                <img src={icoPlus} alt="플러스 아이콘" />
              </button>
            </td>
          </tr>,
        ]
      : standbyData.screenDetailInfo.standbyList.map((bodyValue, bodyIndex) => {
          return (
            <tr key={bodyIndex.toString()}>
              <td>
                <div className="cont_upAndDown">
                  <button
                    type="button"
                    onClick={() => {
                      if (bodyIndex !== 0) {
                        setStandbyData((standbyData) => {
                          const newStandbyData = {
                            screenInfo: standbyData.screenInfo,
                            screenDetailInfo: {
                              ...standbyData.screenDetailInfo,
                              standbyList: standbyData.screenDetailInfo.standbyList.slice(),
                            },
                          }
                          const tmpdata = newStandbyData.screenDetailInfo.standbyList[bodyIndex]
                          newStandbyData.screenDetailInfo.standbyList[bodyIndex] =
                            newStandbyData.screenDetailInfo.standbyList[bodyIndex - 1]
                          newStandbyData.screenDetailInfo.standbyList[bodyIndex - 1] = tmpdata
                          return newStandbyData
                        })

                        setSelectBoxIndex((selectBoxIndex: number[]) => {
                          const newSelectBoxIndex = selectBoxIndex.slice()
                          const tmpSelectBoxIndex = newSelectBoxIndex[bodyIndex]
                          newSelectBoxIndex[bodyIndex] = newSelectBoxIndex[bodyIndex - 1]
                          newSelectBoxIndex[bodyIndex - 1] = tmpSelectBoxIndex
                          return newSelectBoxIndex
                        })

                        setColors((colors) => {
                          const newColor = colors.slice()
                          const tmpColor = newColor[bodyIndex]
                          newColor[bodyIndex] = newColor[bodyIndex - 1]
                          newColor[bodyIndex - 1] = tmpColor
                          return newColor
                        })

                        setIsColorOpen((isColorOpen) => {
                          const newIsColorOpen = isColorOpen.slice()
                          const tmpIsColorOpen = newIsColorOpen[bodyIndex]
                          newIsColorOpen[bodyIndex] = newIsColorOpen[bodyIndex - 1]
                          newIsColorOpen[bodyIndex - 1] = tmpIsColorOpen
                          return newIsColorOpen
                        })

                        setIsMenuOpen((isMenuOpen) => {
                          const newIsMenuOpen = isMenuOpen.slice()
                          const tmpnewIsMenuOpen = newIsMenuOpen[bodyIndex]
                          newIsMenuOpen[bodyIndex] = newIsMenuOpen[bodyIndex - 1]
                          newIsMenuOpen[bodyIndex - 1] = tmpnewIsMenuOpen
                          return newIsMenuOpen
                        })
                      }
                    }}
                  >
                    <img src={icoArrowBlackUp} alt="up" />
                  </button>
                  <button
                    type="button"
                    onClick={() => {
                      if (bodyIndex !== standbyData.screenDetailInfo.standbyList.length - 1) {
                        setStandbyData((standbyData) => {
                          const newStandbyData = {
                            screenInfo: standbyData.screenInfo,
                            screenDetailInfo: {
                              ...standbyData.screenDetailInfo,
                              standbyList: standbyData.screenDetailInfo.standbyList.slice(),
                            },
                          }
                          const tmpdata = newStandbyData.screenDetailInfo.standbyList[bodyIndex]
                          newStandbyData.screenDetailInfo.standbyList[bodyIndex] =
                            newStandbyData.screenDetailInfo.standbyList[bodyIndex + 1]
                          newStandbyData.screenDetailInfo.standbyList[bodyIndex + 1] = tmpdata
                          return newStandbyData
                        })

                        setSelectBoxIndex((selectBoxIndex: number[]) => {
                          const newSelectBoxIndex = selectBoxIndex.slice()
                          const tmpSelectBoxIndex = newSelectBoxIndex[bodyIndex]
                          newSelectBoxIndex[bodyIndex] = newSelectBoxIndex[bodyIndex + 1]
                          newSelectBoxIndex[bodyIndex + 1] = tmpSelectBoxIndex
                          return newSelectBoxIndex
                        })

                        setColors((colors) => {
                          const newColor = colors.slice()
                          const tmpColor = newColor[bodyIndex]
                          newColor[bodyIndex] = newColor[bodyIndex + 1]
                          newColor[bodyIndex + 1] = tmpColor
                          return newColor
                        })

                        setIsColorOpen((isColorOpen) => {
                          const newIsColorOpen = isColorOpen.slice()
                          const tmpIsColorOpen = newIsColorOpen[bodyIndex]
                          newIsColorOpen[bodyIndex] = newIsColorOpen[bodyIndex + 1]
                          newIsColorOpen[bodyIndex + 1] = tmpIsColorOpen
                          return newIsColorOpen
                        })

                        setIsMenuOpen((isMenuOpen) => {
                          const newIsMenuOpen = isMenuOpen.slice()
                          const tmpnewIsMenuOpen = newIsMenuOpen[bodyIndex]
                          newIsMenuOpen[bodyIndex] = newIsMenuOpen[bodyIndex + 1]
                          newIsMenuOpen[bodyIndex + 1] = tmpnewIsMenuOpen
                          return newIsMenuOpen
                        })
                      }
                    }}
                  >
                    <img src={icoArrowBlackDown} alt="down" />
                  </button>
                </div>
              </td>
              <td>{bodyIndex + 1}</td>
              <td>
                <div className="btnR_txt_cont type02">
                  <div className="btn_radio_txt">
                    <input
                      type="radio"
                      className="file_type"
                      id={'rdImage' + bodyIndex}
                      name={'fileType0' + bodyIndex}
                      checked={bodyValue.imageYn === 'Y'}
                      onChange={() => {
                        setStandbyData((standbyData: screenDataType) => {
                          const newStandbyList = standbyData.screenDetailInfo.standbyList.slice()
                          newStandbyList[bodyIndex].imageYn = 'Y'
                          return {
                            ...standbyData,
                            screenDetailInfo: {
                              ...standbyData.screenDetailInfo,
                              standbyList: newStandbyList,
                            },
                          }
                        })
                      }}
                    />
                    <label htmlFor={'rdImage' + bodyIndex}>Image</label>
                  </div>
                  <div className="btn_radio_txt">
                    <input
                      type="radio"
                      className="file_type"
                      id={'rdVideo' + bodyIndex}
                      name={'fileType0' + bodyIndex}
                      checked={bodyValue.imageYn === 'N'}
                      onChange={() => {
                        setStandbyData((standbyData: screenDataType) => {
                          const newStandbyList = standbyData.screenDetailInfo.standbyList.slice()
                          newStandbyList[bodyIndex].imageYn = 'N'
                          return {
                            ...standbyData,
                            screenDetailInfo: {
                              ...standbyData.screenDetailInfo,
                              standbyList: newStandbyList,
                            },
                          }
                        })
                      }}
                    />
                    <label htmlFor={'rdVideo' + bodyIndex}>Video</label>
                  </div>
                </div>
              </td>
              <td className="padlr24">
                <div className="select_screen">
                  <div className="search_wrap disF">
                    <GroupSelectBox
                      items={bodyValue.imageYn === 'Y' ? imageSelectOption : videoSelectOption}
                      selectBoxLength={isMenuOpen.length}
                      groupMenuState={[isMenuOpen, setIsMenuOpen]}
                      openIndex={bodyIndex}
                      initialIndex={selectBoxIndex[bodyIndex]}
                      onItemClick={(index) => {
                        setSelectBoxIndex((selectBoxIndex) => {
                          const newSelectBoxIndex = selectBoxIndex
                          newSelectBoxIndex[bodyIndex] = index
                          return newSelectBoxIndex
                        })
                      }}
                    />
                  </div>
                  <div className="filebox type03">
                    <input className="upload-name" type="text" disabled />
                    <input type="file" id="fileWaiting01" accept="image/*,video/*" className="upload-hidden" />
                    <label htmlFor="fileWaiting01" className="btn_blueline_icon">
                      파일찾기
                    </label>
                  </div>
                </div>
              </td>
              <td style={{ position: 'relative' }}>
                <div
                  style={{
                    background: convertReturnColortoRGB(colors[bodyIndex]),
                  }}
                  className="btn_text_color"
                  onClick={() => {
                    setIsColorOpen((isColorOpen) => {
                      const newIsColorOpen = isColorOpen.slice()
                      for (let i = 0; i < isColorOpen.length; i += 1) {
                        if (i === bodyIndex) {
                          newIsColorOpen[i] = !newIsColorOpen[bodyIndex]
                        } else {
                          newIsColorOpen[i] = false
                        }
                      }

                      return newIsColorOpen
                    })
                  }}
                />
                <span className={'color-pallet ' + (isColorOpen[bodyIndex] ? '' : 'none')}>
                  <DebouncedColorPicker color={colors[bodyIndex]} onChange={setColors} index={bodyIndex} />
                </span>
              </td>
              <td>
                <button
                  type="button"
                  className="btn_minus_row type02"
                  onClick={() => {
                    setStandbyData((standbyData) => {
                      const newStandbyData = {
                        screenInfo: standbyData.screenInfo,
                        screenDetailInfo: {
                          ...standbyData.screenDetailInfo,
                          standbyList: standbyData.screenDetailInfo.standbyList
                            .slice()
                            .filter((_, index) => bodyIndex !== index),
                        },
                      }
                      setColors((colors) => colors.slice().filter((_, index) => bodyIndex !== index))
                      setIsColorOpen((isColorOpen) => isColorOpen.slice().filter((_, index) => bodyIndex !== index))
                      setIsMenuOpen((isMenuOpen) => isMenuOpen.slice().filter((_, index) => bodyIndex !== index))

                      return newStandbyData
                    })
                  }}
                >
                  <img src={icoMinus} alt="마이너스 아이콘" />
                </button>

                {standbyData.screenDetailInfo.standbyList.length < 5 &&
                  standbyData.screenDetailInfo.standbyList.length === bodyIndex + 1 && (
                    <button
                      type="button"
                      className="btn_plus_row type02"
                      onClick={() => {
                        setStandbyData((standbyData) => {
                          const newStandbyData = {
                            screenInfo: standbyData.screenInfo,
                            screenDetailInfo: {
                              ...standbyData.screenDetailInfo,
                              standbyList: [
                                ...standbyData.screenDetailInfo.standbyList,
                                {
                                  imageYn: 'Y',
                                  name: '이미지1',
                                  textRGB: '125,131,100',
                                },
                              ],
                            },
                          }
                          return newStandbyData
                        })
                        setColors((colors) => [...colors, { r: 0, g: 0, b: 0, a: 1 }])
                        setIsColorOpen((isColorOpen) => [...isColorOpen, false])
                        setIsMenuOpen((isMenuOpen) => [...isMenuOpen, false])
                      }}
                    >
                      <img src={icoPlus} alt="플러스 아이콘" />
                    </button>
                  )}
              </td>
            </tr>
          )
        })

  return (
    <div className="content">
      <div className="cnt_div">
        <div id="snb">
          <p>컨시어지 관리</p>
          <img src={icoSlash} alt="/" />
          <Link to="/web/manageConcierge/standby/list">대기화면 관리</Link>
        </div>
        <div className="stn_top">
          <form action="">
            <div className="cont">
              <h2 className="sta">
                대기화면 관리 <span>수정</span>
              </h2>
            </div>
          </form>
        </div>
        <div className="stn_cont">
          <div className="stn_lineBox type02">
            <div className="lineBox_cnts">
              <ul className="lineBox_cnts_ul">
                <li className="lineBox_cnts_tit">지점명</li>
                <li className="lineBox_cnts_cont">
                  <input type="text" className="w208" value={standbyData.screenInfo.branchName} disabled />
                </li>
              </ul>
              <ul className="lineBox_cnts_ul">
                <li className="lineBox_cnts_tit">지점 번호</li>
                <li className="lineBox_cnts_cont">
                  <input type="text" className="tac w64" value={standbyData.screenInfo.branchNumber} disabled />
                </li>
              </ul>
              <ul className="lineBox_cnts_ul">
                <li className="lineBox_cnts_tit">기기 번호</li>
                <li className="lineBox_cnts_cont">
                  <input type="text" className="tac w64" value={standbyData.screenInfo.deviceNumber} disabled />
                </li>
              </ul>
            </div>
          </div>
          <PaginationTable
            tableClass="table_standby_edit"
            divClass="h565"
            heads={heads}
            bodys={bodys}
            isScrollable={false}
            colLength={headOptions.length}
          />

          <div className="cont_btm_box">
            <div className="cnt_checkBox">
              <p className="cb_title">화면전환 타입</p>
              {standbyData !== null
                ? standbyData.screenDetailInfo.typeList.map((value, index) => {
                    return (
                      <div key={index.toString()} className="input_checkBox">
                        <input
                          type="checkbox"
                          className="type02"
                          id={convertIndexToType(index)}
                          name="checkType"
                          defaultChecked={value}
                          onClick={() => {
                            setStandbyData((standbyData: screenDataType) => {
                              const newTypeList = standbyData.screenDetailInfo.typeList.slice()
                              newTypeList[index] = !newTypeList[index]
                              return {
                                ...standbyData,
                                screenDetailInfo: {
                                  ...standbyData.screenDetailInfo,
                                  typeList: newTypeList,
                                },
                              }
                            })
                          }}
                        />
                        <label htmlFor={convertIndexToType(index)}>
                          <span className="checkLabel"></span>
                          <span className="text">{convertIndexToType(index)}</span>
                        </label>
                      </div>
                    )
                  })
                : ''}
              <span className="orange_desc">*중복선택 가능합니다.</span>
            </div>
            {/* <button type="button" className="btn_blueline_icon type02">
              대기화면 리스트 추가
            </button> */}
          </div>
        </div>
        <div className="stn_btm">
          <div className="btm_btn_cont flex_fr">
            <div className="btm_btn_right">
              <Link to="/web/manageConcierge/standby/list" className="btn_blue btn_register">
                취소
              </Link>
              <button
                type="button"
                className="btn_cancel btn_gray btn_md_close"
                onClick={(e) => {
                  e.preventDefault()
                  onEditButtonClick(convertStandbyDataToSetInfo(standbyData))
                }}
              >
                저장하기
              </button>
            </div>
          </div>
        </div>
      </div>
    </div>
  )
}
export default ConciergeStandbyEdit
