import React from 'react'
import { Redirect, Route, Switch } from 'react-router-dom'
import ConciergeStandbyList from './conciergeStandbyList'
import ConciergeStandbyDetail from './conciergeStandbyDetail'
import ConciergeStandbyEdit from './conciergeStandbyEdit'

const ConciergeStandbyInfo = () => {
  return (
    <Switch>
      <Route path="/web/manageConcierge/standby/list" component={ConciergeStandbyList} />
      <Route path="/web/manageConcierge/standby/detail/:id" component={ConciergeStandbyDetail} />
      <Route path="/web/manageConcierge/standby/edit" component={ConciergeStandbyEdit} />
      <Redirect exact path="/web/manageConcierge/standby" to="/web/manageConcierge/standby/list" />
      <Redirect exact path="/web/manageConcierge/standby/detail" to="/web/manageConcierge/standby/list" />
    </Switch>
  )
}

export default ConciergeStandbyInfo
