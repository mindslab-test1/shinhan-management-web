import { icoEye, icoSlash, icoEdit } from 'assets/images/icons'
import UrlPaging from 'components/Pagination/UrlPaging'
import PaginationTable from 'components/table/PaginationTable'
import qs from 'qs'
import React, { useCallback, useEffect, useState } from 'react'
import { Link, useHistory, useLocation } from 'react-router-dom'
import { useDispatch } from 'react-redux'
import ConciergeStandbyRepository from 'repository/Concierge/conciergeStandbyRepository'
import { ConciergeStandbyDetailInfo, ConciergeStandbyInfo } from 'typings/conciergeDTO'
import { getPageRowNumber } from 'utils'
import ConciergeStandbyAddModal from 'components/Modal/ConciergeStandbyAddModal/conciergeStandbyAddModal'
import { setScreenDetailInfos, setScreenInfo } from 'store/module/standbyScreenReducer'

type screenInfo = {
  deviceNumber: number
  branchNumber: number
  branchName: string
}

const ConciergeStandbyList = () => {
  const dispatch = useDispatch()
  const history = useHistory()

  const setScreenInfoState = (screenInfo: screenInfo) => dispatch(setScreenInfo(screenInfo))
  const setScreenDetailInfosState = (screenDetailInfo: ConciergeStandbyDetailInfo) =>
    dispatch(setScreenDetailInfos(screenDetailInfo))

  const [items, setItems] = useState<ConciergeStandbyInfo[]>()
  const [totalPage, setTotalPage] = useState(0)
  const [currentPage, setCurrentPage] = useState(1)
  const location = useLocation()
  const query = qs.parse(location.search, {
    ignoreQueryPrefix: true,
  })
  const [isShowModal, setIsShowModal] = useState(false)

  const getData = useCallback(async () => {
    await Promise.all([
      ConciergeStandbyRepository.getPages(),
      ConciergeStandbyRepository.getStandbyList(currentPage),
    ]).then((responses) => {
      if (responses[0].status === 200) setTotalPage(responses[0].data.pageCnt)
      if (responses[1].status === 200) setItems(responses[1].data.standbyList)
    })
  }, [query.device, currentPage])

  useEffect(() => {
    getData()
  }, [query.device, currentPage])

  const onEditButtonClick = async (screenInfo: screenInfo, id: number) => {
    setScreenInfoState(screenInfo)
    await ConciergeStandbyRepository.getDetail(id).then((response) => {
      if (response.status !== 200) {
        alert('서버에 문제가 발생하였습니다')
      } else if (!response.data.typeList || !response.data.standbyList) {
        setScreenDetailInfosState({
          typeList: [false, false, false],
          standbyList: [],
        })
        history.replace({
          pathname: '/web/manageConcierge/standby/edit',
          state: { state: '/web/manageConcierge/standby/edit' },
        })
      } else {
        setScreenDetailInfosState(response.data)
        history.replace({
          pathname: '/web/manageConcierge/standby/edit',
          state: { state: '/web/manageConcierge/standby/edit' },
        })
      }
    })
  }

  const onDetailButtonClick = async (screenInfo: screenInfo, id: number) => {
    setScreenInfoState(screenInfo)
    await ConciergeStandbyRepository.getDetail(id).then((response) => {
      if (response.status !== 200) {
        alert('서버에 문제가 발생하였습니다')
      } else if (!response.data.typeList || !response.data.standbyList) {
        setScreenDetailInfosState({
          typeList: [false, false, false],
          standbyList: [],
        })
        history.replace({
          pathname: '/web/manageConcierge/standby/detail/' + id,
          state: { state: '/web/manageConcierge/standby/detail/' + id },
        })
      } else {
        setScreenDetailInfosState(response.data)
        history.replace({
          pathname: '/web/manageConcierge/standby/detail/' + id,
          state: { state: '/web/manageConcierge/standby/detail/' + id },
        })
      }
    })
  }

  const getBodys = (bodyItems: ConciergeStandbyInfo[] | undefined): JSX.Element[] => {
    if (typeof bodyItems === 'undefined' || bodyItems === null || bodyItems.length < 1) {
      return [
        <tr key={1}>
          <td colSpan={12} className="nodata_td">
            데이터가 없습니다.
          </td>
        </tr>,
      ]
    } else {
      return bodyItems.map((data, idx) => {
        return (
          <tr key={data.id}>
            <td>{getPageRowNumber(idx, currentPage)}</td>
            <td>{data.branchName}</td>
            <td>{data.branchNo}</td>
            <td>{data.deviceNo}</td>
            <td>{data.typeA ? '✓' : ''}</td>
            <td>{data.typeB ? '✓' : ''}</td>
            <td>{data.typeC ? '✓' : ''}</td>
            <td>{data.imageCnt}</td>
            <td>{data.videoCnt}</td>
            <td>{data.created}</td>
            <td className="td_btn">
              <Link
                className="btn_detailPage"
                to={{
                  pathname: '/web/manageConcierge/standby/detail/' + data.id,
                  state: { state: '/web/manageConcierge/standby/detail/' + data.id },
                }}
                onClick={(e) => {
                  e.preventDefault()
                  onDetailButtonClick(
                    {
                      deviceNumber: parseInt(data.deviceNo, 10),
                      branchName: data.branchName,
                      branchNumber: parseInt(data.branchNo, 10),
                    },
                    data.id,
                  )
                }}
              >
                <span>보기</span>
                <img src={icoEye} alt="보기 아이콘" />
              </Link>
            </td>
            <td colSpan={2} className="td_btn">
              <Link
                className="btn_edit"
                to={{
                  pathname: '/web/manageConcierge/standby/edit',
                  state: { state: '/web/manageConcierge/standby/edit' },
                }}
                onClick={(e) => {
                  e.preventDefault()
                  onEditButtonClick(
                    {
                      deviceNumber: parseInt(data.deviceNo, 10),
                      branchName: data.branchName,
                      branchNumber: parseInt(data.branchNo, 10),
                    },
                    data.id,
                  )
                }}
              >
                <span>수정하기</span>
                <img src={icoEdit} alt="수정하기 아이콘" />
              </Link>
            </td>
          </tr>
        )
      })
    }
  }

  const headOption1 = [
    'No.',
    '지점명',
    '지점 번호',
    '기기 번호',
    '화면전환 스타일',
    '대기화면 형식',
    '등록일',
    '보기',
    '수정',
  ]
  const headOption2 = ['Type A', 'Type B-1', 'Type B-2', '이미지', '영상']

  const heads = (
    <>
      <tr>
        {headOption1.map((data, idx) => {
          if (data === '화면전환 스타일') {
            return (
              <th colSpan={3} className="" key={data}>
                <div>
                  <span>{data}</span>
                </div>
              </th>
            )
          } else if (data === '대기화면 형식') {
            return (
              <th colSpan={2} className="" key={data}>
                <div>
                  <span>{data}</span>
                </div>
              </th>
            )
          } else {
            return (
              <th rowSpan={2} key={data}>
                <div>
                  <span>{data}</span>
                </div>
              </th>
            )
          }
        })}
      </tr>
      <tr>
        <th>
          <div>
            <span>Type A</span>
          </div>
        </th>
        <th className="borderL">
          <div>
            <span>Type B-1</span>
          </div>
        </th>
        <th className="borderL">
          <div>
            <span>Type B-2</span>
          </div>
        </th>
        <th>
          <div>
            <span>이미지</span>
          </div>
        </th>
        <th className="borderL">
          <div>
            <span>영상</span>
          </div>
        </th>
      </tr>
    </>
  )
  return (
    <div className="content">
      <div className="cnt_div">
        <div id="snb">
          <p>컨시어지 관리</p>
          <img src={icoSlash} alt="/" />
          <Link to="/web/manageConcierge/standby/list">대기화면 관리</Link>
        </div>
        <div className="stn_top">
          <div className="cont">
            <h2 className="sta">
              대기화면 관리
              <span>LIST</span>
            </h2>
          </div>
        </div>
        <ConciergeStandbyAddModal
          show={isShowModal}
          onCloseModal={() => {
            setIsShowModal(false)
          }}
        />
        <div className="stn_cont">
          <div className="stn_cont_header">
            <p className="color_blue">
              ※ 대기화면 추가 후 화면변경을 원하신다면 아래 표의 수정하기 버튼을 누르신 후 이용해주세요.
            </p>
            <button type="button" className="btn_blue btn_modal_open" onClick={() => setIsShowModal(true)}>
              대기화면 추가
            </button>
          </div>
          <PaginationTable
            tableClass="table_standby_list"
            heads={heads}
            bodys={getBodys(items)}
            isScrollable
            colLength={12}
          />
          <UrlPaging pageLength={totalPage} currentPage={currentPage} onClickPage={setCurrentPage} />
        </div>
      </div>
    </div>
  )
}

export default ConciergeStandbyList
