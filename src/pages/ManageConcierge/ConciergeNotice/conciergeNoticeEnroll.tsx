import { icoSlash, icoSearch } from 'assets/images/icons'
import qs from 'qs'
import React, { useState } from 'react'
import { Link, Redirect, useHistory, useLocation } from 'react-router-dom'
import { useDispatch, useSelector } from 'react-redux'
import { rootState } from 'store'
import Calendar from 'components/Calendar/Calendar'
import { setDate, setBranch } from 'store/module/noticeInfoReducer'
import { DateRangePicker } from 'rsuite'
import ConciergeNoticeRepository from 'repository/Concierge/conciergeNoticeRepository'
import ConciergeBranchInfoModal from 'components/Modal/ConciergeBranchInfoModal/conciergeBranchInfoModal'
import SelectBox from 'components/SelectBox/selectBox'

const ConciergeNoticeEnroll = () => {
  const location = useLocation()
  const history = useHistory()
  if (location.state === undefined || location.state === null || location.state === '' || history.action === 'POP') {
    return <Redirect to="/web/manageConcierge/notice/list" />
  }

  type noticeType = '전체' | '지점' | 'UNDEFINED'

  type branchInfo = {
    type: noticeType
    deviceNo: string
    branchNo: string
    branchName: string
  }

  const { beforeToday } = DateRangePicker

  const dispatch = useDispatch()
  const setBranchState = (branchInfo: branchInfo) => dispatch(setBranch(branchInfo))
  const noticeInfoState = useSelector((state: rootState) => state.noticeInfoReducer.noticeInfo)

  const [paramKeyIndex, setParamKeyIndex] = useState(0)
  const [inputText, setInputText] = useState('')
  const [isUsing, setIsUsing] = useState('Y')
  const [isShowModal, setIsShowModal] = useState(false)

  const options = ['전체', '지점']

  const onChangeInput = (text: string) => {
    setInputText(text)
  }

  const onEnrollButtonPress = async () => {
    await ConciergeNoticeRepository.setEnroll({
      deviceNo: noticeInfoState.deviceNo.toString(),
      branchNo: noticeInfoState.branchNo.toString(),
      text: inputText,
      branchYn: noticeInfoState.type === '지점' ? 'Y' : 'N',
      startDate: noticeInfoState.searchDate.startDate.toISOString().split('T')[0],
      endDate: noticeInfoState.searchDate.endDate.toISOString().split('T')[0],
      useYn: isUsing,
    })
      .then((res) => {
        if (res.data.code !== 0) {
          alert(res.data.message)
        }
        history.replace('/web/manageConcierge/notice/list')
      })
      .catch(() => {
        alert('공지사항 등록 실패')
        history.replace('/web/manageConcierge/notice/list')
      })
  }

  const onCancelButtonClick = () => {}

  return (
    <>
      <div className="content">
        <div className="cnt_div">
          <div id="snb">
            <p>컨시어지 관리</p>
            <img src={icoSlash} alt="/" />
            <Link to="/web/manageConcierge/notice/list">공지사항 관리</Link>
          </div>
          <div className="stn_top">
            <div className="cont">
              <h2 className="sta">
                공지사항 관리
                <span>등록</span>
              </h2>
            </div>
          </div>
          <ConciergeBranchInfoModal
            show={isShowModal}
            onCloseModal={() => {
              setIsShowModal(false)
            }}
          />
          <div className="stn_cont">
            <div className="stn_lineBox">
              <div className="lineBox_cnts">
                <ul className="lineBox_cnts_ul">
                  <li className="lineBox_cnts_tit">지점명</li>
                  <li className="lineBox_cnts_cont">
                    <input type="text" className="w208" value={noticeInfoState.branchName ?? ''} disabled />
                  </li>
                </ul>
                <ul className="lineBox_cnts_ul">
                  <li className="lineBox_cnts_tit">지점 번호</li>
                  <li className="lineBox_cnts_cont">
                    <input type="text" className="tac w64" value={noticeInfoState.branchNo ?? ''} disabled />
                  </li>
                </ul>
                <ul className="lineBox_cnts_ul">
                  <li className="lineBox_cnts_tit">기기 번호</li>
                  <li className="lineBox_cnts_cont">
                    <input type="text" className="tac w64" value={noticeInfoState.deviceNo ?? ''} disabled />
                  </li>
                </ul>
                {paramKeyIndex === 1 && (
                  <ul className="lineBox_cnts_ul">
                    <li>
                      <button
                        type="button"
                        className="btn_blueline_icon btn_modal_open"
                        data-popup="select_branch"
                        onClick={() => setIsShowModal(true)}
                      >
                        지점정보 불러오기 <img src={icoSearch} alt="검색 아이콘" />
                      </button>
                    </li>
                  </ul>
                )}
              </div>
              <div className="lineBox_cnts">
                <ul className="lineBox_cnts_ul">
                  <li className="lineBox_cnts_tit">종류</li>
                  <li className="lineBox_cnts_cont">
                    <SelectBox
                      items={options}
                      onItemClick={(index) => {
                        if (index === 0) {
                          setBranchState({
                            type: '전체',
                            branchName: '',
                            branchNo: '',
                            deviceNo: '',
                          })
                        }
                        setParamKeyIndex(index)
                      }}
                    />
                  </li>
                </ul>
              </div>
              <div className="lineBox_cnts">
                <ul className="lineBox_cnts_ul">
                  <li className="lineBox_cnts_tit">공지기간</li>
                  <li className="lineBox_cnts_cont">
                    <Calendar date={noticeInfoState.searchDate} reducerAction={setDate} disabledDate={beforeToday} />
                  </li>
                </ul>
              </div>
              <div className="lineBox_cnts">
                <ul className="lineBox_cnts_ul">
                  <li className="lineBox_cnts_tit">내용</li>
                  <li className="lineBox_cnts_cont">
                    <input
                      type="text"
                      className="w518"
                      maxLength={30}
                      value={inputText}
                      autoComplete="off"
                      onChange={(e) => onChangeInput(e.target.value)}
                    />
                    <span className="orange_desc">*최대 30자 까지만 입력 가능합니다.</span>
                  </li>
                </ul>
              </div>
              <div className="lineBox_cnts">
                <ul className="lineBox_cnts_ul">
                  <li className="lineBox_cnts_tit">사용여부</li>
                  <li className="lineBox_cnts_cont btnR_txt_cont">
                    <div className="btn_radio_txt">
                      <input
                        type="radio"
                        id="rdYes"
                        name="noticeUsing"
                        checked={isUsing === 'Y'}
                        onClick={() => {
                          setIsUsing('Y')
                        }}
                      />
                      <label htmlFor="rdYes">YES</label>
                    </div>
                    <div className="btn_radio_txt">
                      <input
                        type="radio"
                        id="rdNo"
                        name="noticeUsing"
                        checked={isUsing === 'N'}
                        onClick={() => {
                          setIsUsing('N')
                        }}
                      />
                      <label htmlFor="rdNo">NO</label>
                    </div>
                  </li>
                </ul>
              </div>
            </div>
          </div>
          <div className="stn_btm">
            <div className="btm_btn_cont ">
              <div className="btm_btn_right">
                <Link
                  to="/web/manageConcierge/notice/list"
                  className="btn_cancel btn_gray btn_md_close"
                  onClick={() => {
                    onCancelButtonClick()
                  }}
                >
                  취소
                </Link>
                <button type="button" className="btn_blue btn_register" onClick={() => onEnrollButtonPress()}>
                  등록하기
                </button>
              </div>
            </div>
          </div>
        </div>
      </div>
    </>
  )
}

export default ConciergeNoticeEnroll
