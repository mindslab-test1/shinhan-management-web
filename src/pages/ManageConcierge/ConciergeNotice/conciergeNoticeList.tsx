import { icoSlash, icoEye, icoEdit } from 'assets/images/icons'
import UrlPaging from 'components/Pagination/UrlPaging'
import PaginationTable from 'components/table/PaginationTable'
import qs from 'qs'
import { getPageRowNumber } from 'utils'
import React, { useEffect, useState, useCallback } from 'react'
import { Link, useLocation } from 'react-router-dom'
import { ConciergeNoticeInfo } from 'typings/conciergeDTO'
import { useDispatch } from 'react-redux'
import { setInitial, setNoticeInfo } from 'store/module/noticeInfoReducer'
import ConciergeNoticeRepository from 'repository/Concierge/conciergeNoticeRepository'

const ConciergeNotice = () => {
  type noticeType = '전체' | '지점' | 'UNDEFINED'

  type noticeInfo = {
    id: number
    type: noticeType
    deviceNo: string
    branchNo: string
    branchName: string
    text: string
    isUsing: string
    searchDate: searchDate
    enrollDate: string
    previousNotice: string
    nextNotice: string
  }

  type searchDate = {
    startDate: Date
    endDate: Date
  }

  const dispatch = useDispatch()
  const setInitialState = () => dispatch(setInitial())
  const setNoticeInfoState = (noticeInfo: noticeInfo) => dispatch(setNoticeInfo(noticeInfo))

  const location = useLocation()
  const [items, setItems] = useState<ConciergeNoticeInfo[]>()
  const [checkList, setCheckList] = useState([])
  const [totalPage, setTotalPage] = useState(10)
  const [currentPage, setCurrentPage] = useState(1)
  // const [currentPage, setCurrentPage] = useState(1)
  const query = qs.parse(location.search, { ignoreQueryPrefix: true })

  const handleCheckClick = (index: number) => {
    setCheckList((checks) => checks.map((c, i) => (i === index ? !c : c)))
  }

  const handleAllCheck = () => {
    if (isAllChecked) {
      setCheckList((checks) => checks.map((c) => false))
    } else if (checkList.every((c) => !c)) {
      setCheckList((checks) => checks.map((c) => true))
    } else if (checkList.filter((c) => c).length < Math.ceil(checkList.length / 2)) {
      setCheckList((checks) => checks.map((c) => true))
    } else {
      setCheckList((checks) => checks.map((c) => false))
    }
  }

  useEffect(() => {
    setInitialState()
  }, [])

  const isAllChecked = checkList.every((x) => x)

  const getNoticeList = useCallback(async () => {
    await ConciergeNoticeRepository.getNoticeList(currentPage).then((response) => {
      if (response.status === 200)
        setItems(
          response.data.noticeList.map((noticeList: any) => {
            return {
              id: noticeList.id,
              type: noticeList.branchYn === 'Y' ? '지점' : '전체',
              branchName: noticeList.branchName,
              branchNo: noticeList.branchNo,
              deviceNo: noticeList.deviceNo,
              text: noticeList.text,
              isUsing: noticeList.useYn,
              searchDate: {
                startDate: new Date(noticeList.startDate),
                endDate: new Date(noticeList.endDate),
              },
              enrollDate: noticeList.created,
            }
          }),
        )
    })
  }, [currentPage])

  const onDeleteButtonClick = useCallback(async () => {
    const ids = checkList
      .map((value, index) => {
        if (value) return items[index].id
        else return undefined
      })
      .filter((value) => value !== undefined)

    if (ids.length === 1) {
      await ConciergeNoticeRepository.deleteOnly(ids[0]).then((_) => getNoticeList())
    } else {
      await ConciergeNoticeRepository.deleteList(ids).then((_) => getNoticeList())
    }
  }, [checkList])

  const getData = useCallback(async () => {
    await Promise.all([ConciergeNoticeRepository.getPages(), ConciergeNoticeRepository.getNoticeList(1)]).then(
      (responses) => {
        if (responses[0].status === 200) setTotalPage(responses[0].data.pageCnt)
        if (responses[1].status === 200)
          setItems(
            responses[1].data.noticeList.map((noticeList: any) => {
              return {
                id: noticeList.id,
                type: noticeList.branchYn === 'Y' ? '지점' : '전체',
                branchName: noticeList.branchName,
                branchNo: noticeList.branchNo,
                deviceNo: noticeList.deviceNo,
                text: noticeList.text,
                isUsing: noticeList.useYn,
                searchDate: {
                  startDate: new Date(noticeList.startDate),
                  endDate: new Date(noticeList.endDate),
                },
                enrollDate: noticeList.created,
              }
            }),
          )
      },
    )
  }, [location.search])

  useEffect(() => {
    getData()
  }, [location.search])

  useEffect(() => {
    getNoticeList()
  }, [currentPage])

  useEffect(() => {
    const tmpCheckList = []
    if (items !== undefined) {
      for (let i = 0; i < items.length; i += 1) {
        tmpCheckList.push(false)
      }
      setCheckList(tmpCheckList)
    }
  }, [items])

  const getBodys = (bodyItems: ConciergeNoticeInfo[] | undefined): JSX.Element[] => {
    if (!bodyItems) {
      return [
        <tr key={1}>
          <td colSpan={headOptions.length} className="nodata_td">
            데이터가 없습니다.
          </td>
        </tr>,
      ]
    } else {
      return bodyItems.map((value, index) => {
        return (
          <tr key={value.id}>
            <td>
              <input
                type="checkbox"
                id={'cb' + index.toString()}
                name="cbDelete"
                checked={checkList[index] ?? false}
                readOnly
                onClick={() => handleCheckClick(index)}
              />
              <label htmlFor={'cb' + index.toString()} />
            </td>
            <td>{getPageRowNumber(index, currentPage)}</td>
            <td>{value.type}</td>
            <td className="padlr24">{value.branchName}</td>
            <td>{value.branchNo}</td>
            <td>{value.deviceNo}</td>
            <td className="padlr24 tal">{value.text}</td>
            <td>{value.isUsing}</td>
            <td>
              {value.searchDate.startDate.toISOString().split('T')[0] +
                ' - ' +
                value.searchDate.endDate.toISOString().split('T')[0]}
            </td>
            <td>{value.enrollDate}</td>
            <td className="td_btn">
              <Link
                className="btn_detailPage"
                to={{
                  pathname: '/web/manageConcierge/notice/detail',
                  state: { state: '/web/manageConcierge/notice/detail' },
                }}
                onClick={() => {
                  setNoticeInfoState({
                    id: value.id,
                    type: value.type as noticeType,
                    branchName: value.branchName,
                    branchNo: value.branchNo,
                    deviceNo: value.deviceNo,
                    text: value.text,
                    isUsing: value.isUsing,
                    searchDate: {
                      startDate: value.searchDate.startDate,
                      endDate: value.searchDate.endDate,
                    },
                    enrollDate: value.enrollDate,
                    previousNotice: index === 0 ? '' : bodyItems[index - 1].text,
                    nextNotice: index < bodyItems.length - 1 ? bodyItems[index + 1].text : '',
                  })
                }}
              >
                <span>보기</span>
                <img src={icoEye} alt="보기 아이콘" />
              </Link>
            </td>
            <td className="td_btn">
              <Link
                className="btn_edit"
                to={{
                  pathname: '/web/manageConcierge/notice/edit',
                  state: { state: '/web/manageConcierge/notice/edit' },
                }}
                onClick={() => {
                  setNoticeInfoState({
                    id: value.id,
                    type: value.type as noticeType,
                    branchName: value.branchName,
                    branchNo: value.branchNo,
                    deviceNo: value.deviceNo,
                    text: value.text,
                    isUsing: value.isUsing,
                    searchDate: {
                      startDate: value.searchDate.startDate,
                      endDate: value.searchDate.endDate,
                    },
                    enrollDate: value.enrollDate,
                    previousNotice: index === 0 ? '' : bodyItems[index - 1].text,
                    nextNotice: index < bodyItems.length - 1 ? bodyItems[index + 1].text : '',
                  })
                }}
              >
                <span>수정하기</span>
                <img src={icoEdit} alt="수정하기 아이콘" />
              </Link>
            </td>
          </tr>
        )
      })
    }
  }
  const headOptions = [
    'No.',
    '종류',
    '지점명',
    '지점 번호',
    '기기 번호',
    '내용',
    '사용여부',
    '공지기간',
    '등록일',
    '보기',
    '수정',
  ]

  const heads = (
    <tr>
      {[
        <th key={0}>
          <input type="checkbox" id="cbAll" name="dummy" checked={isAllChecked} onClick={handleAllCheck} readOnly />
          <label htmlFor="cbAll" />
        </th>,
        ...headOptions.map((data) => {
          return (
            <th key={data}>
              <span>{data}</span>
            </th>
          )
        }),
      ]}
    </tr>
  )

  return (
    <div className="content">
      <div className="cnt_div">
        <div id="snb">
          <p>컨시어지 관리</p>
          <img src={icoSlash} alt="/" />
          <Link to="/web/manageConcierge/notice/list">공지사항 관리</Link>
        </div>
        <div className="stn_top">
          <div className="cont">
            <h2 className="sta">
              공지사항 관리
              <span>LIST</span>
            </h2>
          </div>
        </div>
        <PaginationTable
          tableClass="table_notice"
          heads={heads}
          bodys={getBodys(items)}
          isScrollable
          colLength={headOptions.length + 1}
        />
        <div className="stn_btm">
          <div className="btm_btn_cont">
            <button
              type="button"
              className="btn_delete"
              disabled // {checkList.every((c) => !c)}
              // onClick={() => {
              //   onDeleteButtonClick()
              // }}
            >
              삭제하기
            </button>
            <Link
              to={{
                pathname: '/web/manageConcierge/notice/enroll',
                state: { state: '/web/manageConcierge/notice/enroll' },
              }}
              className="btn_blue btn_register"
            >
              등록하기
            </Link>
          </div>
        </div>
        <UrlPaging pageLength={totalPage} currentPage={currentPage} onClickPage={setCurrentPage} />
      </div>
    </div>
  )
}

export default ConciergeNotice
