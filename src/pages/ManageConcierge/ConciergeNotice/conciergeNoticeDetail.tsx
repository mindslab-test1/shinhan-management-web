import { icoSlash } from 'assets/images/icons'
import qs from 'qs'
import React, { useCallback } from 'react'
import { Link, Redirect, useHistory, useLocation } from 'react-router-dom'
import { useDispatch, useSelector } from 'react-redux'
import { rootState } from 'store'
import ConciergeNoticeRepository from 'repository/Concierge/conciergeNoticeRepository'

const ConciergeNoticeDetail = () => {
  const location = useLocation()
  const history = useHistory()
  if (location.state === undefined || location.state === null || location.state === '' || history.action === 'POP') {
    return <Redirect to="/web/manageConcierge/notice/list" />
  }

  const noticeInfo = useSelector((state: rootState) => state.noticeInfoReducer.noticeInfo)

  const onDeleteButtonClick = useCallback(async () => {
    await ConciergeNoticeRepository.deleteOnly(noticeInfo.id)
  }, [])

  const onEditButtonClick = useCallback(() => {}, [])

  const onListButtonClick = useCallback(() => {}, [])

  return (
    <div className="content">
      <div className="cnt_div">
        <div id="snb">
          <p>컨시어지 관리</p>
          <img src={icoSlash} alt="/" />
          <Link to="/web/manageConcierge/notice/list">공지사항 관리</Link>
        </div>
        <div className="stn_top">
          <div className="cont">
            <h2 className="sta">
              공지사항 관리
              <span>보기</span>
            </h2>
          </div>
        </div>
        <div className="stn_cont">
          <div className="stn_lineBox">
            <div className="lineBox_cnts">
              <ul className="lineBox_cnts_ul">
                <li className="lineBox_cnts_tit">지점명</li>
                <li className="lineBox_cnts_cont">
                  <span>{noticeInfo.branchName}</span>
                </li>
              </ul>
              <ul className="lineBox_cnts_ul">
                <li className="lineBox_cnts_tit">지점 번호</li>
                <li className="lineBox_cnts_cont">
                  <span>{noticeInfo.branchNo}</span>
                </li>
              </ul>
              <ul className="lineBox_cnts_ul">
                <li className="lineBox_cnts_tit">기기 번호</li>
                <li className="lineBox_cnts_cont">
                  <span>{noticeInfo.deviceNo}</span>
                </li>
              </ul>
            </div>
            <div className="lineBox_cnts">
              <ul className="lineBox_cnts_ul">
                <li className="lineBox_cnts_tit">종류</li>
                <li className="lineBox_cnts_cont">
                  <span>{noticeInfo.type}</span>
                </li>
              </ul>
            </div>
            <div className="lineBox_cnts">
              <ul className="lineBox_cnts_ul">
                <li className="lineBox_cnts_tit">공지기간</li>
                <li className="lineBox_cnts_cont">
                  <span>
                    {noticeInfo.searchDate.startDate.toISOString().split('T')[0] +
                      ' - ' +
                      noticeInfo.searchDate.endDate.toISOString().split('T')[0]}
                  </span>
                </li>
              </ul>
            </div>
            <div className="lineBox_cnts">
              <ul className="lineBox_cnts_ul">
                <li className="lineBox_cnts_tit">내용</li>
                <li className="lineBox_cnts_cont">
                  <span>{noticeInfo.text}</span>
                </li>
              </ul>
            </div>
            <div className="lineBox_cnts">
              <ul className="lineBox_cnts_ul">
                <li className="lineBox_cnts_tit">사용여부</li>
                <li className="lineBox_cnts_cont btnR_txt_cont">
                  <span>{noticeInfo.isUsing}</span>
                </li>
              </ul>
            </div>
            <div className="lineBox_cnts">
              <ul className="lineBox_cnts_ul">
                <li className="lineBox_cnts_tit">등록일</li>
                <li className="lineBox_cnts_cont btnR_txt_cont">
                  <span>{noticeInfo.enrollDate}</span>
                </li>
              </ul>
            </div>
          </div>
        </div>
        <div className="stn_btm">
          <div className="btm_btn_cont ">
            <Link to="/web/manageConcierge/notice/list" className="btn_delete" style={{ pointerEvents: 'none' }}>
              {/* onClick={() => onDeleteButtonClick()} style은 없애기 */}
              삭제하기
            </Link>
            <div className="btm_btn_right">
              <Link
                to={{
                  pathname: '/web/manageConcierge/notice/edit',
                  state: { state: '/web/manageConcierge/notice/edit' },
                }}
                className="btn_blue btn_register"
                onClick={() => {
                  onEditButtonClick()
                }}
              >
                수정하기
              </Link>
              <Link
                to="/web/manageConcierge/notice/list"
                className="btn_cancel btn_gray btn_md_close"
                onClick={() => {
                  onListButtonClick()
                }}
              >
                목록으로
              </Link>
            </div>
          </div>
        </div>

        <div className="stn_btm_direction" style={{ display: 'none' }}>
          <div className="cont_direct">
            <p>이전글</p>
            <Link to="/web/manageConcierge/notice/list">{noticeInfo.previousNotice}</Link>
          </div>
          <div className="cont_direct">
            <p>다음글</p>
            <Link to="/web/manageConcierge/notice/list">{noticeInfo.nextNotice}</Link>
          </div>
        </div>
      </div>
    </div>
  )
}

export default ConciergeNoticeDetail
