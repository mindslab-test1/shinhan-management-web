import React from 'react'
import { Redirect, Route, Switch } from 'react-router-dom'
import ConciergeNoticeDetail from './conciergeNoticeDetail'
import ConciergeNoticeList from './conciergeNoticeList'
import ConciergeNoticeEdit from './conciergeNoticeEdit'
import ConciergeNoticeEnroll from './conciergeNoticeEnroll'

const ConciergeNotice = () => {
  return (
    <Switch>
      <Route path="/web/manageConcierge/notice/list" component={ConciergeNoticeList} />
      <Route path="/web/manageConcierge/notice/detail" component={ConciergeNoticeDetail} />
      <Route path="/web/manageConcierge/notice/edit" component={ConciergeNoticeEdit} />
      <Route path="/web/manageConcierge/notice/enroll" component={ConciergeNoticeEnroll} />
      <Redirect exact path="/web/manageConcierge/notice" to="/web/manageConcierge/notice/list" />
    </Switch>
  )
}

export default ConciergeNotice
