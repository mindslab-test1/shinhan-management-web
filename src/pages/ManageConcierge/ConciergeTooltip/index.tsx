import React from 'react'
import { Redirect, Route, Switch } from 'react-router-dom'
import ConciergeTooltipEdit from './conciergeTooltipEdit'
import conciergeTooltipList from './conciergeTooltipList'

const ConciergeNotice = () => {
  return (
    <Switch>
      <Route path="/web/manageConcierge/tooltip/edit" component={ConciergeTooltipEdit} />
      <Route path="/web/manageConcierge/tooltip/list" component={conciergeTooltipList} />
      <Redirect exact path="/web/manageConcierge/tooltip" to="/web/manageConcierge/tooltip/list" />
    </Switch>
  )
}

export default ConciergeNotice
