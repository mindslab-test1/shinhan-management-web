import { icoSlash } from 'assets/images/icons'
import React, { useEffect, useState } from 'react'
import { useDispatch, useSelector } from 'react-redux'
import { Link } from 'react-router-dom'
import ConciergeTooltipRepository from 'repository/Concierge/conciergeTooltipRepository'
import { rootState } from 'store'
import { setTooltipInfo } from 'store/module/tooltipInfoReducer'

const conciergeTooltipList = () => {
  const text = useSelector((state: rootState) => state.tooltipInfoReducer.text)
  const dispatch = useDispatch()
  const setTooltipInfoState = (text: string[]) => dispatch(setTooltipInfo(text))
  const [texts, setTexts] = useState<string[]>(['', '', '', '', ''])

  const getData = async () => {
    await new Promise((resolve) => setTimeout(resolve, 50))
    await ConciergeTooltipRepository.getList().then((response) => {
      if (response.status === 200) {
        setTexts((_) => {
          const newTexts = []
          for (let i = 0; i < 5; i += 1) {
            if (response.data.tooltipList[i] === undefined) {
              newTexts.push('')
            } else {
              newTexts.push(response.data.tooltipList[i].text)
            }
          }
          return newTexts
        })
      }
    })
  }

  useEffect(() => {
    getData()
  }, [text])

  return (
    <div className="content">
      <div className="cnt_div">
        <div id="snb">
          <p>컨시어지 관리</p>
          <img src={icoSlash} alt="/" />
          <Link to="/web/manageConcierge/tooltip/list">툴팁 관리</Link>
        </div>
        <div className="stn_top">
          <form action="">
            <div className="cont">
              <h2 className="sta">
                툴팁 관리 <span>보기</span>
              </h2>
            </div>
          </form>
        </div>
      </div>
      <div className="stn_cont">
        <div className="stn_lineBox">
          <div className="lineBox_cntTitle_wrap">
            <h3 className="lineBox_cnt_title">입력정보</h3>
            {texts.map((value, index) => (
              <div className="lineBox_cnts type03" key={index.toString()}>
                <ul className="lineBox_cnts_ul">
                  <li className="lineBox_cnts_tit pt0">{'툴팁 ' + (index + 1)}</li>
                  <li className="lineBox_cnts_cont">
                    <span style={{ whiteSpace: 'pre-line' }}>{value}</span>
                  </li>
                </ul>
              </div>
            ))}
          </div>
        </div>
      </div>
      <div className="stn_btm">
        <div className="btm_btn_cont flex_fr">
          <div className="btm_btn_right">
            <Link
              to={{
                pathname: '/web/manageConcierge/tooltip/edit',
                state: { state: '/web/manageConcierge/tooltip/edit' },
              }}
              className="btn_blue"
              onClick={() => {
                setTooltipInfoState(texts)
              }}
            >
              수정하기
            </Link>
          </div>
        </div>
      </div>
    </div>
  )
}

export default conciergeTooltipList
