import React, { useEffect, useRef, useState } from 'react'
import { Link, Redirect, useHistory, useLocation } from 'react-router-dom'
import { icoSlash } from 'assets/images/icons'
import { useSelector } from 'react-redux'
import { rootState } from 'store'
import ConciergeTooltipRepository from 'repository/Concierge/conciergeTooltipRepository'

const ConciergeTooltipEdit = () => {
  const location = useLocation()
  const history = useHistory()
  if (location.state === undefined || location.state === null || location.state === '' || history.action === 'POP') {
    return <Redirect to="/web/manageConcierge/tooltip/list" />
  }

  const text = useSelector((state: rootState) => state.tooltipInfoReducer.text)

  const setList = async () => {
    await ConciergeTooltipRepository.setList(texts).then((response) => {
      if (response.status === 200) {
        alert('성공')
      } else {
        alert('실패')
      }
    })
  }

  const [texts, setTexts] = useState<string[]>([])
  const textRef = [
    useRef<HTMLTextAreaElement>(),
    useRef<HTMLTextAreaElement>(),
    useRef<HTMLTextAreaElement>(),
    useRef<HTMLTextAreaElement>(),
    useRef<HTMLTextAreaElement>(),
  ]
  const [heights, setHeights] = useState<number[]>([])

  useEffect(() => {
    setTexts(text)
  }, [text])

  useEffect(() => {
    if (texts !== null) {
      setHeights(texts.map((value) => value.split('\n').length))
    }
  }, [texts])

  const onTextareaChange = (text: string, index: number) => {
    const newText = texts.slice()
    if (text.split('\n').length >= 6) {
      alert('최대 5줄 까지 가능합니다')
      newText[index] = text.replace(/\n$/, '')
      setTexts(newText)
      textRef[index].current.value = text.replace(/\n$/, '')
    } else if (text.length > 30) {
      alert('최대 30자 까지 가능합니다')
      newText[index] = text.substring(0, 30)
      setTexts(newText)
      textRef[index].current.value = text.substring(0, 30)
    } else {
      newText[index] = text
      setTexts(newText)
      textRef[index].current.value = text
    }
  }

  return (
    <div className="content">
      <div className="cnt_div">
        <div id="snb">
          <p>컨시어지 관리</p>
          <img src={icoSlash} alt="/" />
          <Link to="/web/manageConcierge/tooltip/list">툴팁 관리</Link>
        </div>
        <div className="stn_top">
          <form action="">
            <div className="cont">
              <h2 className="sta">
                툴팁 관리 <span>수정</span>
              </h2>
            </div>
          </form>
        </div>
        <div className="stn_cont">
          <div className="stn_lineBox">
            <div className="lineBox_cntTitle_wrap">
              <h3 className="lineBox_cnt_title">
                입력정보 <span className="orange_desc">* 툴팁 하나 당 최대 30자 까지만 입력 가능합니다.</span>
              </h3>
              {texts?.map((_, index) => {
                return (
                  <div className="lineBox_cnts type03" key={index.toString()}>
                    <ul className="lineBox_cnts_ul">
                      <li className="lineBox_cnts_tit">{'툴팁 ' + (index + 1)}</li>
                      <li className="lineBox_cnts_cont">
                        <textarea
                          value={_}
                          style={{ whiteSpace: 'pre-line' }}
                          ref={textRef[index]}
                          className="tooltipTextarea"
                          rows={heights[index]}
                          onChange={(e) => {
                            onTextareaChange(e.currentTarget.value, index)
                          }}
                        >
                          {texts[index]}
                        </textarea>
                      </li>
                    </ul>
                  </div>
                )
              })}
            </div>
          </div>
        </div>
        <div className="stn_btm">
          <div className="btm_btn_cont flex_fr">
            <div className="btm_btn_right">
              <Link to="/web/manageConcierge/tooltip/list" className="btn_gray">
                취소
              </Link>
              <Link
                to="/web/manageConcierge/tooltip/list"
                className="btn_blue"
                onClick={(e) => {
                  setList()
                }}
              >
                저장하기
              </Link>
            </div>
          </div>
        </div>
      </div>
    </div>
  )
}

export default ConciergeTooltipEdit
