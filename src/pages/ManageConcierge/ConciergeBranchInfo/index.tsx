import React from 'react'
import { Redirect, Route, Switch } from 'react-router-dom'
import ConciergeBranchList from './conciergeBranchList'
import ConciergeBranchDetail from './conciergeBranchDetail'
import ConciergeBranchEdit from './conciergeBranchEdit'

const ConciergeBranchInfo = () => {
  return (
    <Switch>
      <Route path="/web/manageConcierge/branchInfo/list" component={ConciergeBranchList} />
      <Route path="/web/manageConcierge/branchInfo/detail" component={ConciergeBranchDetail} />
      <Route path="/web/manageConcierge/branchInfo/edit" component={ConciergeBranchEdit} />
      <Redirect exact path="/web/manageConcierge/branchInfo" to="/web/manageConcierge/branchInfo/list" />
    </Switch>
  )
}

export default ConciergeBranchInfo
