import { icoSlash, icoEye, icoEdit } from 'assets/images/icons'
import UrlPaging from 'components/Pagination/UrlPaging'
import PaginationTable from 'components/table/PaginationTable'
import React, { useEffect, useState } from 'react'
import { getPageRowNumber } from 'utils'
import { Link, useHistory } from 'react-router-dom'
import { ConciergeBranchInfo, GuideList } from 'typings/conciergeDTO'
import { useDispatch } from 'react-redux'
import { setBranchDetailInfo, setDetailInfo, setDeviceId } from 'store/module/branchInfoReducer'
import ConciergeBranchRepository from 'repository/Concierge/conciergeBranchRepository'

type ConciergeBranchInfoType = {
  branchName: string
  branchNo: string
  deviceNo: string
}

type ConciergeBranchDetailInfoType = {
  branchName: string
  branchNo: string
  deviceNo: string
  guideList: GuideList[]
}

const ConciergeBranchList = () => {
  const history = useHistory()
  const dispatch = useDispatch()
  const setDeviceIdState = (deviceId: number) => dispatch(setDeviceId(deviceId))
  const setDetailInfoState = (detailInfo: ConciergeBranchInfoType) => dispatch(setDetailInfo(detailInfo))
  const setBranchDetailInfoState = (branchDetailInfo: ConciergeBranchDetailInfoType) =>
    dispatch(setBranchDetailInfo(branchDetailInfo))

  const [items, setItems] = useState<ConciergeBranchInfo[]>()
  const [totalPage, setTotalPage] = useState(10)
  const [currentPage, setCurrentPage] = useState(1)

  const onEditButtonClick = async (value: ConciergeBranchInfo) => {
    setDeviceIdState(value.deviceId)
    setDetailInfoState({
      branchName: value.branchName,
      branchNo: value.branchNo,
      deviceNo: value.deviceNo,
    })
    await ConciergeBranchRepository.getBranchDetail(value.deviceId).then((response) => {
      if (response.status !== 200) {
        alert('에러가 발생하였습니다')
      } else {
        setBranchDetailInfoState({
          branchName: response.data.branchName,
          branchNo: response.data.branchNo,
          deviceNo: response.data.deviceNo,
          guideList: response.data.guideList,
        })
        history.replace({
          pathname: '/web/manageConcierge/branchInfo/edit',
          state: { state: '/web/manageConcierge/branchInfo/edit' },
        })
      }
    })
  }

  const getNoticeList = async () => {
    await ConciergeBranchRepository.getBranchList(currentPage).then((response) => {
      if (response.status === 200)
        setItems(
          response.data.conciergeList.map((conciergeList: any) => {
            return {
              deviceId: conciergeList.deviceId,
              branchName: conciergeList.branchName,
              branchNo: conciergeList.branchNo,
              deviceNo: conciergeList.deviceNo,
              created: conciergeList.created,
            }
          }),
        )
    })
  }

  const getData = async () => {
    await Promise.all([ConciergeBranchRepository.getPages(), ConciergeBranchRepository.getBranchList(1)]).then(
      (responses) => {
        if (responses[0].status === 200) setTotalPage(responses[0].data.pageCnt)
        if (responses[1].status === 200)
          setItems(
            responses[1].data.conciergeList.map((conciergeList: any) => {
              return {
                deviceId: conciergeList.deviceId,
                branchName: conciergeList.branchName,
                branchNo: conciergeList.branchNo,
                deviceNo: conciergeList.deviceNo,
                created: conciergeList.created,
              }
            }),
          )
      },
    )
  }

  useEffect(() => {
    getData()
  }, [])

  useEffect(() => {
    getNoticeList()
  }, [currentPage])

  const getBodys = (bodyItems: ConciergeBranchInfo[] | undefined): JSX.Element[] => {
    if (!bodyItems) {
      return [<tr key={0}></tr>]
    } else {
      return bodyItems.map((value, index) => {
        return (
          <tr key={value.deviceId}>
            <td>{getPageRowNumber(index, currentPage)}</td>
            <td className="padlr24">{value.branchName}</td>
            <td>{value.branchNo}</td>
            <td>{value.deviceNo}</td>
            <td>{value.created}</td>
            <td className="td_btn">
              <Link
                className="btn_detailPage"
                to={{
                  pathname: '/web/manageConcierge/branchInfo/detail',
                  state: { state: '/web/manageConcierge/branchInfo/detail' },
                }}
                onClick={() => {
                  setDeviceIdState(value.deviceId)
                }}
              >
                <span>보기</span>
                <img src={icoEye} alt="보기 아이콘" />
              </Link>
            </td>
            <td className="td_btn">
              <Link
                className="btn_edit"
                to={{
                  pathname: '/web/manageConcierge/branchInfo/edit',
                  state: { state: '/web/manageConcierge/branchInfo/edit' },
                }}
                onClick={async (e) => {
                  e.preventDefault()
                  onEditButtonClick(value)
                }}
              >
                <span>수정하기</span>
                <img src={icoEdit} alt="수정하기 아이콘" />
              </Link>
            </td>
          </tr>
        )
      })
    }
  }

  const headOptions = ['No.', '지점명', '지점 번호', '기기 번호', '등록일', '보기', '수정']

  const heads = (
    <tr>
      {headOptions.map((data) => {
        return (
          <th key={data}>
            <span>{data}</span>
          </th>
        )
      })}
    </tr>
  )

  return (
    <div className="content">
      <div className="cnt_div">
        <div id="snb">
          <p>컨시어지 관리</p>
          <img src={icoSlash} alt="/" />
          <Link to="/web/manageConcierge/branchInfo/list">지점정보 관리</Link>
        </div>
        <div className="stn_top">
          <div className="cont">
            <h2 className="sta">
              지점정보 관리
              <span>LIST</span>
            </h2>
          </div>
        </div>
        <PaginationTable
          tableClass="table_branchInfo"
          heads={heads}
          bodys={getBodys(items)}
          isScrollable
          colLength={headOptions.length}
        />
        <UrlPaging pageLength={totalPage} currentPage={currentPage} onClickPage={setCurrentPage} />
      </div>
    </div>
  )
}

export default ConciergeBranchList
