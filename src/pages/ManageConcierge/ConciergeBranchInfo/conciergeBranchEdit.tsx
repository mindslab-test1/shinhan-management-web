import React, { useEffect, useRef, useState, VFC } from 'react'
import { icoSlash, icoPlus, icoMinus, icoSearch } from 'assets/images/icons'
import { Link, Redirect, useHistory, useLocation } from 'react-router-dom'
import { useSelector } from 'react-redux'
import { rootState } from 'store'
import SelectBox from 'components/SelectBox/selectBox'
import { GuideList, ModifyGuideList } from 'typings/conciergeDTO'
import ConciergeBranchRepository from 'repository/Concierge/conciergeBranchRepository'
import { ETXTBSY } from 'constants'

type ConciergeBranchEditListType = {
  componentData: GuideList
  setComponentData: React.Dispatch<React.SetStateAction<GuideList[]>>
  onComponentButtonClick: (id: number | undefined) => void
}

const ConciergeBranchEditList: VFC<ConciergeBranchEditListType> = ({
  componentData,
  setComponentData,
  onComponentButtonClick,
}) => {
  const options = ['층을 선택해주세요', '1층', '2층', '3층', '4층', '5층']

  const [listComponentData, setListComponentData] = useState(componentData)

  const titleRef = useRef<HTMLTextAreaElement[]>([])
  const contentRef = useRef<HTMLTextAreaElement[]>([])

  useEffect(() => {
    setListComponentData(componentData)
  }, [componentData])

  useEffect(() => {
    titleRef.current = titleRef.current.slice(0, listComponentData.text.length)
    contentRef.current = contentRef.current.slice(0, listComponentData.text.length)
  }, [listComponentData.text])

  const onFloorChange = (e: React.ChangeEvent<HTMLTextAreaElement>) => {
    if (/^-?\d+$/.test(e.target.value) || e.target.value === '-' || e.target.value === '') {
      const newListComponentData = {
        ...listComponentData,
        floor: parseInt(e.target.value, 10),
      }

      setListComponentData(newListComponentData)
      setComponentData((thisComponentData) => {
        const newComponentData = thisComponentData.slice()
        newComponentData[componentData.id] = newListComponentData
        return newComponentData
      })
    } else {
      e.preventDefault()
      e.target.value = e.target.value.slice(0, e.target.value.length - 1)
      alert('형식에 맞춰주세요 ex) 1 2 3 -1 -2')
    }
  }

  const onPlusButtonClick = () => {
    const newComponentText = {
      ...listComponentData,
      text: [
        ...listComponentData.text,
        {
          id: listComponentData.text[listComponentData.text.length - 1].id + 1,
          title: '',
          content: '',
        },
      ],
    }

    setComponentData((thisComponentData) => {
      const newComponentData = thisComponentData
      newComponentData[thisComponentData.findIndex((value) => value.id === componentData.id)] = newComponentText
      return newComponentData
    })

    setListComponentData(newComponentText)
  }

  const onMinusButtonClick = (id: number) => {
    titleRef.current = titleRef.current.slice(0, listComponentData.text.length - 1)
    contentRef.current = contentRef.current.slice(0, listComponentData.text.length - 1)

    const newComponentText = {
      ...listComponentData,
      text: [...listComponentData.text.filter((value) => value.id !== id)],
    }

    setComponentData((thisComponentData) => {
      const newComponentData = thisComponentData
      newComponentData[newComponentData.findIndex((value) => value.id === componentData.id)] = newComponentText
      return newComponentData
    })
    setListComponentData(newComponentText)

    for (let i = 0; i < newComponentText.text.length; i += 1) {
      ;(titleRef.current[i] as HTMLTextAreaElement).value = newComponentText.text[i].title
      ;(contentRef.current[i] as HTMLTextAreaElement).value = newComponentText.text[i].content
    }
  }

  const onTitleChange = (
    e: React.ChangeEvent<HTMLTextAreaElement>,
    value: {
      id: number
      title: string
      content: string
    },
    index: number,
  ) => {
    const newListData = listComponentData
    newListData.text[index] = {
      ...value,
      title: e.target.value,
    }

    setComponentData((thisComponentData) => {
      const newComponentData = thisComponentData.slice()
      newComponentData[thisComponentData.findIndex((value) => value.id === componentData.id)] = newListData
      return newComponentData
    })
    setListComponentData(newListData)
  }

  const onContentChange = (
    e: React.ChangeEvent<HTMLTextAreaElement>,
    value: {
      id: number
      title: string
      content: string
    },
    index: number,
  ) => {
    const newListData = listComponentData
    newListData.text[index] = {
      ...value,
      content: e.target.value,
    }

    setComponentData((thisComponentData) => {
      const newComponentData = thisComponentData.slice()
      newComponentData[thisComponentData.findIndex((value) => value.id === componentData.id)] = newListData
      return newComponentData
    })
    setListComponentData(newListData)
  }

  const toBase64 = (file: File) =>
    new Promise((resolve, reject) => {
      const reader = new FileReader()
      reader.readAsDataURL(file)
      reader.onload = () => resolve(reader.result)
      reader.onerror = (error) => reject(error)
    })

  const onImageUpload = async (e: React.ChangeEvent<HTMLInputElement>) => {
    e.preventDefault()
    const file = e.currentTarget.files[0]

    if (file.size > 20 * 1024 * 1024) {
      alert('파일 사이즈가 너무 큽니다 20MB 아래의 파일을 설정해주세요')
    } else {
      const result = await toBase64(file)
      if (file !== undefined) {
        const newListComponentData = {
          ...listComponentData,
          image: (result as string).split('base64,')[1],
          imageName: file.name,
        }

        setComponentData((thisComponentData) => {
          const newComponentData = thisComponentData.slice()
          newComponentData[thisComponentData.findIndex((value) => value.id === listComponentData.id)] =
            newListComponentData
          return newComponentData
        })
        setListComponentData(newListComponentData)
      }
    }
  }

  const textPlusMinusButton = (index: number, id: number) => {
    if (index === 0) {
      return (
        <button
          className="btn_plus_row"
          type="button"
          onClick={() => {
            onPlusButtonClick()
          }}
        >
          <img src={icoPlus} alt="플러스 아이콘" />
        </button>
      )
    } else {
      return (
        <button
          className="btn_minus_row"
          type="button"
          onClick={() => {
            onMinusButtonClick(id)
          }}
        >
          <img src={icoMinus} alt="마이너스 아이콘" />
        </button>
      )
    }
  }

  return (
    <div className={'stn_lineBox ' + (componentData.id === 0 ? '' : 'add_stn_lineBox')}>
      <div
        className="btn_edit_box"
        onClick={() => {
          onComponentButtonClick(componentData.id)
        }}
      />
      <div className="lineBox_cntTitle_wrap">
        <h3 className="lineBox_cnt_title">안내도 정보</h3>
        <div className="lineBox_cnts">
          <ul className="lineBox_cnts_ul">
            <li className="lineBox_cnts_tit">층수</li>
            <li className="lineBox_cnts_cont">
              <textarea
                defaultValue={componentData.floor}
                data-autoresize
                rows={1}
                onChange={onFloorChange}
                style={{
                  display: 'block',
                  width: '100%',
                  height: 'auto',
                  padding: '6px 16px',
                  boxSizing: 'border-box',
                  lineHeight: '22px',
                  overflow: 'hidden',
                }}
              />
            </li>
          </ul>
        </div>
        <div className="lineBox_cnts">
          <ul className="lineBox_cnts_ul">
            <li className="lineBox_cnts_tit">이미지</li>
            <li className="lineBox_cnts_cont dfac">
              <div className="filebox">
                <span className="upload-name">{listComponentData.imageName}</span>
                <label htmlFor={'fileupload' + componentData.id} className="btn_blueline_icon">
                  변경하기
                  <input
                    type="file"
                    id={'fileupload' + componentData.id}
                    className="upload-hidden"
                    accept="image/jpg, image/jpeg, image/png"
                    onChange={(e) => {
                      onImageUpload(e)
                    }}
                  />
                </label>
                <input
                  type="file"
                  id="fileupload"
                  className="upload-hidden"
                  accept="image/jpg, image/jpeg, image/png"
                />
              </div>
            </li>
          </ul>
        </div>
        <div className="lineBox_cnts">
          <ul className="lineBox_cnts_ul aifs w100p">
            <li className="lineBox_cnts_tit">텍스트</li>
            <li className="lineBox_cnts_cont div_view_table div_viewTable_type02">
              <div className="div_view_tr">
                <div className="div_view_th">타이틀</div>
                <div className="div_view_th">설명글</div>
              </div>
              <div className="div_table_tbody">
                {listComponentData.text.map((value, index) => (
                  <div className="div_view_tr" key={index.toString()}>
                    <div className="div_view_td">
                      <textarea
                        defaultValue={value.title}
                        data-autoresize
                        rows={1}
                        ref={(el) => (titleRef.current[index] = el)}
                        onChange={(e) => {
                          onTitleChange(e, value, index)
                        }}
                      />
                    </div>
                    <div className="div_view_td">
                      <textarea
                        defaultValue={value.content}
                        ref={(el) => (contentRef.current[index] = el)}
                        data-autoresize
                        rows={1}
                        onChange={(e) => {
                          onContentChange(e, value, index)
                        }}
                      />
                    </div>
                    {textPlusMinusButton(index, value.id)}
                  </div>
                ))}
              </div>
            </li>
          </ul>
        </div>
      </div>
    </div>
  )
}

const ConciergeBranchEdit = () => {
  const location = useLocation()
  const history = useHistory()
  if (location.state === undefined || location.state === null || location.state === '' || history.action === 'POP') {
    return <Redirect to="/web/manageConcierge/branchInfo/list" />
  }

  const branchInfo = useSelector((state: rootState) => state.branchInfoReducer.branchInfo)

  const [componentData, setComponentData] = useState<GuideList[]>()

  useEffect(() => {
    if (branchInfo.guideList.length === 0) {
      setComponentData([
        {
          id: 0,
          floor: 0,
          image: '',
          imageName: '',
          created: '1996-06-29',
          text: [
            {
              id: 0,
              title: '',
              content: '',
            },
          ],
        },
      ])
    } else {
      setComponentData(
        branchInfo.guideList.map((value: GuideList, index: number) => {
          return {
            ...value,
            id: index,
            text: value.text.map((textValue, index) => {
              return {
                id: index,
                title: textValue.title,
                content: textValue.content,
              }
            }),
          }
        }),
      )
    }
  }, [branchInfo])

  const onPlusButtonClick = () => {
    const newComponentData: GuideList = {
      id: componentData.length,
      floor: 0,
      image: '',
      imageName: '',
      created: '1996-06-29',
      text: [
        {
          id: 0,
          title: '',
          content: '',
        },
      ],
    }

    setComponentData((componentData) => {
      return [...componentData, newComponentData]
    })
  }

  const onDeleteButtonClick = (id: number) => {
    setComponentData((componentData) => {
      return componentData.filter((value) => value.id !== id)
    })
  }

  const convertComponentDataToModify = (componentData: GuideList[]) => {
    const modifyData: ModifyGuideList[] = componentData.map((value: GuideList) => {
      return {
        floor: value.floor,
        image: value.image,
        extension: value.imageName.split('.')[value.imageName.split('.').length - 1],
        imageName: value.imageName.split('.')[0],
        text: value.text.map((textValue) => {
          return {
            title: textValue.title,
            content: textValue.content,
          }
        }),
      }
    })
    return modifyData
  }

  return (
    <div className="content">
      <div className="cnt_div">
        <div id="snb">
          <p>컨시어지 관리</p>
          <img src={icoSlash} alt="/" />
          <Link to="/web/manageConcierge/branchInfo/list">지점정보 관리</Link>
        </div>
        <div className="stn_top">
          <div className="cont">
            <h2 className="sta">
              지점정보 관리
              <span>수정</span>
            </h2>
          </div>
        </div>
        <div className="stn_cont">
          {componentData?.map((value, index) => {
            return (
              <ConciergeBranchEditList
                key={index.toString()}
                componentData={value}
                setComponentData={setComponentData}
                onComponentButtonClick={index === 0 ? onPlusButtonClick : onDeleteButtonClick}
              />
            )
          })}
        </div>
        <div className="stn_btm">
          <div className="btm_btn_cont flex_fr">
            <div className="btm_btn_right">
              <Link to="/web/manageConcierge/branchInfo/list" className="btn_cancel btn_gray btn_md_close">
                취소
              </Link>
              <button
                type="button"
                className="btn_blue btn_register"
                onClick={async (e) => {
                  if (!componentData.every((value) => value.floor !== 0)) {
                    alert('설정하지 않은 층이 있습니다.')
                    e.preventDefault()
                  } else if (
                    Array.from(new Set(componentData.map((value) => value.floor))).length !== componentData.length
                  ) {
                    alert('중복된 층이 있습니다.')
                    e.preventDefault()
                  } else if (!componentData.every((value) => value.text.every((textValue) => textValue.title !== ''))) {
                    alert('빈 텍스트 제목이 있습니다.')
                    e.preventDefault()
                  } else if (
                    !componentData.every((value) => value.text.every((textValue) => textValue.content !== ''))
                  ) {
                    alert('빈 텍스트 내용이 있습니다.')
                    e.preventDefault()
                  } else if (!componentData.every((value) => value.imageName !== '')) {
                    alert('빈 이미지가 있습니다.')
                    e.preventDefault()
                  } else {
                    await ConciergeBranchRepository.modifyBranchDetail(
                      convertComponentDataToModify(componentData),
                      branchInfo.deviceId,
                    )
                      .then((res) => {
                        if (res.data.code !== 0) {
                          alert(res.data.message)
                        }
                        history.replace('/web/manageConcierge/branchInfo/list')
                      })
                      .catch(() => {
                        alert('지점정보 수정 실패')
                        history.replace('/web/manageConcierge/branchInfo/list')
                      })
                  }
                }}
              >
                저장하기
              </button>
            </div>
          </div>
        </div>
      </div>
    </div>
  )
}

export default ConciergeBranchEdit
