import { icoSlash } from 'assets/images/icons'
import ImageModal from 'components/Modal/ImageModal/ImageModal'
import React, { useEffect, useState, VFC } from 'react'
import { useDispatch, useSelector } from 'react-redux'
import { Link, Redirect, useHistory, useLocation } from 'react-router-dom'
import ConciergeBranchRepository from 'repository/Concierge/conciergeBranchRepository'
import { rootState } from 'store'
import { setBranchDetailInfo } from 'store/module/branchInfoReducer'
import { GuideList } from 'typings/conciergeDTO'

type ConciergeBranchDetailInfoType = {
  branchName: string
  branchNo: string
  deviceNo: string
  guideList: GuideList[]
}
interface BranchComponentProps {
  guideList: GuideList
}

const ConciergeBranchDetailComponent: VFC<BranchComponentProps> = ({ guideList }) => {
  const [isShowModal, setIsShowModal] = useState(false)

  const onCloseModal = () => {
    setIsShowModal(false)
  }

  const onClickImagePreview = () => {
    setIsShowModal(true)
  }

  return (
    <div className="stn_lineBox">
      <ImageModal
        show={isShowModal}
        onCloseModal={onCloseModal}
        image={guideList.image}
        extension={guideList.imageName.split('.')[guideList.imageName.split('.').length - 1]}
      />
      <div className="lineBox_cntTitle_wrap">
        <h3 className="lineBox_cnt_title">안내도 정보</h3>
        <div className="lineBox_cnts">
          <ul className="lineBox_cnts_ul">
            <li className="lineBox_cnts_tit">층수</li>
            <li className="lineBox_cnts_cont">
              <span>{guideList.floor}층</span>
            </li>
          </ul>
        </div>
        <div className="lineBox_cnts">
          <ul className="lineBox_cnts_ul">
            <li className="lineBox_cnts_tit">이미지</li>
            <li className="lineBox_cnts_cont dfac">
              <span>{guideList.imageName}</span>
              <button
                type="button"
                className="btn_blueline_icon btn_modal_open"
                data-popup="view_media"
                onClick={() => {
                  onClickImagePreview()
                }}
              >
                미리보기
              </button>
            </li>
          </ul>
        </div>
        <div className="lineBox_cnts">
          <ul className="lineBox_cnts_ul aifs">
            <li className="lineBox_cnts_tit">텍스트</li>
            <li className="lineBox_cnts_cont div_view_table">
              <div className="div_view_tr">
                <div className="div_view_th">타이틀</div>
                <div className="div_view_th">설명글</div>
              </div>
              <div className="div_table_tbody">
                {guideList.text.map((value) => {
                  return (
                    <div className="div_view_tr">
                      <div className="div_view_td">{value.title}</div>
                      <div className="div_view_td">{value.content}</div>
                    </div>
                  )
                })}
              </div>
            </li>
          </ul>
        </div>
      </div>
    </div>
  )
}

const ConciergeBranchDetail = () => {
  const location = useLocation()
  const history = useHistory()
  if (location.state === undefined || location.state === null || location.state === '' || history.action === 'POP') {
    return <Redirect to="/web/manageConcierge/branchInfo/list" />
  }

  const dispatch = useDispatch()
  const setBranchDetailInfoState = (branchDetailInfo: ConciergeBranchDetailInfoType) =>
    dispatch(setBranchDetailInfo(branchDetailInfo))
  const deviceId = useSelector((state: rootState) => state.branchInfoReducer.branchInfo.deviceId)
  const branchInfo = useSelector((state: rootState) => state.branchInfoReducer.branchInfo)

  const [branchInfoData, setBranchInfoData] = useState(branchInfo)

  const getData = async (deviceId: number) => {
    await ConciergeBranchRepository.getBranchDetail(deviceId).then((response) => {
      setBranchDetailInfoState({
        branchName: response.data.branchName,
        branchNo: response.data.branchNo,
        deviceNo: response.data.deviceNo,
        guideList: response.data.guideList,
      })
      setBranchInfoData({
        branchName: response.data.branchName,
        branchNo: response.data.branchNo,
        deviceNo: response.data.deviceNo,
        guideList: response.data.guideList,
      })
    })
  }

  const branchComponent = branchInfoData?.guideList.map((guideList: GuideList, index: number) => {
    return <ConciergeBranchDetailComponent guideList={guideList} key={index.toString()} />
  })

  // 최초에 데이터를 넣기 위한 useEffect
  useEffect(() => {
    getData(deviceId)
  }, [deviceId])

  return (
    <div className="content">
      <div className="cnt_div">
        <div id="snb">
          <p>컨시어지 관리</p>
          <img src={icoSlash} alt="/" />
          <Link to="/web/manageConcierge/branchInfo/list">지점정보 관리</Link>
        </div>
        <div className="stn_top">
          <div className="cont">
            <h2 className="sta">
              지점정보 관리
              <span>LIST</span>
            </h2>
          </div>
        </div>
        <div className="stn_cont">
          <div className="stn_lineBox">
            <div className="lineBox_cntTitle_wrap">
              <h3 className="lineBox_cnt_title">지점선택</h3>
              <div className="lineBox_cnts">
                <ul className="lineBox_cnts_ul">
                  <li className="lineBox_cnts_tit">지점명</li>
                  <li className="lineBox_cnts_cont">
                    <span>{branchInfoData?.branchName ?? ''}</span>
                  </li>
                </ul>
                <ul className="lineBox_cnts_ul">
                  <li className="lineBox_cnts_tit">지점 번호</li>
                  <li className="lineBox_cnts_cont">
                    <span>{branchInfoData?.branchNo ?? ''}</span>
                  </li>
                </ul>
                <ul className="lineBox_cnts_ul">
                  <li className="lineBox_cnts_tit">기기 번호</li>
                  <li className="lineBox_cnts_cont">
                    <span>{branchInfoData?.deviceNo ?? ''}</span>
                  </li>
                </ul>
              </div>
            </div>
          </div>
          {branchComponent}
        </div>
        <div className="stn_btm">
          <div className="btm_btn_cont flex_fr">
            <div className="btm_btn_right">
              <Link
                className="btn_blue btn_register"
                to={{
                  pathname: '/web/manageConcierge/branchInfo/edit',
                  state: { state: '/web/manageConcierge/branchInfo/edit' },
                }}
              >
                수정하기
              </Link>
              <Link className="btn_cancel btn_gray btn_md_close" to="/web/manageConcierge/branchInfo/list">
                목록으로
              </Link>
            </div>
          </div>
        </div>
      </div>
    </div>
  )
}

export default ConciergeBranchDetail
