import React, { useState } from 'react'
import { Redirect, Route, Switch, useLocation } from 'react-router-dom'
import ConciergeNotice from './ConciergeNotice'
import ConciergeStandby from './ConciergeStandby'
import ConciergeBranchInfo from './ConciergeBranchInfo'
import ConciergeTooltip from './ConciergeTooltip'
import ConciergeMenu from './ConciergeMenu'

const ManageConcierge = () => {
  return (
    <div id="contents">
      <Switch>
        <Route path="/web/manageConcierge/notice" component={ConciergeNotice} />
        <Route path="/web/manageConcierge/standby" component={ConciergeStandby} />
        <Route path="/web/manageConcierge/branchInfo" component={ConciergeBranchInfo} />
        <Route path="/web/manageConcierge/tooltip" component={ConciergeTooltip} />
        <Route path="/web/manageConcierge/menu" component={ConciergeMenu} />
        <Redirect exact path="/web/manageConcierge/" to="/web/manageConcierge/notice" />
      </Switch>
    </div>
  )
}

export default ManageConcierge
