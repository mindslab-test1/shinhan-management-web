import { icoEdit, icoEye, icoSort } from 'assets/images/icons'
import PaginationTable from 'components/table/PaginationTable'
import qs from 'qs'
import React, { useEffect, VFC } from 'react'
import { useDispatch, useSelector } from 'react-redux'
import { Link, useLocation } from 'react-router-dom'
import { rootState } from 'store'
import { getPageRowNumber } from 'utils'
import { setDeviceInfoDetail } from 'store/module/DeviceInfo/deviceDetailInfoReducer'
import { setDeviceInfoChecked } from 'store/module/DeviceInfo/deviceListInfoReducer'
import { DeviceListInfo } from 'typings/deviceDTO'

interface ContentsProps {
  currentPage: number
}

const Contents: VFC<ContentsProps> = ({ currentPage }) => {
  const location = useLocation()
  const query = qs.parse(location.search, {
    ignoreQueryPrefix: true,
  })
  const device = parseInt(query.device as string, 10)

  const selector = useSelector
  const dispatch = useDispatch()
  const setDeviceInfoCheckedState = (isChecked: boolean[]) => dispatch(setDeviceInfoChecked(isChecked))
  const setDeviceInfoDetailState = (deviceDetailInfo: DeviceListInfo) => dispatch(setDeviceInfoDetail(deviceDetailInfo))
  const deviceListInfo: DeviceListInfo[] = selector((state: rootState) => state.deviceListInfoReducer.deviceListInfo)
  const isChecked: boolean[] = selector((state: rootState) => state.deviceListInfoReducer.isChecked)

  const handleCheckClick = (index: number) => {
    setDeviceInfoCheckedState(isChecked.map((c, i) => (i === index ? !c : c)))
  }

  const handleAllCheck = () => {
    if (isAllChecked) {
      setDeviceInfoCheckedState(isChecked.map((c) => false))
    } else if (isChecked.every((c) => !c)) {
      setDeviceInfoCheckedState(isChecked.map((c) => true))
    } else if (isChecked.filter((c) => c).length < Math.ceil(isChecked.length / 2)) {
      setDeviceInfoCheckedState(isChecked.map((c) => true))
    } else {
      setDeviceInfoCheckedState(isChecked.map((c) => false))
    }
  }

  useEffect(() => {
    const tmpCheckList = []
    if (isChecked !== undefined) {
      for (let i = 0; i < isChecked.length; i += 1) {
        tmpCheckList.push(false)
      }
      setDeviceInfoCheckedState(tmpCheckList)
    }
  }, [])

  const isAllChecked = isChecked?.every((x) => x)

  const headOptions = ['No.', '지점명', '지점번호', '기기번호', '모델', '의상', '배경', '기기담당자', '보기', '수정']

  const head = (
    <tr>
      <th key={0}>
        {/* <input type="checkbox" id="cbAll" name="dummy" readOnly checked={isAllChecked} onClick={handleAllCheck} />
        <label htmlFor="cbAll" /> */}
      </th>
      {headOptions.map((value) => {
        return (
          <th key={value}>
            <div>
              <span>{value}</span>
              {value !== '보기' && value !== '수정' && <img src={icoSort} alt="" />}
            </div>
          </th>
        )
      })}
    </tr>
  )

  const body = deviceListInfo.map((value, index) => {
    return (
      <tr key={index.toString()}>
        <td>
          {/* <input
            type="checkbox"
            id={'cb' + index.toString()}
            name="cbDelete"
            checked={isChecked[index]}
            readOnly
            onClick={() => handleCheckClick(index)}
          />
          <label htmlFor={'cb' + index.toString()} /> */}
        </td>
        <td>{getPageRowNumber(index, currentPage)}</td>
        <td>{value.branchNm}</td>
        <td>{value.branchNo}</td>
        <td>{value.deviceNo}</td>
        <td>{value.avatarNm}</td>
        <td>{value.outfitNm}</td>
        <td>{value.backgroundNm}</td>
        <td>{value.manager}</td>
        <td className="td_btn">
          <Link
            onClick={() => {
              setDeviceInfoDetailState(value)
            }}
            to={{
              pathname: '/web/manageDevice/detail',
              search: '?device=' + device + '&id=' + value.aiHumanId,
              state: {
                branchNo: value.branchNo,
                branchOffice: value.branchNm,
              },
            }}
            className="btn_detailPage"
          >
            <span>보기</span>
            <img src={icoEye} alt="수정하기" />
          </Link>
        </td>
        <td className="td_btn">
          <Link
            onClick={() => {
              setDeviceInfoDetailState(value)
            }}
            to={{
              pathname: '/web/manageDevice/edit',
              search: '?device=' + device + '&id=' + value.aiHumanId,
              state: {
                state: '/web/manageDevice/edit?device=' + device + '&id=' + value.aiHumanId,
              },
            }}
            className="btn_edit"
          >
            <span>수정하기</span>
            <img src={icoEdit} alt="수정하기 아이콘" />
          </Link>
        </td>
      </tr>
    )
  })

  useEffect(() => {}, [deviceListInfo])

  return (
    <div className="stn_cont">
      <div className="table_cont">
        <PaginationTable
          tableClass="table_human01"
          heads={head}
          bodys={body}
          isScrollable={false}
          colLength={headOptions.length + 1}
        />
      </div>
    </div>
  )
}

export default Contents
