import qs from 'qs'
import React, { VFC } from 'react'
import { shallowEqual, useSelector } from 'react-redux'
import { Link, useLocation } from 'react-router-dom'
import { rootState } from 'store'

interface BottomProps {}

const Bottom: VFC<BottomProps> = React.memo(() => {
  const location = useLocation()
  const query = qs.parse(location.search, {
    ignoreQueryPrefix: true,
  })
  const device = parseInt(query.device as string, 10)

  const selector = useSelector
  const isChecked: boolean[] = selector(
    (state: rootState) => state.deviceListInfoReducer.isChecked,
    (prev, next) => {
      return prev.every((c: boolean) => !c) === next.every((c: boolean) => !c)
    },
  )

  return (
    <div className="stn_btm">
      <div className="btm_btn_cont">
        <button type="button" className="btn_delete" disabled>
          {/* diabled = {isChecked.every((c) => !c)} */}
          삭제하기
        </button>
        <Link
          to={{
            pathname: '/web/manageDevice/enroll',
            search: '?device=' + device,
            state: {
              state: '/web/manageDevice/enroll?device=' + device,
            },
          }}
          className="btn_blue btn_register"
        >
          새로운 인공인간 등록하기
        </Link>
      </div>
    </div>
  )
})

export default Bottom
