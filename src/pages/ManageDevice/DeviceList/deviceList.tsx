import UrlPaging from 'components/Pagination/UrlPaging'
import qs from 'qs'
import React, { useCallback, useState, useEffect } from 'react'
import { Link, useLocation } from 'react-router-dom'
import DeviceListRepository from 'repository/Device/deviceListRepository'
import { DeviceListInfo, DeviceListReturnInfo } from 'typings/deviceDTO'
import { convertDeviceName, getTotalPage, getPageRowNumber } from 'utils'
import { icoSlash, icoSort, icoEye, icoEdit } from 'assets/images/icons'
import { setDeviceInfoChecked, setDeviceInfoList } from 'store/module/DeviceInfo/deviceListInfoReducer'
import { setDeviceInfoDetail } from 'store/module/DeviceInfo/deviceDetailInfoReducer'
import { rootState } from 'store'
import { useDispatch, useSelector } from 'react-redux'
import PaginationTable from 'components/table/PaginationTable'
import Top from './top'
import Contents from './contents'
import Bottom from './bottom'

const DeviceList = React.memo(() => {
  const dispatch = useDispatch()
  const selector = useSelector
  const deviceListInfo: DeviceListInfo[] = selector(
    (state: rootState) => state.deviceListInfoReducer.deviceListInfo,
    (prev, next) => {
      if (prev !== undefined && prev !== [] && prev !== null) {
        for (let i = 0; i < prev.length; i += 1) {
          if (JSON.stringify(prev[i] !== JSON.stringify(next[i]))) {
            return false
          }
        }
      }
      return true
    },
  )
  const setDeviceInfoCheckedState = (isChecked: boolean[]) => dispatch(setDeviceInfoChecked(isChecked))
  const setDeviceInfoDetailState = (deviceDetailInfo: DeviceListInfo) => dispatch(setDeviceInfoDetail(deviceDetailInfo))

  const [pageLength, setPageLength] = useState(0)
  const location = useLocation()
  const [currentPage, setCurrentPage] = useState(1)
  const query = qs.parse(location.search, {
    ignoreQueryPrefix: true,
  })
  const device = parseInt(query.device as string, 10)
  const [items, setItems] = useState<DeviceListInfo[]>()

  useEffect(() => {
    setCurrentPage(1)
  }, [device])

  const getData = useCallback(
    async (
      currentPage: number,
      deviceType: number,
      paramKey?: string,
      paramValue?: string,
      releaseDateStart?: string,
      releaseDateEnd?: string,
    ) => {
      await DeviceListRepository.getDeviceList(currentPage, deviceType).then((res) => {
        if (res.status === 200 && res.data.result && res.data.code === 200) {
          setItems(res.data.data.list)
          setDeviceInfoCheckedState(Array(res.data.data.list.length).fill(false))
          setPageLength(res.data.data.listTotalCnt)
        }
      })
    },
    [currentPage, device],
  )

  const onSearchClick = useCallback(
    (paramKey?: string, paramValue?: string, releaseDateStart?: string, releaseDateEnd?: string) => {
      getData(currentPage, device, paramKey, paramValue, releaseDateStart, releaseDateEnd)
    },
    [currentPage, device],
  )

  const onPageClick = (index: number) => {
    getData(index, device)
    setCurrentPage(index)
  }

  /*   useEffect(() => {
    getData(currentPage, device)
  }, []) */

  useEffect(() => {
    getData(currentPage, device)
  }, [currentPage, device])

  const headOptions = [
    'No.',
    '지점명',
    '지점번호',
    '기기번호',
    '모델',
    '의상',
    '배경',
    /* '기기담당자', */ '보기',
    '수정',
  ]

  const head = (
    <tr>
      <th key={0}>
        {/* <input type="checkbox" id="cbAll" name="dummy" readOnly checked={isAllChecked} onClick={handleAllCheck} />
        <label htmlFor="cbAll" /> */}
      </th>
      {headOptions.map((value) => {
        return (
          <th key={value}>
            <div>
              <span>{value}</span>
              {value !== '보기' && value !== '수정' && <img src={icoSort} alt="" />}
            </div>
          </th>
        )
      })}
    </tr>
  )

  const getBodys = (bodyItems: DeviceListInfo[] | undefined): JSX.Element[] => {
    if (typeof bodyItems === 'undefined' || bodyItems === null || bodyItems.length < 1) {
      return [
        <tr key={1}>
          <td colSpan={headOptions.length} className="nodata_td">
            데이터가 없습니다.
          </td>
        </tr>,
      ]
    } else {
      return bodyItems.map((value, index) => {
        return (
          <tr key={index.toString()}>
            <td>
              {/* <input
            type="checkbox"
            id={'cb' + index.toString()}
            name="cbDelete"
            checked={isChecked[index]}
            readOnly
            onClick={() => handleCheckClick(index)}
          />
          <label htmlFor={'cb' + index.toString()} /> */}
            </td>
            <td>{getPageRowNumber(index, currentPage)}</td>
            <td>{value.branchNm}</td>
            <td>{value.branchNo}</td>
            <td>{value.deviceNo}</td>
            <td>{value.avatarNm}</td>
            <td>{value.outfitNm}</td>
            <td>{value.backgroundNm}</td>
            {/* <td>{value.manager}</td> */}
            <td className="td_btn">
              <Link
                onClick={() => {
                  setDeviceInfoDetailState(value)
                }}
                to={{
                  pathname: '/web/manageDevice/detail',
                  search: '?device=' + device + '&id=' + value.aiHumanId,
                  state: {
                    branchNo: value.branchNo,
                    branchOffice: value.branchNm,
                  },
                }}
                className="btn_detailPage"
              >
                <span>보기</span>
                <img src={icoEye} alt="수정하기" />
              </Link>
            </td>
            <td className="td_btn">
              <Link
                onClick={() => {
                  setDeviceInfoDetailState(value)
                }}
                to={{
                  pathname: '/web/manageDevice/edit',
                  search: '?device=' + device + '&id=' + value.aiHumanId,
                  state: {
                    state: '/web/manageDevice/edit?device=' + device + '&id=' + value.aiHumanId,
                  },
                }}
                className="btn_edit"
              >
                <span>수정하기</span>
                <img src={icoEdit} alt="수정하기 아이콘" />
              </Link>
            </td>
          </tr>
        )
      })
    }
  }

  return (
    <div className="content">
      <div className="cnt_div">
        <div id="snb">
          <p>기기 관리</p>
          <img src={icoSlash} alt="/" />
          <Link to={'/web/manageDevice/list?device=' + parseInt(query.device as string, 10)}>
            {convertDeviceName(parseInt(query.device as string, 10))}
          </Link>
        </div>
        <Top onSearchClick={onSearchClick} />
        <div className="stn_cont">
          <div className="table_cont">
            <PaginationTable
              tableClass="table_human01"
              heads={head}
              bodys={getBodys(items)}
              isScrollable={false}
              colLength={headOptions.length + 1}
            />
          </div>
        </div>
        <div className="stn_btm">
          <div className="btm_btn_cont">
            <button type="button" className="btn_delete" disabled>
              {/* diabled = {isChecked.every((c) => !c)} */}
              삭제하기
            </button>
            <Link
              to={{
                pathname: '/web/manageDevice/enroll',
                search: '?device=' + device,
                state: {
                  state: '/web/manageDevice/enroll?device=' + device,
                },
              }}
              className="btn_blue btn_register"
            >
              새로운 인공인간 등록하기
            </Link>
          </div>
        </div>
      </div>
      <UrlPaging pageLength={getTotalPage(pageLength, 10)} currentPage={currentPage} onClickPage={onPageClick} />
    </div>
  )
})

export default DeviceList
