import { icoSearch } from 'assets/images/icons'
import Calendar from 'components/Calendar/Calendar'
import React, { useCallback, useRef, useState, VFC } from 'react'
import { convertDeviceName } from 'utils'
import { useSelector, useDispatch } from 'react-redux'
import { rootState } from 'store'
import { setParamKeyIndex, setDate } from 'store/module/DeviceInfo/deviceSearchReducer'
import { DateRangePicker } from 'rsuite'
import SelectBox from 'components/SelectBox/selectBox'
import { useLocation } from 'react-router-dom'
import qs from 'qs'

interface topProps {
  onSearchClick: (
    paramKey?: string | undefined,
    paramValue?: string | undefined,
    releaseDateStart?: string | undefined,
    releaseDateEnd?: string | undefined,
  ) => void
}

const Top: VFC<topProps> = React.memo(({ onSearchClick }) => {
  type searchDate = {
    startDate: Date
    endDate: Date
  }

  const location = useLocation()
  const query = qs.parse(location.search, {
    ignoreQueryPrefix: true,
  })
  const device = parseInt(query.device as string, 10)

  const [isShowModal, setIsShowModal] = useState(false)

  const dispatch = useDispatch()
  const setParamKeyIndexState = (paramKeyIndex: number) => dispatch(setParamKeyIndex(paramKeyIndex))
  const scenarioVersionInfo = useSelector((state: rootState) => state.scenarioVersionReducer.scenarioVersionInfo)

  const paramKeyIndex: number = useSelector((state: rootState) => state.deviceSearchReducer.paramKeyIndex)
  const date: searchDate = useSelector((state: rootState) => state.deviceSearchReducer.searchDate)

  const paramValueRef = useRef(null)
  const options = ['분류를 선택해주세요', '지점코드', '지점명', '인공인간ID', '배포버전', '담당자']

  const matchParamKey = useCallback((paramKeyIndex: number) => {
    switch (paramKeyIndex) {
      case 1: {
        return 'deviceNo'
      }
      case 2: {
        return 'branchName'
      }
      case 3: {
        return 'aiHumanUuid'
      }
      case 4: {
        return 'version'
      }
      case 5: {
        return 'manager'
      }
      default: {
        return 'null'
      }
    }
  }, [])

  return (
    <div className="stn_top">
      <form action="">
        <div className="cont">
          <h2 className="sta">
            {convertDeviceName(device)} <span>LIST</span>
          </h2>

          <div className="search_wrap">
            {/* <SelectBox
              items={options}
              enableGrayText
              onItemClick={(index) => {
                setParamKeyIndexState(index)
              }}
            /> */}
            {/* <div className="search_box">
              <input
                type="text"
                placeholder="검색어를 입력해주세요"
                ref={paramValueRef}
                onKeyPress={(e) => {
                  if (e.key === 'Enter') {
                    e.preventDefault()
                    if (paramKeyIndex === 0 || (paramValueRef.current as unknown as HTMLInputElement).value === '') {
                      // onSearchClick(
                      // undefined,
                      // undefined,
                      // date.startDate.toISOString().split('T')[0],
                      // date.endDate.toISOString().split('T')[0],
                      // )
                    } else {
                      // onSearchClick(
                      // matchParamKey(paramKeyIndex),
                      // (paramValueRef.current as unknown as HTMLInputElement).value,
                      // date.startDate.toISOString().split('T')[0],
                      // date.endDate.toISOString().split('T')[0],
                      // )
                    }
                  }
                }}
              />
              <button
                type="button"
                onClick={() => {
                  if (paramKeyIndex === 0 || (paramValueRef.current as unknown as HTMLInputElement).value === '') {
                    // onSearchClick(
                    //   undefined,
                    //   undefined,
                    //   date.startDate.toISOString().split('T')[0],
                    //   date.endDate.toISOString().split('T')[0],
                    // )
                  } else {
                    // onSearchClick(
                    //   matchParamKey(paramKeyIndex),
                    //   (paramValueRef.current as unknown as HTMLInputElement).value,
                    //   date.startDate.toISOString().split('T')[0],
                    //   date.endDate.toISOString().split('T')[0],
                    // )
                  }
                }}
              >
                <img src={icoSearch} alt="검색 아이콘" />
              </button>
            </div> */}
          </div>

          {/* <div className="top_calendar">
            <label htmlFor="delivery_period" className="delivery_period">
              배포 기간별 조회
            </label>
            <Calendar date={date} reducerAction={setDate} />
          </div> */}
        </div>
      </form>
    </div>
  )
})

export default Top
