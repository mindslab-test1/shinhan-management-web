import React, { useEffect, useRef, useState } from 'react'
import { icoRight24px, icoSearch, icoSlash } from 'assets/images/icons'
import { Link, Redirect, useLocation, useHistory } from 'react-router-dom'
import qs from 'qs'
import { useSelector } from 'react-redux'
import { rootState } from 'store'
import { convertDeviceName } from 'utils'
import { Swiper, SwiperSlide } from 'swiper/react/swiper-react'
import { Navigation } from 'swiper'
import AiHumanOutlineRepository from 'repository/Device/aiHumanOutlineRepository'
import DeviceManagerModal from 'components/Modal/DeviceManagerModal/deviceManagerModal'
import DeviceResetModal from 'components/Modal/DeviceResetModal/deviceResetModal'
import { videoHeight } from 'assets/Videos'
import SaveModal from 'components/Modal/SaveModal/saveModal'
import DeviceBranchInfoModal from 'components/Modal/DeviceBranchInfoModal/deviceBranchInfoModal'
import DeviceListRepository from 'repository/Device/deviceListRepository'
import ScenarioVersionListModal from 'components/Modal/ScenarioVersionListModal/scenarioVersionListModal'

const DeviceEnroll = () => {
  const location = useLocation()
  const query = qs.parse(location.search, {
    ignoreQueryPrefix: true,
  })
  const history = useHistory()
  const device = parseInt(query.device as string, 10)

  if (location.state === undefined || location.state === null || location.state === '') {
    return <Redirect to={'/web/manageDevice/list?device=' + (Number.isNaN(device) ? 1 : device)} />
  }

  const deviceNoRef = useRef<HTMLInputElement>(null)

  const [isShowManagerModal, setIsShowManagerModal] = useState(false)
  const [isShowResetModal, setIsShowResetModal] = useState(false)
  const [isShowSaveModal, setIsShowSaveModal] = useState(false)
  const [isShowBranchModal, setIsShowBranchModal] = useState(false)
  const [isShowVersionModal, setIsShowVersionModal] = useState(false)

  const [branchNm, setBranchNm] = useState<string>()
  const [branchNo, setBranchNo] = useState<string>()
  const [deviceNo, setDeviceNo] = useState<string>()
  const [manager, setManager] = useState<string>()

  const [avatarImgUrl, setAvatarImgUrl] = useState<string[]>()
  const [backgroundImgUrl, setBackgroundImgUrl] = useState<string[]>()
  const [outfitImgUrlList, setOutfitImgUrlList] = useState<Map<number, string[]>>(new Map())
  const [avatarIdList, setAvatarIdList] = useState<string[]>([])
  const [backgroundIdList, setBackgroundIdList] = useState<string[]>()
  const [outfitIdList, setOutfitIdList] = useState<Map<number, string[]>>(new Map())
  const [avatarId, setAvatarId] = useState<number>()
  const [outfitId, setOutfitId] = useState<number>()
  const [backgroundId, setBackgroundId] = useState<number>()

  const [swiperAvatarIndex, setSwiperAvatarIndex] = useState<number>(0)
  const [outfitIndex, setOutfitIndex] = useState<number>(0)
  const [backgroundIndex, setBackgroundIndex] = useState<number>(0)

  const scenarioVersionInfo = useSelector((state: rootState) => state.scenarioVersionReducer.scenarioVersionInfo)

  const getData = async (deviceType: number) => {
    await Promise.all([AiHumanOutlineRepository.getAvatar(), AiHumanOutlineRepository.getBackground(deviceType)]).then(
      (response) => {
        if (response[0].status === 200 && response[0].data.result && response[0].data.code === 200) {
          setAvatarImgUrl(response[0].data.data.list.map((avatarValue: any) => avatarValue.avatarImgUrl) ?? '')
          setAvatarId(response[0].data.data.list[0].avatarId)
          response[0].data.data.list.map(async (response0Value: any, index: number) => {
            setAvatarIdList((avatarIdList) => [...avatarIdList, response0Value.avatarId])
            AiHumanOutlineRepository.getOutfit(response0Value.avatarId).then((res) => {
              if (res.status === 200 && res.data.result && res.data.code === 200) {
                setOutfitIdList((outfitIdList) => {
                  setOutfitId(res.data.data.list[0].outfitId)
                  const newMap = new Map(outfitIdList)
                  newMap.set(
                    response0Value.avatarId,
                    res.data.data.list.map((newMapValue: any) => newMapValue.outfitId),
                  )
                  return newMap
                })
                setOutfitImgUrlList((outfitImgUrlList) => {
                  const newMap = new Map(outfitImgUrlList)
                  newMap.set(
                    response0Value.avatarId,
                    res.data.data.list.map((newMapValue: any) => newMapValue.outfitImgUrl),
                  )
                  return newMap
                })
              }
            })
          })
        }
        if (response[1].status === 200 && response[1].data.result && response[1].data.code === 200) {
          setBackgroundId(response[1].data.data.list[0].backgroundId)
          setBackgroundImgUrl(response[1].data.data.list.map((value: any) => value.backgroundImgUrl) ?? '')
          setBackgroundIdList(response[1].data.data.list.map((value: any) => value.backgroundId) ?? '')
        }
      },
    )
  }

  const swiperAvatarElement = avatarImgUrl?.map((value, index) => (
    <SwiperSlide
      onClick={() => {
        setSwiperAvatarIndex(index)
        setAvatarId(parseInt(avatarIdList[index], 10))
      }}
    >
      <div className={'swiper-slide btn_select ' + (index === swiperAvatarIndex ? 'select' : '')}>
        <button type="button">
          <img src={value} alt="모델" />
        </button>
      </div>
    </SwiperSlide>
  ))

  const outfitElement = outfitImgUrlList?.get(avatarId ?? parseInt(avatarIdList[0], 10))?.map((value, index) => (
    <button
      type="button"
      className={'btn_clothes btn_select ' + (index === outfitIndex ? 'select' : '')}
      onClick={() => {
        setOutfitIndex(index)
        setOutfitId(parseInt(outfitIdList.get(avatarId ?? parseInt(avatarIdList[0], 10))[index], 10))
      }}
    >
      <img src={value} alt="의상" />
    </button>
  ))

  const backgroundElement = backgroundImgUrl?.map((value, index) => (
    <button
      type="button"
      className={'btn_bgC btn_select ' + (backgroundIndex === index ? 'select' : '')}
      onClick={() => {
        setBackgroundIndex(index)
        setBackgroundId(parseInt(backgroundIdList[index], 10))
      }}
    >
      <img src={value} alt="배경" />
    </button>
  ))

  const onClickSaveButton = (e: React.MouseEvent<HTMLButtonElement, MouseEvent>) => {
    if (deviceNo === '' || !deviceNo) {
      e.preventDefault()
      alert('기기 번호를 입력해주세요')
    } else if (!RegExp('^[0-9]+$').test(deviceNo)) {
      e.preventDefault()
      alert('기기 번호를 올바르게 입력해주세요')
    } else if (branchNo === '' || !branchNo) {
      e.preventDefault()
      alert('지점 정보를 입력해주세요')
    } /* else if (manager === '' || !manager) {
      e.preventDefault()
      alert('담당자를 입력해주세요')
    } */ else if (!avatarId) {
      e.preventDefault()
      alert('모델을 설정해주세요')
    } else if (!outfitId) {
      e.preventDefault()
      alert('의상을 선택해주세요')
    } else if (!backgroundId) {
      e.preventDefault()
      alert('배경을 선택해주세요')
    } else {
      setIsShowSaveModal(true)
    }
  }

  const onSaveModal = async () => {
    await DeviceListRepository.enrollDeviceList({
      avatarId: avatarId,
      backgroundId: backgroundId,
      outfitId: outfitId,
      branchNo: branchNo.toString(),
      deviceNo: deviceNo.toString(),
      deviceType: device,
      versionId: scenarioVersionInfo.id,
    })
      .then((res) => {
        if (res.data.result !== true) {
          alert(res.data.msg)
        }
        history.goBack()
      })
      .catch(() => {
        alert('기기 등록 실패')
        history.goBack()
      })
  }

  const onResetModal = () => {
    setSwiperAvatarIndex(null)
    setBackgroundIndex(null)
    setOutfitIndex(null)
    setBranchNm(null)
    setBranchNo(null)
    setDeviceNo(null)
    setManager(null)
    deviceNoRef.current.value = ''
  }

  const onSaveBranchModal = (branchNm: string, branchNo: string) => {
    setBranchNm(branchNm)
    setBranchNo(branchNo)
    setIsShowBranchModal(false)
  }

  useEffect(() => {
    getData(device)
  }, [device])

  return (
    <div className="content">
      <DeviceManagerModal
        show={isShowManagerModal}
        device={device}
        onCloseModal={() => {
          setIsShowManagerModal(false)
        }}
        onSelectModal={(manager: string) => {
          setIsShowManagerModal(false)
          setManager(manager)
        }}
      />
      <DeviceResetModal
        show={isShowResetModal}
        device={device}
        onCloseModal={() => {
          setIsShowResetModal(false)
        }}
        onSelectModal={() => {
          setIsShowResetModal(false)
          onResetModal()
        }}
      />
      <SaveModal
        show={isShowSaveModal}
        onCloseModal={() => {
          setIsShowSaveModal(false)
        }}
        onSubmit={() => {
          onSaveModal()
          setIsShowSaveModal(false)
        }}
        branchNo={branchNo}
        branchNm={branchNm}
      />
      <DeviceBranchInfoModal
        show={isShowBranchModal}
        onCloseModal={() => setIsShowBranchModal(false)}
        onSetModal={onSaveBranchModal}
      />
      <ScenarioVersionListModal
        show={isShowVersionModal}
        onCloseModal={() => {
          setIsShowVersionModal(false)
        }}
      />
      <div className="cnt_div">
        <div id="snb">
          <p>기기 관리</p>
          <img src={icoSlash} alt="/" />
          <Link to={'/web/manageDevice/list?device=' + parseInt(query.device as string, 10)}>
            {convertDeviceName(device)}
          </Link>
        </div>

        <div className="stn_top">
          <div className="cont">
            <h2 className="sta">
              {convertDeviceName(device)} <span>등록</span>
            </h2>
          </div>
        </div>
        <div className="stn_cont">
          <form action="">
            <div className="stn_cont_top">
              <div className="stn_contTop_div">
                <h3 className="stn_cntTop_tit">등록 기기</h3>
                <ul className="stn_cntTop_ul">
                  <li className="cnt_top_tit">기기종류</li>
                  <li className="cnt_top_desc">{convertDeviceName(device)}</li>
                  <li className="cnt_top_tit">기기 번호</li>
                  <li className="cnt_top_desc lineBox_cnts_cont">
                    <input
                      ref={deviceNoRef}
                      onChange={(e) => setDeviceNo(e.currentTarget.value)}
                      defaultValue={deviceNo}
                      type="text"
                      style={{ padding: '8px 8px', height: 'auto', width: '66px' }}
                    ></input>
                  </li>
                </ul>
              </div>
              <div className="stn_contTop_div">
                <h3 className="stn_cntTop_tit">지점선택</h3>
                <div className="disflex">
                  <ul className="stn_cntTop_ul stn_cntTop_branch">
                    <li className="cnt_top_tit">지점명</li>
                    <li className="cnt_top_desc btn_modal_open" data-popup="select_branch">
                      <input type="text" className="input_disabled w208" disabled defaultValue={branchNm} />
                    </li>
                    <li className="cnt_top_tit">지점 번호</li>
                    <li className="cnt_top_desc btn_modal_open" data-popup="select_branch">
                      <input type="text" className="input_disabled w64 tac" disabled defaultValue={branchNo} />
                    </li>
                  </ul>
                  <button
                    type="button"
                    className="ml36 btn_blueline_icon btn_modal_open"
                    onClick={() => setIsShowBranchModal(true)}
                  >
                    지점선택 <img src={icoSearch} alt="검색 아이콘" />
                  </button>
                </div>
              </div>
              <div className="stn_contTop_div">
                {/* <h3 className="stn_cntTop_tit">기기 담당자</h3>
                <div className="disflex">
                  <ul className="stn_cntTop_ul">
                    <li className="cnt_top_desc btn_modal_open" data-popup="device_manager">
                      <input type="text" className="input_disabled w168" disabled defaultValue={manager} />
                    </li>
                  </ul>
                  <button
                    type="button"
                    className="ml36 btn_blueline_icon btn_modal_open"
                    data-popup="device_manager"
                    onClick={() => {
                      setIsShowManagerModal(true)
                    }}
                  >
                    기기 담당자 선택 <img src={icoSearch} alt="검색 아이콘" />
                  </button>
                </div> */}
                <button
                  type="button"
                  className="btn_blueline_icon btn_modal_open"
                  // style={{ marginLeft: '12px' }}
                  data-popup="select_scenario_version"
                  onClick={() => setIsShowVersionModal(true)}
                >
                  버전선택({scenarioVersionInfo.version})
                </button>
              </div>
            </div>
          </form>

          <div className="stn_video_div">
            <div className="video_cont">
              <div className="video_area">
                <div className="vid">
                  <video src={videoHeight} className="video" controls></video>
                </div>
                <div className="video_btn_cont">
                  <button type="button">인사</button>
                  <button type="button">죄송</button>
                  <button type="button">양손 내리기</button>
                  <button type="button">좌측 상단</button>
                  <button type="button">하단 가르키기</button>
                </div>
              </div>
              <div className="video_setting_bg">
                <h3 className="cont_tit">배경 색상 설정</h3>
                <div className="bg_btn_cont">{backgroundElement}</div>
              </div>
            </div>
            <div className="video_setting_cont">
              <div className="vs_top">
                <h3 className="cont_tit">인공인간 외형 설정</h3>
                <h4 className="cont_title">모델</h4>
                <div className="model_slide">
                  <div className="model_slide_div">
                    <Swiper
                      modules={[Navigation]}
                      spaceBetween={12}
                      slidesPerView={3}
                      slidesPerGroup={3}
                      loop={avatarImgUrl?.length > 3}
                      loopFillGroupWithBlank
                      navigation={{
                        prevEl: null,
                        nextEl: '.btn_model_next',
                      }}
                      onSlideChange={(e) => {
                        setSwiperAvatarIndex(Math.floor(e.realIndex / 3) * 3)
                      }}
                    >
                      {swiperAvatarElement}
                    </Swiper>
                    {avatarImgUrl?.length > 3 ?? (
                      <button type="button" className="btn_model_next">
                        <img src={icoRight24px} alt="오른쪽 화살표 아이콘" />
                      </button>
                    )}
                  </div>
                </div>

                <h4 className="cont_title">의상</h4>
                <div className="clothes_cont">
                  <div className="img_wrap img_wrap_man select">{outfitElement}</div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div className="stn_btm">
          <div className="btm_btn_cont">
            <Link to={'/web/manageDevice/list?device=' + device} className="btn_undo btn_modal_open">
              목록으로
            </Link>
            <div className="btm_btn_right">
              <button
                type="button"
                className="btn_reset btn_modal_open"
                data-popup="go_out"
                onClick={() => setIsShowResetModal(true)}
              >
                초기화
              </button>
              <button
                type="button"
                className="btn_blue btn_save btn_modal_open"
                onClick={(e) => {
                  onClickSaveButton(e)
                }}
              >
                저장하기
              </button>
            </div>
          </div>
        </div>
      </div>
    </div>
  )
}

export default DeviceEnroll
