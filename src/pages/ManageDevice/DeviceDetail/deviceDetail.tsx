import { icoSlash } from 'assets/images/icons'
import qs from 'qs'
import React, { useEffect, useState } from 'react'
import { useSelector } from 'react-redux'
import { Link, Redirect, useLocation } from 'react-router-dom'
import AiHumanOutlineRepository from 'repository/Device/aiHumanOutlineRepository'
import { rootState } from 'store'
import { DeviceListInfo } from 'typings/deviceDTO'
import { convertDeviceName } from 'utils'
import { videoHeight } from 'assets/Videos'

const DeviceDetail = () => {
  const deviceDetailInfo: DeviceListInfo = useSelector(
    (state: rootState) => state.deviceDetailInfoReducer.deviceDetailInfo,
  )

  const location = useLocation()
  const query = qs.parse(location.search, {
    ignoreQueryPrefix: true,
  })
  const device = parseInt(query.device as string, 10)
  if (location.state === undefined || location.state === null || location.state === '') {
    return <Redirect to={'/web/manageDevice/list?device=' + (Number.isNaN(device) ? 1 : device)} />
  }

  const [avatarImgUrl, setAvatarImgUrl] = useState<string>()
  const [backgroundImgUrl, setBackgroundImgUrl] = useState<string>()
  const [outfitImgUrl, setOutfitImgUrl] = useState<string>()

  const scenarioVersionInfo = useSelector((state: rootState) => state.scenarioVersionReducer.scenarioVersionInfo)

  const getData = async (avatarId: number, deviceType: number, deviceDetailInfo: DeviceListInfo) => {
    if (deviceDetailInfo.avatarNm !== '' && deviceDetailInfo.backgroundNm !== '' && deviceDetailInfo.outfitNm !== '') {
      await Promise.all([
        AiHumanOutlineRepository.getAvatar(),
        AiHumanOutlineRepository.getBackground(deviceType),
        AiHumanOutlineRepository.getOutfit(avatarId),
      ]).then((response) => {
        if (response[0].status === 200 && response[0].data.result && response[0].data.code === 200) {
          setAvatarImgUrl(
            response[0].data.data.list.find((value: any) => value.avatarId === deviceDetailInfo.avatarId)
              ?.avatarImgUrl ?? '',
          )
        }
        if (response[1].status === 200 && response[1].data.result && response[1].data.code === 200) {
          setBackgroundImgUrl(
            response[1].data.data.list.find((value: any) => value.backgroundId === deviceDetailInfo.backgroundId)
              ?.backgroundImgUrl ?? '',
          )
        }
        if (response[2].status === 200 && response[2].data.result && response[2].data.code === 200) {
          setOutfitImgUrl(
            response[2].data.data.list.find((value: any) => value.outfitId === deviceDetailInfo.outfitId)
              ?.outfitImgUrl ?? '',
          )
        }
      })
    }
  }

  useEffect(() => {
    getData(deviceDetailInfo.avatarId, device, deviceDetailInfo)
  }, [deviceDetailInfo])

  return (
    <div className="content">
      <div className="cnt_div">
        <div id="snb">
          <p>기기 관리</p>
          <img src={icoSlash} alt="/" loading="lazy" />
          <Link to={'/web/manageDevice/list?device=' + parseInt(query.device as string, 10)}>
            {convertDeviceName(device)}
          </Link>
        </div>

        <div className="stn_top">
          <div className="cont">
            <h2 className="sta">
              AI 컨시어지 <span>상세보기</span>
            </h2>
          </div>
        </div>
        <div className="stn_cont">
          <div className="stn_cont_top">
            <div className="stn_contTop_div">
              <h3 className="stn_cntTop_tit">지점 기기 정보</h3>
              <div className="cntTop_ul_div">
                <ul className="stn_cntTop_ul">
                  <li className="cnt_top_tit">기기종류</li>
                  <li className="cnt_top_desc">{convertDeviceName(device)}</li>
                  <li className="cnt_top_tit">기기 번호</li>
                  <li className="cnt_top_desc">{deviceDetailInfo.deviceNo}</li>
                </ul>
                <ul className="stn_cntTop_ul">
                  <li className="cnt_top_tit">지점명</li>
                  <li className="cnt_top_desc">{deviceDetailInfo.branchNm}</li>
                  <li className="cnt_top_tit">지점 번호</li>
                  <li className="cnt_top_desc">{deviceDetailInfo.branchNo}</li>
                </ul>
                <ul className="stn_cntTop_ul">
                  <li className="cnt_top_tit">담당자</li>
                  <li className="cnt_top_desc">{deviceDetailInfo.manager}</li>
                </ul>
                <ul className="stn_cntTop_ul">
                  <li className="cnt_top_tit">버전</li>
                  <li className="cnt_top_desc">{scenarioVersionInfo.version}</li>
                </ul>
              </div>
            </div>
          </div>

          <div className="stn_video_div video_detail">
            <div className="video_cont">
              <div className="video_area">
                <div className="vid">
                  <video src={videoHeight} className="video" controls></video>
                </div>
                <div className="video_btn_cont">
                  <button type="button">인사</button>
                  <button type="button">죄송</button>
                  <button type="button">양손 내리기</button>
                  <button type="button">좌측 상단</button>
                  <button type="button">하단 가르키기</button>
                </div>
              </div>
            </div>
            <div className="video_setting_cont">
              <div className="vs_top">
                <h3 className="cont_tit">인공인간 외형</h3>

                <div className="detail_cont">
                  <h4 className="cont_title">모델</h4>
                  <div className="detail_cnt select_model">
                    {avatarImgUrl !== '' && <img src={avatarImgUrl} alt="남자 모델" />}
                  </div>
                </div>

                <div className="detail_cont">
                  <h4 className="cont_title">의상</h4>
                  <div className="detail_cnt select_clothes">
                    {outfitImgUrl !== '' && <img src={outfitImgUrl} alt="의상 1" />}
                  </div>
                </div>

                <div className="detail_cont">
                  <h4 className="cont_title">배경색</h4>
                  <div className="detail_cnt select_bg">
                    {backgroundImgUrl !== '' && <img className="detail_bg" src={backgroundImgUrl} alt="배경" />}
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div className="stn_btm">
          <div className="btm_btn_cont">
            <Link to={'/web/manageDevice/list?device=' + device} className="btn_gray">
              목록으로
            </Link>
            <div className="btm_btn_right">
              <Link
                to={{
                  pathname: '/web/manageDevice/edit',
                  search: '?device=' + device,
                  state: {
                    state: '/web/manageDevice/edit?device=' + device,
                  },
                }}
                className="btn_blue"
              >
                수정하기
              </Link>
            </div>
          </div>
        </div>
      </div>
    </div>
  )
}

export default DeviceDetail
