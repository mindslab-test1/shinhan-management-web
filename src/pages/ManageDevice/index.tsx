import React, { useState } from 'react'
import { Switch, Route, Redirect } from 'react-router-dom'
import DeviceEdit from './DeviceEdit/deviceEdit'
import DeviceList from './DeviceList/deviceList'
import DeviceEnroll from './DeviceEnroll/deviceEnroll'
import DeviceDetail from './DeviceDetail/deviceDetail'

const ManageDevice = () => {
  return (
    <div id="contents">
      <Switch>
        <Redirect exact path="/web/manageDevice/" to="/web/manageDevice/list?device=1" />
        <Route path="/web/manageDevice/list" component={DeviceList} />
        <Route path="/web/manageDevice/edit" component={DeviceEdit} />
        <Route path="/web/manageDevice/enroll" component={DeviceEnroll} />
        <Route path="/web/manageDevice/detail" component={DeviceDetail} />
      </Switch>
    </div>
  )
}

export default ManageDevice
