import React from 'react'
import { Redirect, Route, Switch } from 'react-router-dom'
import DistributeList from './DistributeList/distributeList'
import DistributeDetailSystem from './DistributeDetailSystem/distributeDetailSystem'
import DistributeDetailScenario from './DistributeDetailScenario/distributeDetailScenario'

const ManageVideoDistribute = () => {
  return (
    <div id="contents">
      <Switch>
        <Route path="/web/manageVideoDistribute/list" component={DistributeList} />
        <Route path="/web/manageVideoDistribute/detail/system" component={DistributeDetailSystem} />
        <Route path="/web/manageVideoDistribute/detail/scenario" component={DistributeDetailScenario} />
        <Redirect exact path="/web/manageVideoDistribute/" to="/web/manageVideoDistribute/list" />
      </Switch>
    </div>
  )
}

export default ManageVideoDistribute
