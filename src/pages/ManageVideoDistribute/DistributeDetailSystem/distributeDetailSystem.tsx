import React, { useEffect, useState, useCallback, useRef } from 'react'
import { Link, useLocation, useHistory } from 'react-router-dom'
import { useSelector, useDispatch } from 'react-redux'
import { rootState } from 'store'
import { setDtbtVideoCnt } from 'store/module/videoDistributionReducer'
import { setParamKeyIndex } from 'store/module/distributeSearchReducer'
import qs from 'qs'
import { ReleasedVideoInfo } from 'typings/videoDistributeDTO'
import VideoDistributeRepository from 'repository/VideoDistribute/videoDistributeRepository'
import { icoDelivery, icoSlash, icoSearch } from 'assets/images/icons'
import {
  usePolling,
  convertVideoDistributeStatus,
  convertVideoAvatarName,
  convertScenarioName,
  convertDeviceName,
  numberFormatComma,
  getPageRowNumber,
  getTotalPage,
} from 'utils'
import UrlPaging from 'components/Pagination/UrlPaging'
import PaginationTable from 'components/table/PaginationTable'
import ScenarioVersionListModal from 'components/Modal/ScenarioVersionListModal/scenarioVersionListModal'
import Spinner from 'components/Modal/spinner/spinner'
import SelectBox from 'components/SelectBox/selectBox'

type dtbtVideoCnt = {
  dtbtWaitCnt: number
  dtbtReqCnt: number
  dtbtIngCnt: number
  dtbtCompCnt: number
}

const DistributeDetailSystem = () => {
  const history = useHistory()
  const location = useLocation()
  const query = qs.parse(location.search, { ignoreQueryPrefix: true })
  const deviceType = parseInt(query.device as string, 10)

  const dispatch = useDispatch()
  const setDtbtVideoCntState = (dtbtVideoCnt: dtbtVideoCnt) => dispatch(setDtbtVideoCnt(dtbtVideoCnt))
  const dtbtInfo = useSelector((state: rootState) => state.videoDistributionReducer.dtbtInfo)
  const scenarioVersionInfo = useSelector((state: rootState) => state.scenarioVersionReducer.scenarioVersionInfo)

  const [currentPage, setCurrentPage] = useState(1)
  const [items, setItems] = useState<ReleasedVideoInfo[]>()
  const [totalPage, setTotalPage] = useState(0)
  const [isShowModal, setIsShowModal] = useState(false)

  if (location.state === undefined || location.state === null || location.state === '' || history.action === 'POP') {
    history.goBack()
  }

  /* const setParamKeyIndexState = (paramKeyIndex: number) => dispatch(setParamKeyIndex(paramKeyIndex))
  const onSearchClick = (paramkey?: string | undefined, paramValue?: string | undefined) => {}
  const paramKeyIndex: number = useSelector((state: rootState) => state.distributeSearchReducer.paramKeyIndex)
  const paramValueRef = useRef(null)
  const options = [
    '분류를 선택해주세요',
    '지점명',
    '지점번호',
    '기기번호',
    '타입',
    '배포된 영상 수',
    '배포상태',
    '배포완료일시',
  ]

  const matchParamKey = useCallback((paramKeyIndex: number) => {
    switch (paramKeyIndex) {
      case 1: {
        return 'deviceNo'
      }
      case 2: {
        return 'branchName'
      }
      case 3: {
        return 'aiHumanUuid'
      }
      case 4: {
        return 'version'
      }
      case 5: {
        return 'manager'
      }
      default: {
        return 'null'
      }
    }
  }, []) */

  const getData = useCallback(async () => {
    await Promise.all([
      VideoDistributeRepository.getDistributeList(
        currentPage,
        dtbtInfo.deviceId,
        deviceType,
        dtbtInfo.scenarioType,
        scenarioVersionInfo.id,
      ),
      VideoDistributeRepository.getDistributeTotalPage(
        dtbtInfo.deviceId,
        deviceType,
        dtbtInfo.scenarioType,
        scenarioVersionInfo.id,
      ),
    ]).then((responses) => {
      if (responses[0].status === 200) setItems(responses[0].data)
      if (responses[1].status === 200) setTotalPage(responses[1].data)
    })
  }, [currentPage, deviceType, dtbtInfo, scenarioVersionInfo])

  const getVideoCntData = useCallback(async () => {
    await Promise.all([
      VideoDistributeRepository.getDistributeCountInfo(
        dtbtInfo.deviceId,
        deviceType,
        dtbtInfo.scenarioType,
        scenarioVersionInfo.id,
      ),
    ]).then((responses) => {
      if (responses[0].status === 200) setDtbtVideoCntState(responses[0].data)
    })
  }, [deviceType, dtbtInfo, scenarioVersionInfo])

  useEffect(() => {
    getData()
  }, [currentPage, deviceType, dtbtInfo, scenarioVersionInfo])

  useEffect(() => {
    getVideoCntData()
  }, [deviceType, dtbtInfo, scenarioVersionInfo])

  /*   usePolling(() => {
    getData()
    getVideoCntData()
  }, 5000) */

  const getBodys = (bodyItems: ReleasedVideoInfo[] | undefined): JSX.Element[] => {
    if (!bodyItems || bodyItems.length < 1) {
      return [
        <tr key={1}>
          <td colSpan={headOptions.length} className="nodata_td">
            데이터가 없습니다.
          </td>
        </tr>,
      ]
    } else {
      return bodyItems.map((data, idx) => {
        if (data.status === 0) {
          return (
            <tr key={data.videoId.toString() + data.scenarioType.toString()}>
              <td>{getPageRowNumber(idx, currentPage)}</td>
              <td>{convertScenarioName(data.scenarioType, 'ko')}</td>
              <td className="padlr24 tal">{data.intent}</td>
              <td className="padlr24 tal">{data.utter}</td>
              <td>{convertVideoAvatarName(data.avatarDisplayNm)}</td>
              <td>{data.outfitDisplayNm}</td>
              <td className="padlr24">{data.gestureDisplayNm}</td>
              <td>{numberFormatComma(data.size)}</td>
              <td>{convertVideoDistributeStatus(data.status)}</td>
              <td>{data.created}</td>
              <td colSpan={2} className="td_btn">
                <button
                  type="button"
                  className="btn_detailPage"
                  onClick={() => {
                    indiDistribution(data.videoId)
                  }}
                >
                  <span>배포</span>
                  <img src={icoDelivery} alt="배포 아이콘" />
                </button>
              </td>
            </tr>
          )
        } else {
          return (
            <tr key={data.videoId.toString() + data.scenarioType.toString()}>
              <td>{getPageRowNumber(idx, currentPage)}</td>
              <td>{convertScenarioName(data.scenarioType, 'ko')}</td>
              <td className="padlr24 tal">{data.intent}</td>
              <td className="padlr24 tal">{data.utter}</td>
              <td>{convertVideoAvatarName(data.avatarDisplayNm)}</td>
              <td>{data.outfitDisplayNm}</td>
              <td className="padlr24">{data.gestureDisplayNm}</td>
              <td>{numberFormatComma(data.size)}</td>
              <td>{convertVideoDistributeStatus(data.status)}</td>
              <td>{data.created}</td>
              <td colSpan={2} className="td_btn"></td>
            </tr>
          )
        }
      })
    }
  }

  const headOptions = [
    'No.',
    '타입',
    '의도',
    '답변 텍스트(화면표시)',
    '모델',
    '복장',
    '행동',
    '영상 용량',
    '상태',
    '배포완료일시',
    '배포',
  ]

  const heads = (
    <tr>
      {headOptions.map((data) => {
        return (
          <th key={data}>
            <span>{data}</span>
          </th>
        )
      })}
    </tr>
  )

  // 개별 배포
  const indiDistribution = (videoId: number) => {
    VideoDistributeRepository.saveIndiDistribution(dtbtInfo.deviceId, deviceType, dtbtInfo.scenarioType, 1, videoId)
      .then((res) => {
        if (res.data === 1) {
          getData()
          getVideoCntData()
          alert('개별배포 요청 완료')
        } else {
          alert('개별배포 요청 실패')
        }
      })
      .catch(() => {
        alert('개별배포 요청 실패')
      })
  }

  // 기기별 전체 배포
  const totalDistribution = () => {
    VideoDistributeRepository.saveTotalDistribution(
      dtbtInfo.deviceId,
      deviceType,
      dtbtInfo.scenarioType,
      0,
      scenarioVersionInfo.id,
    )
      .then((res) => {
        if (res.data === 1) {
          getData()
          getVideoCntData()
          setIsShowModal(false)
          alert('전체배포 요청 완료')
        } else {
          alert('전체배포 요청 실패')
        }
      })
      .catch(() => {
        setIsShowModal(false)
        alert('전체배포 요청 실패')
      })
  }

  /* 전체배포 로딩 스피너
  기기별 전체배포는 응답시간이 짧아서 지움
  나중에 길어지면 쓰면됨 */
  /* if (isShowModal) {
    return (
      <Spinner 
        show={isShowModal}
        onCloseModal={() => {
        setIsShowModal(false)
        }} 
        type="spin" 
        color="rgb(24, 31, 103)"
        message='전체 배포 진행중입니다' 
      />
    )
  } */

  return (
    <div className="content">
      <div className="cnt_div">
        <div id="snb">
          <p>배포 관리</p>
          <img src={icoSlash} alt="/" />
          <Link to={'/web/manageVideoDistribute/list?device=' + deviceType}>{convertDeviceName(deviceType)}</Link>
        </div>
        <div className="stn_top">
          <div className="cont">
            <h2 className="sta">
              {convertDeviceName(deviceType)}
              <span>LIST</span>
            </h2>
            <div className="link_tab">
              <ul className="link_cnt">
                <li className="on">
                  <Link
                    to={{
                      pathname: '/web/manageVideoDistribute/detail/system',
                      state: {
                        state: '/web/manageVideoDistribute/detail/system?device=' + deviceType,
                      },
                      search: '?device=' + deviceType,
                    }}
                  >
                    시스템 발화
                  </Link>
                </li>
                {/* <li>
                <Link to={'/web/manageVideoDistribute/scenario?device=' + parseInt(query.device as string, 10)}>
                  시나리오 발화
                </Link>
              </li> */}
              </ul>
            </div>
            <div className="search_wrap">
              <button
                type="button"
                className="btn_blueline_icon btn_modal_open"
                style={{
                  marginRight: '8px',
                }}
                data-popup="select_scenario_version"
                onClick={() => setIsShowModal(true)}
              >
                버전선택({scenarioVersionInfo.version})
              </button>
              {/* <SelectBox
                items={options}
                enableGrayText
                onItemClick={(index) => {
                  setParamKeyIndexState(index)
                }}
              />
              <div className="search_box">
                <input
                  type="text"
                  placeholder="검색어를 입력해주세요"
                  ref={paramValueRef}
                  onKeyPress={(e) => {
                    if (e.key === 'Enter') {
                      e.preventDefault()
                      if (paramKeyIndex !== 0 || (paramValueRef.current as unknown as HTMLInputElement).value === '') {
                        onSearchClick(undefined, undefined)
                      } else {
                        onSearchClick(
                          matchParamKey(paramKeyIndex),
                          (paramValueRef.current as unknown as HTMLInputElement).value,
                        )
                      }
                    }
                  }}
                />
                <button
                  type="button"
                  onClick={() => {
                    if (paramKeyIndex === 0 || (paramValueRef.current as unknown as HTMLInputElement).value === '') {
                      onSearchClick(undefined, undefined)
                    } else {
                      onSearchClick(
                        matchParamKey(paramKeyIndex),
                        (paramValueRef.current as unknown as HTMLInputElement).value,
                      )
                    }
                  }}
                >
                  <img src={icoSearch} alt="검색 아이콘" />
                </button>
              </div> */}
            </div>
          </div>
        </div>
        <ScenarioVersionListModal
          show={isShowModal}
          onCloseModal={() => {
            setIsShowModal(false)
          }}
        />
        <div className="stn_cont">
          <div className="stn_lineBox type02 dfac">
            <div className="lineBox_cntTitle_wrap">
              <h3 className="lineBox_cnt_title">지점 기기 정보</h3>
              <div className="lineBox_cnts">
                <ul className="lineBox_cnts_ul">
                  <li className="lineBox_cnts_tit">지점명</li>
                  <li className="lineBox_cnts_cont">
                    <span>{dtbtInfo.branchName}</span>
                  </li>
                </ul>
                <ul className="lineBox_cnts_ul">
                  <li className="lineBox_cnts_tit">지점 번호</li>
                  <li className="lineBox_cnts_cont">
                    <span>{dtbtInfo.branchNo}</span>
                  </li>
                </ul>
                <ul className="lineBox_cnts_ul">
                  <li className="lineBox_cnts_tit">기기 번호</li>
                  <li className="lineBox_cnts_cont">
                    <span>{dtbtInfo.deviceNo}</span>
                  </li>
                </ul>
              </div>
            </div>
            <div className="lineBox_cntTitle_wrap">
              <h3 className="lineBox_cnt_title">
                배포 영상 수 (
                {dtbtInfo.dtbtVideoCnt.dtbtWaitCnt +
                  dtbtInfo.dtbtVideoCnt.dtbtReqCnt +
                  dtbtInfo.dtbtVideoCnt.dtbtIngCnt +
                  dtbtInfo.dtbtVideoCnt.dtbtCompCnt}
                )
              </h3>
              <div className="lineBox_cnts">
                <ul className="lineBox_cnts_ul">
                  <li className="lineBox_cnts_tit">배포 대기중</li>
                  <li className="lineBox_cnts_cont">
                    <span>{dtbtInfo.dtbtVideoCnt.dtbtWaitCnt}</span>
                  </li>
                </ul>
                <ul className="lineBox_cnts_ul">
                  <li className="lineBox_cnts_tit">배포 요청</li>
                  <li className="lineBox_cnts_cont">
                    <span>{dtbtInfo.dtbtVideoCnt.dtbtReqCnt}</span>
                  </li>
                </ul>
                <ul className="lineBox_cnts_ul">
                  <li className="lineBox_cnts_tit">배포중</li>
                  <li className="lineBox_cnts_cont">
                    <span>{dtbtInfo.dtbtVideoCnt.dtbtIngCnt}</span>
                  </li>
                </ul>
                <ul className="lineBox_cnts_ul">
                  <li className="lineBox_cnts_tit">배포 완료</li>
                  <li className="lineBox_cnts_cont">
                    <span>{dtbtInfo.dtbtVideoCnt.dtbtCompCnt}</span>
                  </li>
                </ul>
              </div>
            </div>
          </div>
          <PaginationTable
            tableClass="table_dtbt_detail"
            heads={heads}
            bodys={getBodys(items)}
            isScrollable
            colLength={headOptions.length}
          />
        </div>
      </div>
      <div className="stn_btm">
        <div className="btm_btn_cont flex_fr">
          <div className="btm_btn_right">
            <Link to={'/web/manageVideoDistribute/list?device=' + deviceType} className="btn_gray">
              목록으로
            </Link>
            <button
              type="button"
              className="btn_blue btn_register"
              onClick={() => {
                totalDistribution()
              }}
            >
              전체 배포
            </button>
          </div>
        </div>
      </div>
      <UrlPaging pageLength={getTotalPage(totalPage, 10)} currentPage={currentPage} onClickPage={setCurrentPage} />
    </div>
  )
}

export default DistributeDetailSystem
