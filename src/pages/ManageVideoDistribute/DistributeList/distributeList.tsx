import React, { useEffect, useState, useCallback, useRef } from 'react'
import { Link, useLocation } from 'react-router-dom'
import { useDispatch, useSelector } from 'react-redux'
import { rootState } from 'store'
import { setDtbtInfo } from 'store/module/videoDistributionReducer'
import { setParamKeyIndex } from 'store/module/distributeSearchReducer'
import qs from 'qs'
import { VideoDistributeGroupInfo } from 'typings/videoDistributeDTO'
import VideoDistributeRepository from 'repository/VideoDistribute/videoDistributeRepository'
import VideoCreateRepository from 'repository/VideoCreate/videoCreateRepository'
import { icoEye, icoSearch, icoSlash } from 'assets/images/icons'
import {
  usePolling,
  convertVideoDistributeStatus,
  convertScenarioName,
  convertDeviceName,
  getPageRowNumber,
  getTotalPage,
} from 'utils'
import UrlPaging from 'components/Pagination/UrlPaging'
import PaginationTable from 'components/table/PaginationTable'
import ScenarioVersionListModal from 'components/Modal/ScenarioVersionListModal/scenarioVersionListModal'
import Spinner from 'components/Modal/spinner/spinner'
import SelectBox from 'components/SelectBox/selectBox'

type dtbtInfo = {
  deviceId: number
  branchName: string
  branchNo: number
  deviceNo: number
  scenarioType: number
  totalCnt: number
  cnt: number
  status: number
  dtbtCreated: string
  dtbtVideoCnt: dtbtVideoCnt
}

type dtbtVideoCnt = {
  dtbtWaitCnt: number
  dtbtReqCnt: number
  dtbtIngCnt: number
  dtbtCompCnt: number
}

const DistributeList = () => {
  const location = useLocation()
  const query = qs.parse(location.search, { ignoreQueryPrefix: true })
  const deviceType = parseInt(query.device as string, 10)

  const dispatch = useDispatch()
  const setDtbtInfoState = (dtbtInfo: dtbtInfo) => dispatch(setDtbtInfo(dtbtInfo))
  const scenarioVersionInfo = useSelector((state: rootState) => state.scenarioVersionReducer.scenarioVersionInfo)

  const [items, setItems] = useState<VideoDistributeGroupInfo[]>()
  const [totalPage, setTotalPage] = useState(0)
  const [currentPage, setCurrentPage] = useState(1)
  const [isShowModal, setIsShowModal] = useState(false)
  const [isShowSpinner, setIsShowSpinner] = useState(false)
  const [systemVideoCnt, setSystemVideoCnt] = useState(0)
  const [scenarioVideoCnt, setScenarioVideoCnt] = useState(0)

  /*   const setParamKeyIndexState = (paramKeyIndex: number) => dispatch(setParamKeyIndex(paramKeyIndex))
  const onSearchClick = (paramkey?: string | undefined, paramValue?: string | undefined) => {}
  const paramKeyIndex: number = useSelector((state: rootState) => state.distributeSearchReducer.paramKeyIndex)
  const paramValueRef = useRef(null)
  const options = [
    '분류를 선택해주세요',
    '지점명',
    '지점번호',
    '기기번호',
    '타입',
    '배포된 영상 수',
    '배포상태',
    '배포완료일시',
  ]

  const matchParamKey = useCallback((paramKeyIndex: number) => {
    switch (paramKeyIndex) {
      case 1: {
        return 'deviceNo'
      }
      case 2: {
        return 'branchName'
      }
      case 3: {
        return 'aiHumanUuid'
      }
      case 4: {
        return 'version'
      }
      case 5: {
        return 'manager'
      }
      default: {
        return ''
      }
    }
  }, []) */

  const getData = useCallback(async () => {
    await Promise.all([
      VideoDistributeRepository.getDistributeGroupList(currentPage, deviceType, scenarioVersionInfo.id),
      VideoDistributeRepository.getDistributeGroupTotalPage(deviceType, scenarioVersionInfo.id),
    ]).then((responses) => {
      if (responses[0].status === 200) setItems(responses[0].data)
      if (responses[1].status === 200) setTotalPage(responses[1].data)
    })
  }, [currentPage, scenarioVersionInfo])

  const getVideoCntData = useCallback(
    async (deviceType: number) => {
      await Promise.all([
        VideoCreateRepository.getCreatedGroupTotalPage(deviceType, 0, 2, scenarioVersionInfo.id),
        VideoCreateRepository.getCreatedGroupTotalPage(deviceType, 1, 2, scenarioVersionInfo.id),
      ]).then((responses) => {
        if (responses[0].status === 200) setScenarioVideoCnt(responses[0].data)
        if (responses[1].status === 200) setSystemVideoCnt(responses[1].data)
      })
    },
    [scenarioVersionInfo],
  )

  useEffect(() => {
    getData()
  }, [currentPage, scenarioVersionInfo])

  useEffect(() => {
    getVideoCntData(deviceType)
  }, [scenarioVersionInfo])

  /*   usePolling(() => {
    getData()
    getVideoCntData(deviceType)
  }, 5000) */

  const getBodys = (bodyItems: VideoDistributeGroupInfo[] | undefined): JSX.Element[] => {
    if (!bodyItems || bodyItems.length < 1) {
      return [
        <tr key={1}>
          <td colSpan={headOptions.length} className="nodata_td">
            데이터가 없습니다.
          </td>
        </tr>,
      ]
    } else {
      return bodyItems.map((data, idx) => {
        const scenarioEnName = data.scenarioType === 1 ? 'system' : 'scenario'

        return (
          <tr key={data.deviceId.toString() + data.scenarioType.toString()}>
            <td>{getPageRowNumber(idx, currentPage)}</td>
            <td className="padlr24">{data.branchName}</td>
            <td>{data.branchNo}</td>
            <td>{data.deviceNo}</td>
            <td>{convertScenarioName(data.scenarioType, 'ko')}</td>
            <td>{data.cnt}</td>
            <td>{convertVideoDistributeStatus(data.status)}</td>
            <td>{data.dtbtCreated}</td>
            <td colSpan={2} className="td_btn">
              <Link
                className="btn_detailPage"
                to={{
                  pathname: '/web/manageVideoDistribute/detail/' + scenarioEnName,
                  state: {
                    state: '/web/manageVideoDistribute/detail/' + scenarioEnName + '?device=' + deviceType,
                  },
                  search: '?device=' + deviceType,
                }}
                onClick={() => {
                  setDtbtInfoState({
                    deviceId: data.deviceId,
                    branchName: data.branchName,
                    branchNo: data.branchNo,
                    deviceNo: data.deviceNo,
                    scenarioType: data.scenarioType,
                    totalCnt: data.totalCnt,
                    cnt: data.cnt,
                    status: data.status,
                    dtbtCreated: data.dtbtCreated,
                    dtbtVideoCnt: {
                      dtbtWaitCnt: 0,
                      dtbtReqCnt: 0,
                      dtbtIngCnt: 0,
                      dtbtCompCnt: 0,
                    },
                  })
                }}
              >
                <span>보기</span>
                <img src={icoEye} alt="보기 아이콘" />
              </Link>
            </td>
          </tr>
        )
      })
    }
  }

  const headOptions = [
    'No.',
    '지점명',
    '지점번호',
    '기기 번호',
    '타입',
    '배포완료 영상 수',
    '배포상태',
    '배포완료일시',
    '보기',
  ]

  const heads = (
    <tr>
      {headOptions.map((data) => {
        return (
          <th key={data}>
            <span>{data}</span>
          </th>
        )
      })}
    </tr>
  )

  // 전체 배포 버튼 클릭
  /*   const onTotalAllDtbtButtonClick = () => {
    setIsShowModal(true)
  } */

  // 전체 배포 수행
  const totalAllDistribution = () => {
    if (window.confirm('모든 기기에 전체 배포를 진행하시겠습니까?')) {
      setIsShowSpinner(true)
      VideoDistributeRepository.saveTotalAllDistribution(deviceType, 2, scenarioVersionInfo.id)
        .then((res) => {
          if (res.data > 1) {
            alert('전체배포 요청 완료')
          } else {
            alert('전체배포 요청 실패')
          }
          setIsShowSpinner(false)
          getData()
        })
        .catch(() => {
          alert('전체배포 요청 실패')
          setIsShowSpinner(false)
        })
    }
  }
  if (isShowSpinner) {
    return (
      <Spinner
        show={isShowSpinner}
        onCloseModal={() => {
          setIsShowSpinner(false)
        }}
        type="spin"
        color="rgb(24, 31, 103)"
        message="전체 배포 진행중입니다"
      />
    )
  }

  return (
    <div className="content">
      <div className="cnt_div">
        <div id="snb">
          <p>배포 관리</p>
          <img src={icoSlash} alt="/" />
          <Link to={'/web/manageVideoDistribute/list?device=' + deviceType}>{convertDeviceName(deviceType)}</Link>
        </div>
        <div className="stn_top">
          <div className="cont">
            <h2 className="sta">
              {convertDeviceName(deviceType)}
              <span>LIST</span>
            </h2>
            <div className="search_wrap">
              <button
                type="button"
                className="btn_blueline_icon btn_modal_open"
                style={{
                  marginRight: '8px',
                }}
                data-popup="select_scenario_version"
                onClick={() => setIsShowModal(true)}
              >
                버전선택({scenarioVersionInfo.version})
              </button>
              {/* <SelectBox
                items={options}
                enableGrayText
                onItemClick={(index) => {
                  setParamKeyIndexState(index)
                }}
              />
              <div className="search_box">
                <input
                  type="text"
                  placeholder="검색어를 입력해주세요"
                  ref={paramValueRef}
                  onKeyPress={(e) => {
                    if (e.key === 'Enter') {
                      e.preventDefault()
                      if (paramKeyIndex !== 0 || (paramValueRef.current as unknown as HTMLInputElement).value === '') {
                        onSearchClick(undefined, undefined)
                      } else {
                        onSearchClick(
                          matchParamKey(paramKeyIndex),
                          (paramValueRef.current as unknown as HTMLInputElement).value,
                        )
                      }
                    }
                  }}
                />
                <button
                  type="button"
                  onClick={() => {
                    if (paramKeyIndex === 0 || (paramValueRef.current as unknown as HTMLInputElement).value === '') {
                      onSearchClick(undefined, undefined)
                    } else {
                      onSearchClick(
                        matchParamKey(paramKeyIndex),
                        (paramValueRef.current as unknown as HTMLInputElement).value,
                      )
                    }
                  }}
                >
                  <img src={icoSearch} alt="검색 아이콘" />
                </button>
              </div> */}
            </div>
          </div>
        </div>
        <ScenarioVersionListModal
          show={isShowModal}
          onCloseModal={() => {
            setIsShowModal(false)
          }}
        />
        <div className="stn_cont">
          <div className="stn_lineBox type02">
            <div className="lineBox_cnts">
              <ul className="lineBox_cnts_ul">
                <li className="lineBox_cnts_tit">시스템 영상</li>
                <li className="lineBox_cnts_cont">
                  <span>{systemVideoCnt}</span>
                </li>
              </ul>
              <ul className="lineBox_cnts_ul">
                <li className="lineBox_cnts_tit">시나리오 영상</li>
                <li className="lineBox_cnts_cont">
                  <span>{scenarioVideoCnt}</span>
                </li>
              </ul>
            </div>
          </div>
          <PaginationTable
            tableClass="table_dtbt"
            heads={heads}
            bodys={getBodys(items)}
            isScrollable={false}
            colLength={headOptions.length}
          />
        </div>
        <div className="stn_btm">
          <div className="btm_btn_cont">
            {/* <button type="button" className="btn_delete" disabled>삭제하기</button> */}
            <button
              type="button"
              className="btn_blue"
              onClick={() => {
                totalAllDistribution()
              }}
            >
              전체 배포
            </button>
          </div>
        </div>
      </div>
      <UrlPaging pageLength={getTotalPage(totalPage, 10)} currentPage={currentPage} onClickPage={setCurrentPage} />
    </div>
  )
}

export default DistributeList
