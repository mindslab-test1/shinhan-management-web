import React, { useState, useCallback, useEffect } from 'react'
import { Link, useLocation, useHistory } from 'react-router-dom'
import { useSelector } from 'react-redux'
import { rootState } from 'store'
import qs from 'qs'
import { VideoDetailInfo } from 'typings/videoCreateDTO'
import VideoCreateRepository from 'repository/VideoCreate/videoCreateRepository'
import { icoSlash } from 'assets/images/icons'
import { convertScenarioName, convertDeviceName, convertVideoAvatarName, numberFormatComma } from 'utils'

const CreateDetailInfo = () => {
  const location = useLocation()
  const history = useHistory()
  const query = qs.parse(location.search, { ignoreQueryPrefix: true })

  const videoId = useSelector((state: rootState) => state.videoCreationReducer.videoId)

  const [videoInfo, setVideoInfo] = useState<VideoDetailInfo>({
    intent: '',
    utter: '',
    avatar: -1,
    avatarDisplayNm: '',
    outfit: -1,
    outfitDisplayNm: '',
    gesture: -1,
    gestureDisplayNm: '',
    background: -1,
    backgroundDisplayNm: '',
    ttsInputText: '',
    scenarioType: -1,
    scenarioKey: '',
    created: '',
    name: '',
    size: -1,
  })

  const getData = useCallback(() => {
    if (videoId !== -1) {
      VideoCreateRepository.getVideoDetailInfo(videoId)
        .then((res) => {
          setVideoInfo(res.data)
        })
        .catch((res) => {
          alert('영상 상세정보 가져오기 실패')
        })
    }
  }, [videoId])

  useEffect(() => {
    getData()
  }, [videoId])

  useEffect(() => {
    if (location.state === undefined || location.state === null || location.state === '' || history.action === 'POP') {
      history.goBack()
    }
  }, [])

  return (
    <div className="content">
      <div className="cnt_div">
        <div id="snb">
          <p>영상 관리</p>
          <img src={icoSlash} alt="/" />
          <Link to={'/web/manageVideoCreate/system?device=' + parseInt(query.device as string, 10)}>
            {convertDeviceName(parseInt(query.device as string, 10))}
          </Link>
        </div>
        <div className="stn_top">
          <div className="cont">
            <h2 className="sta">
              {convertDeviceName(parseInt(query.device as string, 10))}
              <span>{convertScenarioName(videoInfo.scenarioType, 'ko')} 영상 보기</span>
            </h2>
          </div>
        </div>
        <div className="stn_cont">
          <div className="stn_lineBox video_view_detail">
            <div className="lineBox_cntTitle_wrap">
              <h3 className="lineBox_cnt_title">인공인간 정보</h3>
              <div className="lineBox_cnts">
                <ul className="lineBox_cnts_ul">
                  <li className="lineBox_cnts_tit">모델</li>
                  <li className="lineBox_cnts_cont">
                    <span>{convertVideoAvatarName(videoInfo.avatarDisplayNm)}</span>
                  </li>
                </ul>
                <ul className="lineBox_cnts_ul">
                  <li className="lineBox_cnts_tit">복장</li>
                  <li className="lineBox_cnts_cont">
                    <span>{videoInfo.outfitDisplayNm}</span>
                  </li>
                </ul>
                <ul className="lineBox_cnts_ul">
                  <li className="lineBox_cnts_tit">행동</li>
                  <li className="lineBox_cnts_cont">
                    <span>{videoInfo.gestureDisplayNm}</span>
                  </li>
                </ul>
              </div>
            </div>
            <div className="lineBox_cntTitle_wrap">
              <h3 className="lineBox_cnt_title">답변 정보</h3>
              <div className="lineBox_cnts">
                <ul className="lineBox_cnts_ul">
                  <li className="lineBox_cnts_tit">의도</li>
                  <li className="lineBox_cnts_cont">
                    <input type="text" value={videoInfo.intent} className="color_lightGray02" disabled />
                  </li>
                </ul>
              </div>
              <div className="lineBox_cnts">
                <ul className="lineBox_cnts_ul aifs w100p">
                  <li className="lineBox_cnts_cont div_view_table">
                    <div className="div_view_tr">
                      <div className="div_view_th">답변 텍스트(화면표시)</div>
                      {/* <div className="div_view_th">설명글</div> */}
                    </div>
                    <div className="div_table_tbody">
                      <div className="div_view_tr">
                        <div className="div_view_td">
                          <textarea
                            data-autoresize
                            className="color_lightGray02"
                            value={videoInfo.utter}
                            disabled
                          ></textarea>
                        </div>
                        {/* <div className="div_view_td">
                          <textarea data-autoresize className="color_lightGray02" disabled>현위치 바로 앞쪽으로 빠른창구 및 상담창구가 있고, 뒤쪽에는 ATM기가 설치 되어 있습니다. 1층 안쪽으로는 프리미어라운지가 위치해 있습니다.</textarea>
                        </div> */}
                      </div>
                    </div>
                  </li>
                </ul>
              </div>
            </div>
            <div className="lineBox_cntTitle_wrap">
              <h3 className="lineBox_cnt_title">영상 정보</h3>
              <div className="lineBox_cnts">
                <ul className="lineBox_cnts_ul">
                  <li className="lineBox_cnts_tit">타입</li>
                  <li className="lineBox_cnts_cont">
                    <span>{convertScenarioName(videoInfo.scenarioType, 'ko')}</span>
                  </li>
                </ul>
                {/* <ul className="lineBox_cnts_ul">
                  <li className="lineBox_cnts_tit">지역번호</li>
                  <li className="lineBox_cnts_cont">
                    <span>대한민국</span>
                  </li>
                </ul> */}
              </div>
              <div className="lineBox_cnts">
                <ul className="lineBox_cnts_ul">
                  <li className="lineBox_cnts_tit">UUID</li>
                  <li className="lineBox_cnts_cont">
                    <span>{videoInfo.name}</span>
                  </li>
                </ul>
                <ul className="lineBox_cnts_ul">
                  <li className="lineBox_cnts_tit">파일</li>
                  <li className="lineBox_cnts_cont dfac">
                    <span>{videoInfo.name}</span>
                    {/* <button type="button" className="btn_blueline_icon btn_modal_open" data-popup="viewing_detail_video">미리보기</button> */}
                  </li>
                </ul>
              </div>
              <div className="lineBox_cnts">
                <ul className="lineBox_cnts_ul">
                  <li className="lineBox_cnts_tit">등록일시</li>
                  <li className="lineBox_cnts_cont">
                    <span>{videoInfo.created}</span>
                  </li>
                </ul>
                <ul className="lineBox_cnts_ul">
                  <li className="lineBox_cnts_tit">용량</li>
                  <li className="lineBox_cnts_cont">
                    <span>{numberFormatComma(videoInfo.size)}</span>
                  </li>
                </ul>
              </div>
            </div>
          </div>
        </div>
        <div className="stn_btm">
          <div className="btm_btn_cont flex_fr">
            <div className="btm_btn_right">
              {/* <Link to=
                {
                  "/web/manageVideoCreate/" 
                  + convertScenarioName(videoInfo.scenarioType,'en') 
                  + "?device=" + parseInt(query.device as string, 10)
                } 
                className="btn_blue">
                수정하기
              </Link> */}
              <Link
                to={
                  '/web/manageVideoCreate/' +
                  convertScenarioName(videoInfo.scenarioType, 'en') +
                  '?device=' +
                  query.device
                }
                className="btn_cancel btn_gray"
              >
                목록으로
              </Link>
            </div>
          </div>
        </div>
      </div>
    </div>
  )
}

export default CreateDetailInfo
