import React, { useCallback, useEffect, useState, useRef } from 'react'
import { Link, useLocation } from 'react-router-dom'
import { useDispatch, useSelector } from 'react-redux'
import { rootState } from 'store'
import { setVideoId } from 'store/module/videoCreationReducer'
import { setParamKeyIndex } from 'store/module/distributeSearchReducer'
import qs from 'qs'
import { CreateVideoListInfo } from 'typings/videoCreateDTO'
import VideoCreateRepository from 'repository/VideoCreate/videoCreateRepository'
import { icoEye, icoSlash, icoSearch } from 'assets/images/icons'
import {
  usePolling,
  convertVideoCreateStatus,
  convertVideoAvatarName,
  convertScenarioName,
  convertDeviceName,
  numberFormatComma,
  getPageRowNumber,
  getTotalPage,
} from 'utils'
import UrlPaging from 'components/Pagination/UrlPaging'
import PaginationTable from 'components/table/PaginationTable'
import ScenarioVersionListModal from 'components/Modal/ScenarioVersionListModal/scenarioVersionListModal'
import SelectBox from 'components/SelectBox/selectBox'

const CreateScenarioList = () => {
  const location = useLocation()
  const query = qs.parse(location.search, {
    ignoreQueryPrefix: true,
  })
  const deviceType = parseInt(query.device as string, 10)

  const scenarioVersionInfo = useSelector((state: rootState) => state.scenarioVersionReducer.scenarioVersionInfo)

  const dispatch = useDispatch()
  const setVideoIdState = (videoId: number) => dispatch(setVideoId(videoId))

  const [items, setItems] = useState<CreateVideoListInfo[]>()
  const [totalPage, setTotalPage] = useState(0)
  const [currentPage, setCurrentPage] = useState(1)
  const [isShowModal, setIsShowModal] = useState(false)

  /* const setParamKeyIndexState = (paramKeyIndex: number) => dispatch(setParamKeyIndex(paramKeyIndex))
  const onSearchClick = (paramkey?: string | undefined, paramValue?: string | undefined) => {}
  const paramKeyIndex: number = useSelector((state: rootState) => state.distributeSearchReducer.paramKeyIndex)
  const paramValueRef = useRef(null)
  const options = [
    '분류를 선택해주세요',
    '지점명',
    '지점번호',
    '기기번호',
    '타입',
    '배포된 영상 수',
    '배포상태',
    '배포완료일시',
  ]

  const matchParamKey = useCallback((paramKeyIndex: number) => {
    switch (paramKeyIndex) {
      case 1: {
        return 'deviceNo'
      }
      case 2: {
        return 'branchName'
      }
      case 3: {
        return 'aiHumanUuid'
      }
      case 4: {
        return 'version'
      }
      case 5: {
        return 'manager'
      }
      default: {
        return 'null'
      }
    }
  }, []) */

  const getData = useCallback(async () => {
    await Promise.all([
      VideoCreateRepository.getCreatedGroupTotalPage(deviceType, 0, 0, scenarioVersionInfo.id),
      VideoCreateRepository.getCreatedGroupByPage(currentPage, deviceType, 0, scenarioVersionInfo.id),
    ]).then((responses) => {
      if (responses[0].status === 200) setTotalPage(responses[0].data)
      if (responses[1].status === 200) setItems(responses[1].data)
    })
  }, [currentPage, scenarioVersionInfo])

  useEffect(() => {
    getData()
  }, [currentPage, scenarioVersionInfo])

  usePolling(() => {
    getData()
  }, 5000)

  const getBodys = (bodyItems: CreateVideoListInfo[] | undefined): JSX.Element[] => {
    if (typeof bodyItems === 'undefined' || bodyItems === null || bodyItems.length < 1) {
      return [
        <tr key={1}>
          <td colSpan={headOptions.length} className="nodata_td">
            데이터가 없습니다.
          </td>
        </tr>,
      ]
    } else {
      return bodyItems.map((data, idx) => {
        return (
          <tr key={data.videoId}>
            <td>{getPageRowNumber(idx, currentPage)}</td>
            <td>{convertScenarioName(data.scenarioType, 'ko')}</td>
            <td className="padlr24 tal">{data.intent}</td>
            <td className="padlr24 tal">{data.utter}</td>
            <td>{convertVideoAvatarName(data.avatarDisplayNm)}</td>
            <td className="span_m1">
              <span>{data.outfitDisplayNm}</span>
            </td>
            <td className="padlr24">{data.gestureDisplayNm}</td>
            <td>{numberFormatComma(data.size)}</td>
            <td>{convertVideoCreateStatus(data.status)}</td>
            <td>{data.created}</td>
            <td colSpan={2} className="td_btn">
              <Link
                className="btn_detailPage"
                to={{
                  pathname: '/web/manageVideoCreate/detail/' + data.videoId,
                  state: {
                    state: '/web/manageVideoCreate/detail/' + data.videoId + '?device=' + deviceType,
                  },
                  search: '?device=' + deviceType,
                }}
                onClick={() => {
                  setVideoIdState(data.videoId)
                }}
              >
                <span>보기</span>
                <img src={icoEye} alt="보기 아이콘" />
              </Link>
            </td>
          </tr>
        )
      })
    }
  }

  const headOptions = [
    'No.',
    '타입',
    '의도',
    '답변 텍스트(화면표시)',
    '모델',
    '복장',
    '행동',
    '영상 용량',
    '상태',
    '영상등록일시',
    '보기',
  ]

  const heads = (
    <tr>
      {headOptions.map((data) => {
        return (
          <th key={data}>
            <span>{data}</span>
          </th>
        )
      })}
    </tr>
  )

  return (
    <div className="content">
      <div className="cnt_div">
        <div id="snb">
          <p>영상 관리</p>
          <img src={icoSlash} alt="/" />
          <Link to={'/web/manageVideoCreate/system?device=' + deviceType}>{convertDeviceName(deviceType)}</Link>
        </div>
        <div className="stn_top">
          <div className="cont">
            <h2 className="sta">
              {convertDeviceName(deviceType)}
              <span>LIST</span>
            </h2>
            <div className="link_tab">
              <ul className="link_cnt">
                <li>
                  <Link to={'/web/manageVideoCreate/system?device=' + deviceType}>시스템 발화</Link>
                </li>
                <li className="on">
                  <Link to={'/web/manageVideoCreate/scenario?device=' + deviceType}>시나리오 발화</Link>
                </li>
              </ul>
            </div>
            <div className="search_wrap">
              <button
                type="button"
                className="btn_blueline_icon btn_modal_open"
                style={{
                  marginRight: '8px',
                }}
                data-popup="select_scenario_version"
                onClick={() => setIsShowModal(true)}
              >
                버전선택({scenarioVersionInfo.version})
              </button>
              {/* <SelectBox
                items={options}
                enableGrayText
                onItemClick={(index) => {
                  setParamKeyIndexState(index)
                }}
              />
              <div className="search_box">
                <input
                  type="text"
                  placeholder="검색어를 입력해주세요"
                  ref={paramValueRef}
                  onKeyPress={(e) => {
                    if (e.key === 'Enter') {
                      e.preventDefault()
                      if (paramKeyIndex !== 0 || (paramValueRef.current as unknown as HTMLInputElement).value === '') {
                        onSearchClick(undefined, undefined)
                      } else {
                        onSearchClick(
                          matchParamKey(paramKeyIndex),
                          (paramValueRef.current as unknown as HTMLInputElement).value,
                        )
                      }
                    }
                  }}
                />
                <button
                  type="button"
                  onClick={() => {
                    if (paramKeyIndex === 0 || (paramValueRef.current as unknown as HTMLInputElement).value === '') {
                      onSearchClick(undefined, undefined)
                    } else {
                      onSearchClick(
                        matchParamKey(paramKeyIndex),
                        (paramValueRef.current as unknown as HTMLInputElement).value,
                      )
                    }
                  }}
                >
                  <img src={icoSearch} alt="검색 아이콘" />
                </button>
              </div> */}
            </div>
          </div>
        </div>

        <ScenarioVersionListModal
          show={isShowModal}
          onCloseModal={() => {
            setIsShowModal(false)
          }}
        />
        <div className="stn_cont">
          <PaginationTable
            tableClass="table_video_create"
            heads={heads}
            bodys={getBodys(items)}
            isScrollable
            colLength={headOptions.length}
          />
        </div>
        <div className="stn_btm">
          <div className="btm_btn_cont">
            {/* <button type="button" className="btn_delete" disabled>삭제하기</button> */}
            <div className="btm_btn_right">
              <Link to={'/web/manageVideoCreate/enroll?device=' + deviceType + '&scenarioType=0'} className="btn_blue">
                영상 생성하기
              </Link>
            </div>
          </div>
        </div>
        <UrlPaging pageLength={getTotalPage(totalPage, 10)} currentPage={currentPage} onClickPage={setCurrentPage} />
      </div>
    </div>
  )
}

export default CreateScenarioList
