import React, { useState, useRef } from 'react'
import { Link, useLocation } from 'react-router-dom'
import { useDispatch } from 'react-redux'
import { setUploadInfo } from 'store/module/videoCreationReducer'
import { CreateVideoEnrollUploadListInfo } from 'typings/videoCreateDTO'
import ScenarioVersionRepository from 'repository/scenarioVersionRepository'
import qs from 'qs'
import { imgLogoShinhan } from 'assets/images'

const UploadList = () => {
  const location = useLocation()
  const query = qs.parse(location.search, { ignoreQueryPrefix: true })

  const dispatch = useDispatch()
  const setUploadInfoState = (excel: File, scenarioVersion: string) => dispatch(setUploadInfo(excel, scenarioVersion))
  const [items, setItems] = useState<CreateVideoEnrollUploadListInfo[]>()
  const fileInputRef = useRef(null)

  const [tmpExcel, setTmpExcel] = useState<File>()
  const [tmpScenarioVersion, setTmpScenarioVersion] = useState('')

  let heads = []

  if (query.scenarioType === '1') {
    heads = ['No.', '키', '의도', '답변 텍스트(화면표시)', /* '발화 텍스트(TTS 엔진)', */ '행동'].map((data) => {
      return (
        <th key={data}>
          <span>{data}</span>
        </th>
      )
    })
  } else {
    heads = ['No.', '의도', '답변 텍스트(화면표시)', /* '발화 텍스트(TTS 엔진)', */ '행동'].map((data) => {
      return (
        <th key={data}>
          <span>{data}</span>
        </th>
      )
    })
  }

  const getBodys = (bodyItems: CreateVideoEnrollUploadListInfo[] | undefined): JSX.Element[] => {
    if (typeof bodyItems === 'undefined' || bodyItems === null || bodyItems.length < 1) {
      return [
        <tr key={1}>
          <td colSpan={heads.length} className="nodata_td">
            데이터가 없습니다.
          </td>
        </tr>,
      ]
    } else {
      return bodyItems.map((data, idx) => {
        if (query.scenarioType === '1') {
          return (
            <tr key={idx.toString()}>
              <td>{idx + 1}</td>
              <td>{data.scenarioKey}</td>
              <td>{data.intent}</td>
              <td>{data.utter}</td>
              {/* <td>{data.ttsInputText}</td> */}
              <td>{data.gestureNm}</td>
            </tr>
          )
        } else {
          return (
            <tr key={idx.toString()}>
              <td>{idx + 1}</td>
              <td>{data.intent}</td>
              <td>{data.utter}</td>
              {/* <td>{data.ttsInputText}</td> */}
              <td>{data.gestureNm}</td>
            </tr>
          )
        }
      })
    }
  }

  // 엑셀 파일 읽기
  const handleFileChange = async (event: any) => {
    if (event.target.files[0]) {
      const formData = new FormData()
      formData.append('files', event.target.files[0], event.target.files[0].name)
      await ScenarioVersionRepository.getVideoExcelList(formData)
        .then((res) => {
          setTmpExcel(event.target.files[0])
          setUploadInfoState(event.target.files[0], tmpScenarioVersion)
          setItems(res.data)
        })
        .catch(() => {
          alert('요청 실패')
        })
    }
  }

  // 시나리오버전 인풋 읽기
  const versionChangeHandler = (event: any) => {
    setTmpScenarioVersion(event.target.value)
    setUploadInfoState(tmpExcel, event.target.value)
  }

  if (items) {
    return (
      <>
        <div className="table_cont">
          <table className={query.scenarioType === '1' ? 'table_excel_sys' : 'table_excel_sce'}>
            <caption>엑셀 업로드 목록</caption>
            <colgroup>
              {heads.map((_) => (
                <col key={_.key} />
              ))}
            </colgroup>
            <thead>
              <tr>{heads}</tr>
            </thead>
            <tbody>{getBodys(items)}</tbody>
          </table>
        </div>

        <div className="btm_cnt">
          <div className="btm_btn_cont">
            <button type="button" className="btn_delete" disabled>
              삭제하기
            </button>
            <input type="text" id="versionInput" onChange={versionChangeHandler} />
            <div className="btm_btn_right">
              <Link
                to={process.env.PUBLIC_URL + '/excel/시나리오_업로드_양식.xlsx'}
                target="_blank"
                className="btn_excel_download template_download"
                download
              >
                <span className="ico_img"></span>
                엑셀 템플릿 다운로드
              </Link>

              <button
                type="button"
                className="btn_excel_upload"
                onClick={() => {
                  fileInputRef.current.click()
                }}
              >
                <span className="ico_img template_download"></span>
                시나리오 업로드
              </button>
              <input
                type="file"
                name="excelFile"
                id="excelFile"
                ref={fileInputRef}
                onChange={handleFileChange}
                hidden
              />
            </div>
          </div>
        </div>
      </>
    )
  } else {
    return (
      <>
        <div className="table_cont">
          <table>
            <caption>엑셀 업로드 목록</caption>
            <colgroup>
              {heads.map((_) => (
                <col key={_.key} />
              ))}
            </colgroup>
            <thead>
              <tr>{heads}</tr>
            </thead>
          </table>
          <div className="none_cont">
            <img src={imgLogoShinhan} alt="신한 로고" />
            <p>
              <em className="color_darkBlue">시나리오</em>를 업로드 해주세요.
            </p>
          </div>
        </div>
        <div className="btm_cnt">
          <div className="btm_btn_cont">
            <button type="button" className="btn_delete" disabled>
              삭제하기
            </button>
            <input type="text" id="versionInput" onChange={versionChangeHandler} />
            <div className="btm_btn_right">
              <Link
                to={process.env.PUBLIC_URL + '/excel/시나리오_업로드_양식.xlsx'}
                target="_blank"
                className="btn_excel_download template_download"
                download
              >
                <span className="ico_img template_download"></span>
                엑셀 템플릿 다운로드
              </Link>
              <button
                type="button"
                className="btn_excel_upload"
                onClick={() => {
                  fileInputRef.current.click()
                }}
              >
                <span className="ico_img template_download"></span>
                시나리오 업로드
              </button>
              <input
                type="file"
                accept=".xls,.xlsx"
                name="excelFile"
                id="excelFile"
                ref={fileInputRef}
                onChange={handleFileChange}
                hidden
              />
            </div>
          </div>
        </div>
      </>
    )
  }
}

export default UploadList
