import React from 'react'
import { Link, useHistory, useLocation } from 'react-router-dom'
import { useSelector } from 'react-redux'
import { rootState } from 'store'
import qs from 'qs'
import VideoCreateRepository from 'repository/VideoCreate/videoCreateRepository'
import { icoSlash } from 'assets/images/icons'
import { convertScenarioName, convertDeviceName } from 'utils'
import UploadList from './uploadList'

const CreateEnroll = () => {
  const history = useHistory()
  const location = useLocation()
  const query = qs.parse(location.search, {
    ignoreQueryPrefix: true,
  })
  const uploadInfo = useSelector((state: rootState) => state.videoCreationReducer.uploadInfo)

  const versionPattern = '[1-9]+\\.[1-9]+\\.[1-9]+'

  // 엑셀 파일업로드
  const handleFileUpload = () => {
    let isValid = true
    if (!uploadInfo.scenarioVersion) {
      isValid = false
      alert('시나리오 버전을 입력해주세요.')
    }
    if (!uploadInfo.scenarioVersion.match(versionPattern) && uploadInfo.scenarioVersion) {
      isValid = false
      alert('형식에 맞는 버전을 입력해주세요.(예: 1.0.0)')
    }

    if (isValid) {
      const formData = new FormData()
      formData.append('files', uploadInfo.excel, uploadInfo.excel.name)
      formData.append('deviceType', query.device as string)
      formData.append('scenarioType', query.scenarioType as string)
      formData.append('scenarioVersion', uploadInfo.scenarioVersion)

      VideoCreateRepository.createVideoExcelUpload(formData)
        .then((res) => {
          if (res.data.result === true) {
            alert('엑셀 파일 업로드 성공')
          } else {
            alert(res.data.msg)
          }
          history.goBack()
        })
        .catch(() => {
          alert('요청 실패')
          history.goBack()
        })
    }
  }
  return (
    <div className="content">
      <div className="cnt_div">
        <div id="snb">
          <p>영상 관리</p>
          <img src={icoSlash} alt="/" />
          <Link to={'/web/manageVideoCreate/system?device=' + parseInt(query.device as string, 10)}>
            {convertDeviceName(parseInt(query.device as string, 10))}
          </Link>
        </div>
        <div className="stn_top">
          <div className="cont">
            <h2 className="sta">
              {convertDeviceName(parseInt(query.device as string, 10))}
              <span>{convertScenarioName(parseInt(query.scenarioType as string, 10), 'ko')} 영상 생성</span>
            </h2>
          </div>
        </div>
        <div className="stn_cont">
          <UploadList />
          <div className="stn_lineBox type02">
            <div className="lineBox_cntTitle_wrap">
              <h3 className="lineBox_cnt_title">영상정보</h3>
              <div className="lineBox_cnts">
                <ul className="lineBox_cnts_ul">
                  <li className="lineBox_cnts_tit">타입</li>
                  <li className="lineBox_cnts_cont">
                    <input
                      type="text"
                      value={convertScenarioName(parseInt(query.scenarioType as string, 10), 'ko')}
                      className="color_lightGray02"
                      disabled
                    />
                  </li>
                </ul>
                {/* <ul className="lineBox_cnts_ul">
              <li className="lineBox_cnts_tit">지역 코드</li>
              <li className="lineBox_cnts_cont">
                <div className="select_box">
                  <div className="custom_select type02">
                    <select>
                      <option value="1">대한민국</option>
                      <option value="2">대한민국?</option>
                    </select>
                  </div>
                </div>
              </li>
            </ul> */}
              </div>
            </div>
          </div>
        </div>

        <div className="stn_btm">
          <div className="btm_btn_cont flex_fr">
            <div className="btm_btn_right">
              <Link
                to={
                  '/web/manageVideoCreate/' +
                  convertScenarioName(parseInt(query.scenarioType as string, 10), 'en') +
                  '?device=' +
                  query.device
                }
                className="btn_gray"
              >
                취소
              </Link>

              {/* <button
                type="button"
                className="btn_gray"
                onClick={() => {
                  history.goBack()
                }}
              >
                취소
              </button> */}

              <button
                type="button"
                className="btn_blue"
                onClick={() => {
                  handleFileUpload()
                }}
              >
                저장 후 영상생성
              </button>
            </div>
          </div>
        </div>
      </div>
    </div>
  )
}

export default CreateEnroll
