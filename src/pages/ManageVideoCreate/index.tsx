import React from 'react'
import { Redirect, Route, Switch } from 'react-router-dom'
import CreateSystemList from './CreateSystemList/createSystemList'
import CreateScenarioList from './CreateScenarioList/createScenarioList'
import CreateEnroll from './CreateEnroll/createEnroll'
import CreateDetail from './CreateDetail/createDetail'

const ManageVideoCreate = () => {
  return (
    <div id="contents">
      <Switch>
        <Route path="/web/manageVideoCreate/system" component={CreateSystemList} />
        <Route path="/web/manageVideoCreate/scenario" component={CreateScenarioList} />
        <Route path="/web/manageVideoCreate/enroll" component={CreateEnroll} />
        <Route path="/web/manageVideoCreate/detail" component={CreateDetail} />
        <Redirect exact path="/web/manageVideoCreate/" to="/web/manageVideoCreate/system" />
      </Switch>
    </div>
  )
}

export default ManageVideoCreate
