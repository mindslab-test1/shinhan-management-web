import { icoSearch } from 'assets/images/icons'
import React, { VFC } from 'react'
import { SearchBoxProps } from './searchBoxType'

const SearchBox: VFC<SearchBoxProps> = ({
  className = '',
  inputClassName = '',
  placeholder = '',
  onInputChange,
  onButtonClick,
  onInputKeyPress,
  buttonImgSrc = icoSearch,
  buttonImgAlt = '',
}) => {
  return (
    <div className={'search_box ' + className}>
      <input
        onKeyPress={onInputKeyPress}
        type="text"
        placeholder={placeholder}
        onChange={onInputChange}
        className={inputClassName}
      />
      <button type="button" onClick={onButtonClick}>
        <img src={buttonImgSrc} alt={buttonImgAlt} />
      </button>
    </div>
  )
}

export default SearchBox
