import { KeyboardEventHandler, MouseEventHandler } from 'react'

export interface SearchBoxProps {
  className?: string | undefined
  inputClassName?: string | undefined
  placeholder?: string | undefined
  onInputChange?: React.ChangeEventHandler<HTMLInputElement> | undefined
  onInputKeyPress?: KeyboardEventHandler<HTMLInputElement> | undefined
  onButtonClick?: MouseEventHandler<HTMLButtonElement> | undefined
  buttonImgSrc?: string | undefined
  buttonImgAlt: string | undefined
}
