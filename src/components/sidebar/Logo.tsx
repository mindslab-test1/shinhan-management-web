import React from 'react'
import { Link } from 'react-router-dom'
import { icoLogo } from '../../assets/images'

const Logo = () => {
  return (
    <div className="gnb_header">
      <h1 className="logo">
        <Link to="/web">
          <img src={icoLogo} alt="신한은행 인공인간관리시스템" />
        </Link>
      </h1>
    </div>
  )
}

export default Logo
