import React, { FC } from 'react'
import Logo from './Logo'
import UserInfo from './UserInfo'
import Alarm from './Alarm'
import Menu from './Menu'

const Sidebar: FC = () => {
  return (
    <div id="gnb">
      <Logo />
      <div className="gnb_stn">
        <UserInfo />
        <Alarm />
        <Menu />
      </div>
    </div>
  )
}

export default Sidebar
