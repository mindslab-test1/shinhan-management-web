import React, { useRef } from 'react'
import { icoProfile, icoArrowWhite, icoLogout } from '../../assets/images/icons'

const UserInfo = () => {
  const userInfoBtnRef = useRef(null)
  const userInfoDivRef = useRef(null)

  const onUserInfoClick = () => {
    ;(userInfoDivRef.current as unknown as HTMLDivElement).classList.toggle('on')
  }

  return (
    <div className="gnb_user" ref={userInfoDivRef}>
      <button type="button" className="btn_gnb_user" ref={userInfoBtnRef} onClick={onUserInfoClick}>
        <span className="user_left">
          <img src={icoProfile} alt="유저 아이콘" />
          <span className="desc">
            <span className="depart">미래전략연구소</span>
            <b className="name">뽀니스 아구스아구스 아구스 </b>
          </span>
        </span>
        <span className="accIcon">
          <img src={icoArrowWhite} alt="화살표" />
        </span>
      </button>
      <button type="button" className="btn_logout">
        <img src={icoLogout} alt="로그아웃 아이콘" />
        <span>로그아웃</span>
      </button>
    </div>
  )
}

export default UserInfo
