import React, { useRef } from 'react'
import { Link, useLocation } from 'react-router-dom'
import { icoDashboard, icoMonitor, icoChart, icoSetting } from '../../assets/images/icons'

const Menu = () => {
  const accordionRefs = [useRef(null), useRef(null), useRef(null), useRef(null)]
  const panelRefs = [useRef(null), useRef(null), useRef(null), useRef(null)]

  const onAccordionDeviceButtonClick = (e: React.MouseEvent<HTMLButtonElement, MouseEvent>, divName: String) => {
    let accordion: HTMLButtonElement | null
    let mainPanel: HTMLDivElement | null
    let minorPanel1: HTMLDivElement | null
    let minorPanel2: HTMLDivElement | null
    let minorPanel3: HTMLDivElement | null

    const classNames = e.currentTarget.classList
    if (classNames.contains('videoMaking')) {
      accordion = accordionRefs[0].current as unknown as HTMLButtonElement
      mainPanel = panelRefs[0].current as unknown as HTMLDivElement
      minorPanel1 = panelRefs[1].current as unknown as HTMLDivElement
      minorPanel2 = panelRefs[2].current as unknown as HTMLDivElement
      minorPanel3 = panelRefs[3].current as unknown as HTMLDivElement
    } else if (classNames.contains('device')) {
      accordion = accordionRefs[1].current as unknown as HTMLButtonElement
      mainPanel = panelRefs[1].current as unknown as HTMLDivElement
      minorPanel1 = panelRefs[2].current as unknown as HTMLDivElement
      minorPanel2 = panelRefs[3].current as unknown as HTMLDivElement
      minorPanel3 = panelRefs[0].current as unknown as HTMLDivElement
    } else if (classNames.contains('videoDistribution')) {
      accordion = accordionRefs[2].current as unknown as HTMLButtonElement
      mainPanel = panelRefs[2].current as unknown as HTMLDivElement
      minorPanel1 = panelRefs[3].current as unknown as HTMLDivElement
      minorPanel2 = panelRefs[0].current as unknown as HTMLDivElement
      minorPanel3 = panelRefs[1].current as unknown as HTMLDivElement
    } else if (classNames.contains('concierge')) {
      accordion = accordionRefs[3].current as unknown as HTMLButtonElement
      mainPanel = panelRefs[3].current as unknown as HTMLDivElement
      minorPanel1 = panelRefs[0].current as unknown as HTMLDivElement
      minorPanel2 = panelRefs[1].current as unknown as HTMLDivElement
      minorPanel3 = panelRefs[2].current as unknown as HTMLDivElement
    } else {
      mainPanel = null
      minorPanel1 = null
      minorPanel2 = null
      minorPanel3 = null
      accordion = null
    }

    const panelList = [mainPanel, minorPanel1, minorPanel2, minorPanel3]
    const alreadyActive = accordion?.classList.contains('accordion_active')

    if (mainPanel === null || minorPanel1 === null || minorPanel2 === null || minorPanel3 === null) {
      throw new Error('there is no such div element')
    } else if (accordion === null) {
      throw new Error('there is no such button element')
    }

    panelList.forEach((eachPanel) => {
      const panelElement = eachPanel
      ;(panelElement as HTMLDivElement).style.maxHeight = '0'
      panelElement?.previousElementSibling?.classList.remove('accordion_active')
    })

    if (!alreadyActive) {
      accordion.classList.add('accordion_active')
      mainPanel.style.maxHeight = mainPanel.scrollHeight + 'px'
    }
  }

  const onPanelButtonClick = (e: React.MouseEvent<HTMLAnchorElement, MouseEvent>) => {
    for (let i = 0; i < panelRefs.length; i += 1) {
      for (let j = 0; j < (panelRefs[i].current as unknown as HTMLDivElement).children[0].children.length; j += 1) {
        ;(panelRefs[i].current as unknown as HTMLDivElement).children[0].children[j].classList.remove('on')
      }
    }
    e.currentTarget.parentElement.className = 'on'
  }

  return (
    <ul className="gnb_menu">
      <li>
        <button type="button" className="accordion_none">
          <Link to="/web/dashBoard">
            <span className="txt">
              <img src={icoDashboard} className="dashboardImg" alt="대시보드 아이콘" />
              대시보드
            </span>
          </Link>
        </button>
      </li>
      <li>
        <button
          type="button"
          className="videoMaking accordion"
          ref={accordionRefs[0]}
          onClick={(e) => onAccordionDeviceButtonClick(e, 'videoMaking')}
        >
          <span className="txt">
            <span className="txtImg sceneImg" />
            영상 관리
          </span>
          <span className="accIcon" />
        </button>
        <div className="panel" ref={panelRefs[0]}>
          <ul>
            <li>
              <Link to="/web/manageVideoCreate/system?device=1" onClick={onPanelButtonClick}>
                <span className="hexagon" />
                AI 컨시어지
              </Link>
            </li>
            <li>
              <Link to="/web/manageVideoCreate/system?device=2" onClick={onPanelButtonClick}>
                <span className="hexagon" />
                AI 뱅커 디지털데스크
              </Link>
            </li>
            <li>
              <Link to="/web/manageVideoCreate/system?device=3" onClick={onPanelButtonClick}>
                <span className="hexagon" />
                스마트 키오스크
              </Link>
            </li>
            <li>
              <Link to="/web/manageVideoCreate/system?device=4" onClick={onPanelButtonClick}>
                <span className="hexagon" />
                스마트 카드업무 키오스크
              </Link>
            </li>
          </ul>
        </div>
      </li>
      <li>
        <button
          type="button"
          className="device accordion"
          ref={accordionRefs[1]}
          onClick={(e) => onAccordionDeviceButtonClick(e, 'device')}
        >
          <span className="txt">
            <span className="txtImg humanImg" />
            기기 관리
          </span>
          <span className="accIcon" />
        </button>
        <div className="panel" ref={panelRefs[1]}>
          <ul>
            <li>
              <Link to="/web/manageDevice/list?device=1" onClick={onPanelButtonClick}>
                <span className="hexagon" />
                AI 컨시어지
              </Link>
            </li>
            <li>
              <Link to="/web/manageDevice/list?device=2" onClick={onPanelButtonClick}>
                <span className="hexagon" />
                AI 뱅커 디지털데스크
              </Link>
            </li>
            <li>
              <Link to="/web/manageDevice/list?device=3" onClick={onPanelButtonClick}>
                <span className="hexagon" />
                스마트 키오스크
              </Link>
            </li>
            <li>
              <Link to="/web/manageDevice/list?device=4" onClick={onPanelButtonClick}>
                <span className="hexagon" />
                스마트 카드업무 키오스크
              </Link>
            </li>
          </ul>
        </div>
      </li>
      <li>
        <button
          type="button"
          className="videoDistribution accordion"
          ref={accordionRefs[2]}
          onClick={(e) => onAccordionDeviceButtonClick(e, 'videoDistribution')}
        >
          <span className="txt">
            <span className="txtImg distributionImg" />
            배포 관리
          </span>
          <span className="accIcon" />
        </button>
        <div className="panel" ref={panelRefs[2]}>
          <ul>
            <li>
              <Link to="/web/manageVideoDistribute/list?device=1" onClick={onPanelButtonClick}>
                <span className="hexagon" />
                AI 컨시어지
              </Link>
            </li>
            <li>
              <Link to="/web/manageVideoDistribute/list?device=2" onClick={onPanelButtonClick}>
                <span className="hexagon" />
                AI 뱅커 디지털데스크
              </Link>
            </li>
            <li>
              <Link to="/web/manageVideoDistribute/list?device=3" onClick={onPanelButtonClick}>
                <span className="hexagon" />
                스마트 키오스크
              </Link>
            </li>
            <li>
              <Link to="/web/manageVideoDistribute/list?device=4" onClick={onPanelButtonClick}>
                <span className="hexagon" />
                스마트 카드업무 키오스크
              </Link>
            </li>
          </ul>
        </div>
      </li>
      <li>
        <button
          type="button"
          className="concierge accordion"
          ref={accordionRefs[3]}
          onClick={(e) => onAccordionDeviceButtonClick(e, 'concierge')}
        >
          <span className="txt">
            <span className="txtImg conciergeImg" />
            컨시어지 관리
          </span>
          <span className="accIcon" />
        </button>
        <div className="panel" ref={panelRefs[3]}>
          <ul>
            <li>
              <Link to="/web/manageConcierge/notice" onClick={onPanelButtonClick}>
                <span className="hexagon" />
                공지사항 관리
              </Link>
            </li>
            <li>
              <Link to="/web/manageConcierge/standby" onClick={onPanelButtonClick}>
                <span className="hexagon" />
                대기화면 관리
              </Link>
            </li>
            <li>
              <Link to="/web/manageConcierge/branchInfo" onClick={onPanelButtonClick}>
                <span className="hexagon" />
                지점정보 관리
              </Link>
            </li>
            <li>
              <Link to="/web/manageConcierge/tooltip" onClick={onPanelButtonClick}>
                <span className="hexagon" />
                툴팁 관리
              </Link>
            </li>
            <li>
              <Link to="/web/manageConcierge/menu" onClick={onPanelButtonClick}>
                <span className="hexagon" />
                메뉴 관리
              </Link>
            </li>
          </ul>
        </div>
      </li>
      <li>
        <button type="button" className="accordion_none">
          <Link to="/web/manageAiEngine">
            <span className="txt">
              <img src={icoChart} className="chartImg" alt="통계  아이콘" />
              AI엔진 관리
            </span>
          </Link>
        </button>
      </li>
      {/* <li>
        <button type="button" className="accordion_none">
          <span className="txt">
            <img src={icoMonitor} className="monitorImg" alt="대기화면 관리  아이콘" />
            대기화면 관리
          </span>
        </button>
      </li>
      <li>
        <button type="button" className="accordion_none">
          <span className="txt">
            <img src={icoChart} className="chartImg" alt="통계  아이콘" />
            통계
          </span>
        </button>
      </li> */}
      <li>
        <button type="button" className="accordion_none">
          <Link to="/web/manageAuth">
            <span className="txt">
              <img src={icoSetting} className="settingImg" alt="권한관리  아이콘" />
              권한관리
            </span>
          </Link>
        </button>
      </li>
    </ul>
  )
}

export default Menu
