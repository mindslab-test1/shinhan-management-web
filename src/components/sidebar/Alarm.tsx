import React from 'react'
import { icoError, icoUpdate, icoChange, icoAlarmOn } from '../../assets/images/icons'

const Alarm = () => {
  const onAlarmClick = (e: React.MouseEvent<HTMLButtonElement, MouseEvent>) => {
    if (e.currentTarget.classList.contains('on')) {
      e.currentTarget.classList.remove('on')
      ;(e.currentTarget.parentElement as HTMLElement).children[1].classList.remove('on')
      ;(
        e.currentTarget.parentElement?.parentElement?.parentElement?.parentElement as HTMLElement
      ).children[1].classList.remove('alarm')
    } else {
      e.currentTarget.classList.add('on')
      ;(e.currentTarget.parentElement as HTMLElement).children[1].classList.add('on')
      ;(
        e.currentTarget.parentElement?.parentElement?.parentElement?.parentElement as HTMLElement
      ).children[1].classList.add('alarm')
    }
  }

  const onBGClick = (e: React.MouseEvent<HTMLDivElement, MouseEvent>) => {
    ;(e.currentTarget.parentElement?.parentElement as HTMLElement).children[0].classList.remove('on')
    ;(
      (e.currentTarget.parentElement?.parentElement as HTMLElement).children[0].parentElement as HTMLElement
    ).children[1].classList.remove('on')
    ;(
      (e.currentTarget.parentElement?.parentElement as HTMLElement).children[0].parentElement?.parentElement
        ?.parentElement?.parentElement as HTMLElement
    ).children[1].classList.remove('alarm')
  }

  return (
    <div className="notice_cont">
      <button type="button" className="btn_notice" id="btn_notice" onClick={(e) => onAlarmClick(e)}>
        <img src={icoAlarmOn} alt="알림 아이콘" />
        <span>알림</span>
      </button>
      <div className="noti_wrap">
        <div className="noti_bg" onClick={(e) => onBGClick(e)}></div>
        <div className="notice_popup" id="np">
          <p className="noti_tit">알림</p>
          <div className="noti_list_cont">
            <div className="noti_list">
              <img src={icoChange} alt="변경 아이콘" />
              <div>
                <b>인공인간 설정이 변경되었습니다.</b>
                <p>
                  <span>서소문</span>
                  <span> / </span>
                  <span>AI 컨시어지</span>
                </p>
              </div>
            </div>
            <div className="noti_list">
              <img src={icoUpdate} alt="업데이트 아이콘" />
              <div>
                <b>답변 시나리오가 업데이트되었습니다.</b>
                <p>
                  <span>명동대기업금융센터</span>
                  <span> / </span>
                  <span>AI 뱅커 디지털데스크</span>
                </p>
              </div>
            </div>
            <div className="noti_list">
              <img src={icoError} alt="오류 아이콘" />
              <div>
                <b>배포중 오류가 발생하였습니다.</b>
                <p>
                  <span>신한PWM태평로센터</span>
                  <span> / </span>
                  <span>스마트 카드업무 키오스크</span>
                </p>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  )
}

export default Alarm
