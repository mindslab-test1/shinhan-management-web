import React, { VFC } from 'react'
import xlsx from 'xlsx'

interface excelProps {
  body: object[]
}

const Excel: VFC<excelProps> = ({ body }) => {
  const dataToExcel = () => {
    const wscols = [{ width: 5 }, { width: 25 }, { width: 50 }]
    const wsrows = body.map((val) => {
      return {
        hpx: ((parseInt(Object.values(val)[2].toString().length, 10) % 5) + 1) * 20,
      }
    })
    wsrows.splice(0, 0, { hpx: 20 })
    const ws = xlsx.utils.json_to_sheet(body)
    const wb = xlsx.utils.book_new()
    ws['!cols'] = wscols
    ws['!rows'] = wsrows
    xlsx.utils.book_append_sheet(wb, ws, '인공인간 업데이트')
    xlsx.writeFile(wb, '인공인간 업데이트 목록.xlsx')
  }

  return (
    <button type="button" className="btn_excel_download" disabled={body.length === 0} onClick={dataToExcel}>
      <span className="ico_img" />
      선택항목 엑셀 다운로드
    </button>
  )
}

export default Excel
