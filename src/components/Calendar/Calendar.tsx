import React, { VFC } from 'react'
import { useDispatch } from 'react-redux'
import { DateRangePicker } from 'rsuite'
import { DisabledDateFunction } from 'rsuite/lib/DateRangePicker'

interface CalendarProps {
  date: {
    startDate: Date
    endDate: Date
  }
  reducerAction?: (searchDate: { startDate: Date; endDate: Date }) => void
  setStateAction?: (searchDate: { startDate: Date; endDate: Date }) => void
  disabledDate?: () => DisabledDateFunction
}

const Calendar: VFC<CalendarProps> = ({ date, reducerAction, setStateAction, disabledDate }) => {
  type searchDate = {
    startDate: Date
    endDate: Date
  }

  const dispatch = useDispatch()

  let setDateState: (searchDate: { startDate: Date; endDate: Date }) => void
  if (reducerAction !== undefined) {
    setDateState = (searchDate: searchDate) => dispatch(reducerAction(searchDate))
  } else {
    setDateState = (searchDate: searchDate) => setStateAction(searchDate)
  }

  const timezoneOffset = new Date().getTimezoneOffset() * 60000

  return (
    <div className="input_calendar">
      <DateRangePicker
        disabledDate={disabledDate === undefined ? null : disabledDate()}
        format="YYYY-MM-DD"
        ranges={[
          {
            label: '전체기간 보기',
            value: [new Date(Date.UTC(1900, 0, 1)), new Date(Date.now())],
          },
          {
            label: '한달전',
            value: [new Date(Date.now() - 1000 * 60 * 60 * 24 * 30), new Date(Date.now())],
          },
          {
            label: '오늘',
            value: [new Date(Date.now()), new Date(Date.now())],
          },
        ]}
        placement="bottomEnd"
        locale={{
          sunday: '일',
          monday: '월',
          tuesday: '화',
          wednesday: '수',
          thursday: '목',
          friday: '금',
          saturday: '토',
          ok: '확인',
          today: '오늘',
          yesterday: '어제',
          last7Days: '지난 7일',
          hours: '시',
          minutes: '분',
          seconds: '초',
        }}
        value={[date.startDate, date.endDate]}
        onChange={(val) => {
          setDateState({
            startDate: new Date(val[0].getTime() - timezoneOffset),
            endDate: new Date(val[1].getTime() - timezoneOffset),
          })
        }}
        cleanable={false}
      />
    </div>
  )
}

export default Calendar
