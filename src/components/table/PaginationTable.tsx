import React, { FC, useEffect, useRef, useState, VFC } from 'react'

interface Props {
  tableClass: string
  divClass?: string
  heads: JSX.Element
  bodys: JSX.Element[]
  isScrollable: boolean
  caption?: string | undefined
  style?: React.CSSProperties
  colLength: number
}

const PaginationTable: VFC<Props> = ({
  tableClass,
  divClass = '',
  heads,
  bodys,
  isScrollable = true,
  caption,
  style,
  colLength,
}) => {
  const scrollBoxRef = useRef<HTMLDivElement>(null)
  const [isOverflowing, setIsOverflowing] = useState(false)

  useEffect(() => {
    if (scrollBoxRef.current) {
      if (
        (scrollBoxRef.current as HTMLDivElement).scrollHeight > (scrollBoxRef.current as HTMLDivElement).clientHeight
      ) {
        setIsOverflowing(true)
      } else {
        setIsOverflowing(false)
      }
    }
  }, [bodys])

  return (
    <div
      className={
        'table_cont ' + divClass + (isScrollable ? ' scroll_box ' : '') + (isOverflowing ? 'overflowing ' : '')
      }
      style={style}
    >
      <table className={tableClass}>
        <caption>{caption ?? ''}</caption>
        <colgroup>
          {Array.from({ length: colLength }).map((_, index) => (
            <col key={index.toString()} />
          ))}
        </colgroup>
        <thead>{heads}</thead>
        <tbody>{bodys}</tbody>
      </table>
    </div>
  )
}

export default PaginationTable
