import React, { useState, VFC } from 'react'
import { ChromePicker } from 'react-color'
import { useDebounce } from 'utils'

interface debouncedPickerProps {
  color: {
    r: number
    g: number
    b: number
    a: number
  }
  onChange: React.Dispatch<
    React.SetStateAction<
      {
        r: number
        g: number
        b: number
        a: number
      }[]
    >
  >
  index: number
}

const DebouncedColorPicker: VFC<debouncedPickerProps> = ({ color, onChange, index }) => {
  const [value, setValue] = useState(color)

  useDebounce(value, 200, () =>
    onChange((thisColors) => {
      const newColors = thisColors.slice()
      newColors[index] = value
      return newColors
    }),
  )

  return (
    <ChromePicker
      color={value}
      onChange={(colorValue) =>
        setValue({ r: colorValue.rgb.r, g: colorValue.rgb.g, b: colorValue.rgb.b, a: colorValue.rgb.a })
      }
      disableAlpha
    />
  )
}

export default DebouncedColorPicker
