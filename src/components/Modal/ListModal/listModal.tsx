import ModalPaging from 'components/Pagination/ModalPaging'
import qs from 'qs'
import React, { useEffect, useRef, useState, VFC } from 'react'
import { useLocation } from 'react-router-dom'
import DeviceListRepository from 'repository/Device/deviceListRepository'
import { ListEnrollPlace } from 'typings/commonDTO'
import Modal from '..'
import { icoSearch } from '../../../assets/images/icons'

interface Props {
  show: boolean
  onCloseModal: any
  setDeviceInfo: (branchNo?: string | undefined, branchOffice?: string | undefined) => void
}

const heads = ['No.', '지점코드', '지점명']

const headCom = heads.map((value) => {
  return <th>{value}</th>
})

const bodys: ListEnrollPlace[] = [
  {
    id: 1,
    code: '0263025',
    branchOffice: '마인즈',
  },
  {
    id: 2,
    code: '0263025',
    branchOffice: '판교',
  },
  {
    id: 3,
    code: '0263025',
    branchOffice: '서소문',
  },
  {
    id: 4,
    code: '0263025',
    branchOffice: '서소문',
  },
  {
    id: 5,
    code: '0263025',
    branchOffice: '서소문',
  },
  {
    id: 6,
    code: '0263025',
    branchOffice: '서소문',
  },
  {
    id: 7,
    code: '0263025',
    branchOffice: '서소문',
  },
  {
    id: 8,
    code: '0263025',
    branchOffice: '서소문',
  },
  {
    id: 9,
    code: '0263025',
    branchOffice: '서소문',
  },
  {
    id: 10,
    code: '0263025',
    branchOffice: '서소문',
  },
]

const ListModal: VFC<Props> = ({ show, onCloseModal, setDeviceInfo }) => {
  const [searchVal, setSearchVal] = useState('')
  const [branchOffice, setBranchOffice] = useState('')
  const [pageNum, setPageNum] = useState(1)
  const location = useLocation()
  const [device, setDevice] = useState<string[]>([])
  const deviceType = qs.parse(location.search, { ignoreQueryPrefix: true }).device
  const scrollBoxRef = useRef<HTMLDivElement>(null)
  const [isOverflowing, setIsOverflowing] = useState(false)

  useEffect(() => {
    setDeviceInfo('', '')
    setDevice([])
    setPageNum(1)
  }, [show])

  useEffect(() => {
    if (scrollBoxRef.current) {
      if (
        (scrollBoxRef.current as HTMLDivElement).scrollHeight > (scrollBoxRef.current as HTMLDivElement).clientHeight
      ) {
        setIsOverflowing(true)
      } else {
        setIsOverflowing(false)
      }
    }
  }, [device])

  const deviceBodyCom = [...device].map((value) => {
    return (
      <tr
        onClick={(e) => {
          let i
          for (i = 0; i < (e.currentTarget.parentElement as HTMLElement)?.children.length; i += 1) {
            if ((e.currentTarget.parentElement as HTMLElement)?.children[i].classList.contains('select')) {
              ;(e.currentTarget.parentElement as HTMLElement)?.children[i].classList.remove('select')
            }
          }
          e.currentTarget.classList.add('select')
          setDeviceInfo(value, undefined)
        }}
      >
        <td>{value}</td>
      </tr>
    )
  })

  const bodyCom = bodys
    .filter((val) => {
      if (searchVal === '') return val
      else if (val.code.includes(searchVal) || val.branchOffice.includes(searchVal)) return val
      else return null
    })
    .map((value) => {
      return (
        <tr
          onClick={(e) => {
            let i
            for (i = 0; i < (e.currentTarget.parentElement as HTMLElement)?.children.length; i += 1) {
              if ((e.currentTarget.parentElement as HTMLElement)?.children[i].classList.contains('select')) {
                ;(e.currentTarget.parentElement as HTMLElement)?.children[i].classList.remove('select')
              }
            }
            e.currentTarget.classList.add('select')
            setDeviceInfo(undefined, value.branchOffice)
            setBranchOffice(value.branchOffice)
          }}
        >
          <td>{value.id}</td>
          <td>{value.code}</td>
          <td>{value.branchOffice}</td>
        </tr>
      )
    })

  return (
    <Modal onCloseModal={onCloseModal} show={show}>
      <div className="select_branch modal_popup">
        {pageNum === 1 ? (
          <div className="modal_stn">
            <div className="pop_top">
              <h3>지점 선택</h3>
            </div>
            <div className="pop_mid">
              <form action="">
                <div className="search_box">
                  <input
                    type="text"
                    placeholder="지점코드나 지점명을 입력해주세요"
                    onChange={(event) => setSearchVal(event.target.value)}
                  />
                  <button type="button">
                    <img src={icoSearch} alt="검색 아이콘" />
                  </button>
                </div>
                <div className="table_cont">
                  <table className="table_modal01">
                    <caption>영상생성관리 지점별 영상 목록</caption>
                    <colgroup>
                      <col width="16.7%" />
                      <col />
                      <col width="55.6%" />
                    </colgroup>
                    <thead>
                      <tr>{headCom}</tr>
                    </thead>
                    <tbody>{bodyCom}</tbody>
                  </table>
                </div>
                {/* <ModalPaging pageLength={20} onClickPage={() => {}} /> */}
              </form>
            </div>
            <div className="pop_btm">
              <button
                type="button"
                className="btn_cancel btn_gray btn_md_close"
                onClick={() => {
                  onCloseModal('', '')
                }}
              >
                취소
              </button>
              <button
                type="button"
                className="btn_ok btn_blue btn_md_close"
                onClick={() => {
                  setPageNum(2)
                  // DeviceListRepository.getDeviceList(parseInt(deviceType as string, 10), branchOffice).then(
                  //   (response) =>
                  //     response.data.map((val: { deviceNo: string }) => {
                  //       return setDevice((device) => [...device, val.deviceNo])
                  //     }),
                  // )
                }}
              >
                다음
              </button>
            </div>
          </div>
        ) : (
          <div className="modal_stn">
            <div className="pop_top">
              <h3>기기 선택</h3>
            </div>
            <div className="pop_mid">
              <form action="">
                <div className="table_cont">
                  <table className="table_modal01">
                    <thead>
                      <th>기기 번호</th>
                    </thead>
                  </table>
                  <div className={'scroll_box ' + (isOverflowing ? 'overflowing' : '')} ref={scrollBoxRef}>
                    <table>
                      <tbody>{deviceBodyCom}</tbody>
                    </table>
                  </div>
                </div>
              </form>
            </div>
            <div className="pop_btm">
              <button
                type="button"
                className="btn_cancel btn_gray btn_md_close"
                onClick={() => {
                  setPageNum(1)
                  setDevice([])
                }}
              >
                이전
              </button>
              <button
                type="button"
                className="btn_ok btn_blue btn_md_close"
                onClick={() => {
                  onCloseModal()
                }}
              >
                선택완료
              </button>
            </div>
          </div>
        )}
      </div>
    </Modal>
  )
}

export default ListModal
