import React, { VFC } from 'react'
import { Link } from 'react-router-dom'
import Modal from '..'

interface Props {
  show: boolean
  onCloseModal: () => void
  device: number
}

const GoOutModal: VFC<Props> = ({ show, onCloseModal, device }) => {
  return (
    <Modal onCloseModal={onCloseModal} show={show}>
      <div className="go_out modal_popup">
        <div className="modal_stn">
          <div className="pop_mid">
            <p>
              <em>
                설정값이 있습니다.
                <br />
                이동하시겠습니까?
              </em>
            </p>
          </div>
          <div className="pop_btm">
            <Link to={'/web/manageDevice/list?device=' + device} className="btn_cancel btn_gray_type02 btn_md_close">
              나가기
            </Link>
            <button type="button" className="btn_ok btn_blue_type02 btn_md_close" onClick={onCloseModal}>
              계속하기
            </button>
          </div>
        </div>
      </div>
    </Modal>
  )
}

export default GoOutModal
