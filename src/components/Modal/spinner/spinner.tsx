import React, { useCallback, useEffect, useState, VFC } from 'react'
import ReactLoading from 'react-loading'
import Modal from 'components/Modal'

interface spinnerModalProps {
  show: boolean
  onCloseModal: () => void
  type: any
  color: any
  message: any
}

const Spinner: VFC<spinnerModalProps> = ({ show, onCloseModal, type, color, message }) => {
  return (
    <Modal show={show} onCloseModal={onCloseModal}>
      <div className="save_popup modal_popup">
        <div className="modal_stn">
          <div className="pop_top">
            <h3>전체 배포</h3>
          </div>
          <div className="pop_mid">
            <h2>{message}</h2>
            <h2>창을 닫지 말아주세요.</h2>
            <ReactLoading type={type} color={color} height="100%" width="100%" />
          </div>
        </div>
      </div>
    </Modal>
  )
}

export default Spinner
