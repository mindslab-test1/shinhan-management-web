import React, { VFC } from 'react'
import { Link } from 'react-router-dom'
import Modal from '..'

interface Props {
  show: boolean
  onCloseModal: () => void
  onSelectModal: () => void
  device: number
}

const DeviceResetModal: VFC<Props> = ({ show, onCloseModal, device, onSelectModal }) => {
  return (
    <Modal onCloseModal={onCloseModal} show={show}>
      <div className="go_out modal_popup">
        <div className="modal_stn">
          <div className="pop_mid">
            <p>
              <em>
                설정값이 있습니다.
                <br />
                초기화 하시겠습니까?
              </em>
            </p>
          </div>
          <div className="pop_btm">
            <button className="btn_cancel btn_gray_type02 btn_md_close" type="button" onClick={onCloseModal}>
              취소
            </button>
            <button type="button" className="btn_ok btn_blue_type02 btn_md_close" onClick={onSelectModal}>
              초기화
            </button>
          </div>
        </div>
      </div>
    </Modal>
  )
}

export default DeviceResetModal
