import React, { VFC } from 'react'
import { Link } from 'react-router-dom'
import { icoClose } from 'assets/images/icons'
import Modal from '..'

interface Props {
  show: boolean
  onCloseModal: () => void
  image: string
  extension: string
}

const ImageModal: VFC<Props> = ({ show, onCloseModal, image, extension }) => {
  return (
    <Modal onCloseModal={onCloseModal} show={show}>
      <div className="view_media modal_popup">
        <div className="modal_stn">
          <div className="pop_mid">
            <div className="pop_media">
              <img src={'data:image/' + extension + ';base64,' + image} alt="image13131" />
            </div>
            <button type="button" className="btn_md_close" onClick={onCloseModal}>
              <img src={icoClose} alt="닫기" />
            </button>
          </div>
        </div>
      </div>
    </Modal>
  )
}

export default ImageModal
