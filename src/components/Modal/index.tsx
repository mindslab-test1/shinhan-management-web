import React, { FC, useCallback } from 'react'

interface Props {
  show: boolean
  onCloseModal: () => void
}

const Modal: FC<Props> = ({ children, show, onCloseModal }) => {
  const stopPropagation = useCallback((e) => {
    e.stopPropagation()
  }, [])

  if (!show) {
    return null
  }

  return (
    <>
      <div onClick={onCloseModal}>
        <div onClick={stopPropagation}>
          <div className="modal_wrap">
            <div className="dim" onClick={onCloseModal} />
            {children}
          </div>
        </div>
      </div>
    </>
  )
}

export default Modal
