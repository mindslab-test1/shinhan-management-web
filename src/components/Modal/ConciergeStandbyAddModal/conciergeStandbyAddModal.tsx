import React, { useState, VFC } from 'react'
import { icoSearch } from 'assets/images/icons'
import Modal from 'components/Modal'
import ConciergeStandbyRepository from 'repository/Concierge/conciergeStandbyRepository'

interface conciergeStandbyInfoModalProps {
  show: boolean
  onCloseModal: () => void
}

const ConciergeStandbyInfoModal: VFC<conciergeStandbyInfoModalProps> = ({ show, onCloseModal }) => {
  const [standbyInfo, setStandbyInfo] = useState({
    data: '',
    name: '',
    imageYn: 'Y',
  })

  const [fileName, setFileName] = useState('')

  const addStandby = () => {
    const formData = new FormData()
    formData.append('file', standbyInfo.data, fileName)
    formData.append('name', standbyInfo.name)
    formData.append('imageYn', standbyInfo.imageYn)

    ConciergeStandbyRepository.uploadStandby(formData)
      .then((res) => {
        if (res.data.code !== 0) {
          alert(res.data.message)
        } else {
          alert('대기화면 추가 성공')
        }
      })
      .catch(() => {
        alert('대기화면 추가 실패')
      })
  }

  const handleFileChange = (e: any) => {
    const standbyData = {
      ...standbyInfo,
      data: e.target.files[0],
    }
    setFileName(e.target.files[0].name)
    setStandbyInfo(standbyData)
  }

  const onChangeInput = (e: any) => {
    const standbyData = {
      ...standbyInfo,
      name: e.target.value,
    }

    setStandbyInfo(standbyData)
  }

  const onChangeRadio = (e: any) => {
    const standbyData = {
      ...standbyInfo,
      imageYn: e.target.value,
    }

    setStandbyInfo(standbyData)
  }

  return (
    <Modal onCloseModal={onCloseModal} show={show}>
      <div className="add_waiting modal_popup">
        <div className="modal_stn">
          <div className="pop_top">
            <h3>대기화면 추가</h3>
          </div>
          <div className="pop_mid">
            <div className="mid_cnt">
              <p className="mid_gray_tit">대기화면 명</p>
              <input type="text" name="name" onChange={onChangeInput} className="tooltip_input" />
            </div>
            <div className="mid_cnt">
              <p className="mid_gray_tit">파일 형식</p>
              <div className="btnR_txt_cont mt12">
                <div className="btn_radio_txt">
                  <input
                    type="radio"
                    id="rdImage"
                    name="imageYn"
                    value="Y"
                    checked={standbyInfo.imageYn === 'Y'}
                    onChange={onChangeRadio}
                  />
                  <label htmlFor="rdImage">Image</label>
                </div>
                <div className="btn_radio_txt">
                  <input
                    type="radio"
                    id="rdVideo"
                    name="imageYn"
                    value="N"
                    checked={standbyInfo.imageYn === 'N'}
                    onChange={onChangeRadio}
                  />
                  <label htmlFor="rdVideo">Video</label>
                </div>
              </div>
            </div>
            <div className="mid_cnt">
              <p className="mid_gray_tit">파일</p>
              <div className="filebox type02">
                <input className="upload-name" value={fileName} disabled />
                <input
                  type="file"
                  accept={standbyInfo.imageYn === 'Y' ? 'image/jpg, image/jpeg, image/png' : 'video/*'}
                  id="standbyFile"
                  name="standbyFile"
                  className="upload-hidden"
                  onChange={handleFileChange}
                />
                <label htmlFor="standbyFile" className="btn_blueline_icon">
                  찾기 <img src={icoSearch} alt="돋보기 아이콘" />
                </label>
              </div>
            </div>
          </div>
          <div className="pop_btm">
            <button
              type="button"
              className="btn_cancel btn_gray btn_md_close"
              onClick={() => {
                setStandbyInfo({
                  data: '',
                  name: '',
                  imageYn: 'Y',
                })
                setFileName('')
                onCloseModal()
              }}
            >
              취소
            </button>
            <button
              type="button"
              className="btn_ok btn_blue_type03 btn_md_close"
              onClick={() => {
                addStandby()
                setStandbyInfo({
                  data: '',
                  name: '',
                  imageYn: 'Y',
                })
                setFileName('')
                onCloseModal()
              }}
            >
              추가
            </button>
          </div>
        </div>
      </div>
    </Modal>
  )
}

export default ConciergeStandbyInfoModal
