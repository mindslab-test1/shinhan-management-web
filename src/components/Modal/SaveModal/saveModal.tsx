import qs from 'qs'
import React, { VFC } from 'react'
import { Link, useLocation } from 'react-router-dom'
import Modal from '..'

interface Props {
  show: boolean
  onCloseModal: () => void
  onSubmit: () => void
  branchNo: String
  branchNm: String
}

const SaveModal: VFC<Props> = ({ show, onCloseModal, onSubmit, branchNo, branchNm }) => {
  const location = useLocation()

  const query = qs.parse(location.search, {
    ignoreQueryPrefix: true,
  })

  return (
    <Modal onCloseModal={onCloseModal} show={show}>
      <div className="save_popup modal_popup">
        <div className="modal_stn">
          <div className="pop_top">
            <h3>등록</h3>
          </div>
          <div className="pop_mid">
            <p>
              <em className="color_darkOrange fwR">{branchNm + ' / ' + branchNo}</em> 인공인간 설정을
              <br />
              저장하시겠습니까?
            </p>
          </div>
          <div className="pop_btm">
            <button
              type="button"
              className="btn_cancel btn_gray_type02 btn_md_close"
              onClick={() => {
                onCloseModal()
              }}
            >
              취소
            </button>
            <button
              type="button"
              className="btn_ok btn_blue_type02 btn_md_close"
              onClick={() => {
                onSubmit()
              }}
            >
              저장
            </button>
          </div>
        </div>
      </div>
    </Modal>
  )
}

export default SaveModal
