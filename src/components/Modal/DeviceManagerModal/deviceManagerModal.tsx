import { icoSearch } from 'assets/images/icons'
import ModalPaging from 'components/Pagination/ModalPaging'
import SearchBox from 'components/SearchBox/searchBox'
import PaginationTable from 'components/table/PaginationTable'
import React, { useState, VFC } from 'react'
import Modal from '..'

interface Props {
  show: boolean
  onCloseModal?: () => void
  onSelectModal: (manager: string) => void
  device: number
}

type tableBodyType = {
  id: number
  branchNo: string
  branchName: string
  eMail: string
  name: string
}

const DeviceManagerModal: VFC<Props> = ({ show, onCloseModal = () => {}, device, onSelectModal }) => {
  const [selectedManager, setSelectedManager] = useState<string>()
  const [selectedIndex, setSelectedIndex] = useState<number>()
  const [currentPage, setCurrentPage] = useState<number>(1)
  const [pageLength, setPageLength] = useState<number>(10)

  const headOptions = ['No.', '지점번호', '지점명', 'ID', '이름']
  const bodyOptions: tableBodyType[] = [
    {
      id: 1,
      branchNo: '1111',
      branchName: '도곡',
      eMail: 'asdf@naver.com',
      name: '김김김',
    },
    {
      id: 2,
      branchNo: '2222',
      branchName: '노동청',
      eMail: '52hour@naver.com',
      name: '유유유',
    },
    {
      id: 3,
      branchNo: '3333',
      branchName: '강남',
      eMail: 'psy@naver.com',
      name: '박박박',
    },
    {
      id: 4,
      branchNo: '4444',
      branchName: '판교',
      eMail: 'IT@naver.com',
      name: '최최최',
    },
    {
      id: 5,
      branchNo: '5555',
      branchName: '하남',
      eMail: 'starfield@naver.com',
      name: '정정정',
    },
    {
      id: 6,
      branchNo: '6666',
      branchName: '도봉',
      eMail: 'DB@naver.com',
      name: '배배배',
    },
    {
      id: 7,
      branchNo: '7777',
      branchName: '신사',
      eMail: 'gentleman@naver.com',
      name: '임임임',
    },
    {
      id: 8,
      branchNo: '8888',
      branchName: '신창동',
      eMail: 'newspear@naver.com',
      name: '장장장',
    },
    {
      id: 9,
      branchNo: '9999',
      branchName: '성남',
      eMail: 'angry@naver.com',
      name: '조조조',
    },
    {
      id: 10,
      branchNo: '1234',
      branchName: '수원',
      eMail: 'watersource@naver.com',
      name: '구구구',
    },
  ]
  const head = (
    <tr>
      {headOptions.map((value) => (
        <th>{value}</th>
      ))}
    </tr>
  )
  const body = bodyOptions.map((value, index) => (
    <tr
      className={index === selectedIndex ? 'select' : ''}
      onClick={() => {
        setSelectedManager(value.name)
        setSelectedIndex(index)
      }}
    >
      <td>{value.id}</td>
      <td>{value.branchNo}</td>
      <td>{value.branchName}</td>
      <td>{value.eMail}</td>
      <td>{value.name}</td>
    </tr>
  ))
  return (
    <Modal onCloseModal={onCloseModal} show={show}>
      <div className="device_manager modal_popup">
        <div className="modal_stn">
          <div className="pop_top">
            <h3>기기 담당자 선택</h3>
          </div>
          <div className="pop_mid">
            <form action="">
              <SearchBox
                className="w100p"
                buttonImgAlt="검색 아이콘"
                buttonImgSrc={icoSearch}
                inputClassName="w100p"
                placeholder="기기 담당자 이름을 입력해주세요"
              />
              <PaginationTable
                tableClass="table_manager mt12"
                isScrollable={false}
                heads={head}
                bodys={body}
                colLength={headOptions.length}
              />
              <ModalPaging
                pageLength={pageLength}
                onClickPage={(index) => {
                  setCurrentPage(index)
                }}
                currentPage={currentPage}
              />
            </form>
          </div>
          <div className="pop_btm">
            <button type="button" className="btn_cancel btn_gray btn_md_close" onClick={onCloseModal}>
              취소
            </button>
            <button
              type="button"
              className="btn_ok btn_blue_type03 btn_md_close"
              onClick={() => onSelectModal(selectedManager ?? '')}
            >
              선택완료
            </button>
          </div>
        </div>
      </div>
    </Modal>
  )
}

export default DeviceManagerModal
