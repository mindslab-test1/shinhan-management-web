import React, { useState, VFC, useRef } from 'react'
import { ResponseVideoInfo } from 'typings/commonDTO'
import Modal from '..'

interface Props {
  show: boolean
  onCloseModal: () => void
  responseVideoInfo: ResponseVideoInfo
  url: string
}

const ViewDetailVideoModal: VFC<Props> = ({ show, onCloseModal, responseVideoInfo, url }) => {
  const videoRef = useRef<HTMLVideoElement>(null)
  const [isVideoPlaying, setIsVideoPlaying] = useState(false)

  const onPlayButtonClick = () => {
    setIsVideoPlaying(true)
    ;(videoRef.current as HTMLVideoElement).play()
  }
  const onPauseButtonClick = () => {
    setIsVideoPlaying(false)
    ;(videoRef.current as HTMLVideoElement).pause()
  }
  return (
    <Modal show={show} onCloseModal={onCloseModal}>
      <div className="viewing_detail_video modal_popup">
        <div className="modal_stn">
          <div className="pop_mid">
            <div className="pop_vid">
              <div className="vid_cnt">
                <video src={url} ref={videoRef} onPause={onPauseButtonClick} />
              </div>
              <button
                type="button"
                className={'btn_video btn_play ' + (isVideoPlaying ? '' : 'on')}
                onClick={onPlayButtonClick}
              >
                <span className="ico_img"></span>
                <span className="txt">재&nbsp;&nbsp;생</span>
              </button>
              <button
                type="button"
                className={'btn_video btn_pause ' + (isVideoPlaying ? 'on' : '')}
                onClick={onPauseButtonClick}
              >
                <span className="ico_img"></span>
                <span className="txt">일시정지</span>
              </button>
            </div>
            <div className="pop_vid_script">
              <h4>영상 스크립트</h4>
              <p className="txt">{responseVideoInfo.utter}</p>
              <button type="button" className="btn_cancel btn_gray btn_md_close" onClick={onCloseModal}>
                영상종료
              </button>
            </div>
          </div>
        </div>
      </div>
    </Modal>
  )
}

export default ViewDetailVideoModal
