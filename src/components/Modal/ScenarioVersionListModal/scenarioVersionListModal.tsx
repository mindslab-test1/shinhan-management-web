import React, { useCallback, useEffect, useState, VFC } from 'react'
import { useLocation, useHistory } from 'react-router-dom'
import { useDispatch, useSelector } from 'react-redux'
import { rootState } from 'store'
import { setScenarioVersionInfo } from 'store/module/scenarioVersionReducer'
import qs from 'qs'
import { ScenarioVersionInfo } from 'typings/commonDTO'
import ScenarioVersionRepository from 'repository/scenarioVersionRepository'
import { getTotalPage } from 'utils'
import PaginationTable from 'components/table/PaginationTable'
import Modal from 'components/Modal'
import ModalPaging from 'components/Pagination/ModalPaging'

interface scenarioVersionListModalProps {
  show: boolean
  onCloseModal: () => void
}

const scenarioVersionListModal: VFC<scenarioVersionListModalProps> = ({ show, onCloseModal }) => {
  const [scenarioVersionList, setScenarioVersionList] = useState<ScenarioVersionInfo[]>([
    {
      id: -1,
      version: '',
      created: '',
      deviceType: -1,
    },
  ])

  const history = useHistory()
  const location = useLocation()
  const query = qs.parse(location.search, { ignoreQueryPrefix: true })
  const deviceType = parseInt(query.device as string, 10)
  const dispatch = useDispatch()
  const setScenarioVersionState = (scenarioVersionInfo: ScenarioVersionInfo) =>
    dispatch(setScenarioVersionInfo(scenarioVersionInfo))
  const scenarioVersionInfo = useSelector((state: rootState) => state.scenarioVersionReducer.scenarioVersionInfo)

  const [currentPage, setCurrentPage] = useState(1)
  const [totalPage, setTotalPage] = useState(0)
  const [selectedItem, setSelectedItem] = useState<ScenarioVersionInfo>()

  const getScenarioVersionList = async () => {
    await ScenarioVersionRepository.getScenarioVersionList(currentPage, deviceType)
      .then((res) => {
        if (res.data.result === true) {
          setScenarioVersionList(res.data.data.list)
          setTotalPage(res.data.data.listTotalCnt)
        } else {
          alert(res.data.msg)
        }
      })
      .catch(() => {
        alert('시나리오 버전 목록 가져오기 실패')
      })
  }

  const setScenarioVersionInit = useCallback(
    async (currentPage: number, deviceType: number) => {
      await ScenarioVersionRepository.getScenarioVersionList(currentPage, deviceType)
        .then((res) => {
          if (res.data.result === true) {
            setScenarioVersionList(res.data.data.list)
            setTotalPage(res.data.data.listTotalCnt)
            if (res.data.data.list.length > 0) {
              if (
                scenarioVersionInfo.id === -1 ||
                res.data.data.list
                  .map((data: ScenarioVersionInfo) => {
                    return data.id
                  })
                  .indexOf(scenarioVersionInfo.id) < 0
              ) {
                setScenarioVersionState({ ...res.data.data.list[0], deviceType: deviceType })
              } else {
                setScenarioVersionState({ ...scenarioVersionInfo, deviceType: deviceType })
              }
            } else {
              setScenarioVersionState({ id: -1, version: '', created: '', deviceType: -1 })
            }
          } else {
            alert(res.data.msg)
          }
        })
        .catch(() => {
          alert('시나리오 버전 초기화 실패')
        })
    },
    [deviceType],
  )

  const onPageClick = (index: number) => {
    setCurrentPage(index)
  }

  useEffect(() => {
    setScenarioVersionInit(currentPage, deviceType)
  }, [deviceType])

  const headOptions = ['ID', '버전', '생성일']

  const heads = (
    <tr>
      {headOptions.map((data) => {
        return (
          <th key={data}>
            <span>{data}</span>
          </th>
        )
      })}
    </tr>
  )

  const onSelectButtonClick = () => {
    if (selectedItem) {
      setScenarioVersionState({
        id: selectedItem.id,
        version: selectedItem.version,
        created: selectedItem.created,
        deviceType: selectedItem.deviceType,
      })
      onCloseModal()
    } else {
      alert('버전을 선택해주세요')
    }
  }

  const setBody = useCallback(() => {
    if (scenarioVersionList === undefined || scenarioVersionList === null || scenarioVersionList.length < 1) {
      return [
        <tr key={1}>
          <td colSpan={headOptions.length} className="nodata_td">
            데이터가 없습니다.
          </td>
        </tr>,
      ]
    } else {
      return scenarioVersionList.map((value: ScenarioVersionInfo) => {
        return (
          <tr
            key={value.id}
            onClick={(e) => {
              for (let i = 0; i < e.currentTarget.parentElement.children.length; i += 1) {
                e.currentTarget.parentElement.children[i].className = ''
              }
              e.currentTarget.className = 'select'
              setSelectedItem({
                id: value.id,
                version: value.version,
                created: value.created,
                deviceType: deviceType,
              })
            }}
            className={scenarioVersionInfo.id === value.id ? 'select' : ''}
          >
            <td>{value.id}</td>
            <td className="padlr24">{value.version}</td>
            <td>{value.created}</td>
          </tr>
        )
      })
    }
  }, [scenarioVersionInfo, scenarioVersionList])

  return (
    <Modal onCloseModal={onCloseModal} show={show}>
      <div className="select_scenario_version modal_popup">
        <div className="modal_stn">
          <div className="pop_top">
            <h3>시나리오버전 선택</h3>
          </div>
          <div className="pop_mid">
            <div className="table_cont mt12">
              <PaginationTable
                tableClass="table_scenario_version"
                heads={heads}
                bodys={setBody()}
                isScrollable={false}
                colLength={headOptions.length}
              />
            </div>
            <ModalPaging
              pageLength={getTotalPage(totalPage, 10)}
              currentPage={currentPage}
              onClickPage={(index) => {
                onPageClick(index)
              }}
            />
          </div>
          <div className="pop_btm">
            <button
              type="button"
              className="btn_cancel btn_gray btn_md_close"
              onClick={() => {
                onCloseModal()
              }}
            >
              취소
            </button>
            <button
              type="button"
              className="btn_ok btn_blue_type03 btn_md_close"
              onClick={() => {
                onSelectButtonClick()
              }}
            >
              선택완료
            </button>
          </div>
        </div>
      </div>
    </Modal>
  )
}

export default scenarioVersionListModal
