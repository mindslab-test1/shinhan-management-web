import ModalPaging from 'components/Pagination/ModalPaging'
import React, { useCallback, useEffect, useState, VFC } from 'react'
import { icoSearch } from 'assets/images/icons'
import Modal from 'components/Modal'
import PaginationTable from 'components/table/PaginationTable'
import { useDispatch } from 'react-redux'
import { setBranch } from 'store/module/noticeInfoReducer'
import SelectBox from 'components/SelectBox/selectBox'
import ConciergeDeviceListRepository from 'repository/Concierge/conciergeDeviceListRepository'
import SearchBox from 'components/SearchBox/searchBox'

interface conciergeBranchInfoModalProps {
  show: boolean
  onCloseModal: () => void
}

const ConciergeBranchInfoModal: VFC<conciergeBranchInfoModalProps> = ({ show, onCloseModal }) => {
  type noticeType = '전체' | '지점' | 'UNDEFINED'

  type ConciergeModalListType = {
    deviceNo: string
    branchNo: string
    branchName: string
  }

  type branchInfoType = {
    type: noticeType
    deviceNo: string
    branchNo: string
    branchName: string
  }

  const dispatch = useDispatch()
  const setBranchState = (branchInfo: branchInfoType) => dispatch(setBranch(branchInfo))

  const [item, setItem] = useState<ConciergeModalListType[] | undefined>()
  const [selectedItem, setSelectedItem] = useState<ConciergeModalListType | undefined>()
  const selectOptions = ['지점번호', '지점명']
  const [selectedParam, setSelectedParam] = useState<number>(0)
  const [enteredParam, setEnteredParam] = useState<string>('')
  const [pageLength, setPageLength] = useState<number>(0)
  const [currentPage, setCurrentPage] = useState<number>(1)

  const convertIndexToParam = (paramIndex: number) => {
    switch (paramIndex) {
      case 0:
        return 'branchNo'
      case 1:
        return 'branchName'
      default:
        return null
    }
  }

  const onPressEnter = (e: React.KeyboardEvent<HTMLInputElement>) => {
    if (e.key === 'Enter') {
      e.preventDefault()
      onSearchClick()
    }
  }

  const getPageCnt = async (paramKey?: string, paramValue?: string) => {
    await ConciergeDeviceListRepository.getConciergeDeviceListPage(1, paramKey, paramValue).then((res) => {
      if (res.status === 200) {
        setPageLength(res.data.pageCnt)
      } else {
        alert('페이지 정보를 불러오는데 실패하였습니다.')
      }
    })
  }

  const getPageItem = async (index: number, paramKey?: string, paramValue?: string) => {
    await ConciergeDeviceListRepository.getConciergeDeviceList(index, 1, paramKey, paramValue).then((res) => {
      if (res.status === 200) {
        setItem(res.data.deviceList)
      } else {
        alert('페이지 정보를 불러오는데 실패하였습니다.')
      }
    })
  }

  const onSearchClick = () => {
    setCurrentPage(1)
    getPageItem(
      1,
      enteredParam === '' ? null : convertIndexToParam(selectedParam),
      enteredParam === '' ? null : enteredParam,
    )
    getPageCnt(
      enteredParam === '' ? null : convertIndexToParam(selectedParam),
      enteredParam === '' ? null : enteredParam,
    )
  }

  const onPageClick = (index: number) => {
    getPageItem(
      index,
      enteredParam === '' ? null : convertIndexToParam(selectedParam),
      enteredParam === '' ? null : enteredParam,
    )
    setCurrentPage(index)
  }

  useEffect(() => {
    getPageItem(1)
    getPageCnt()
  }, [show])

  const heads = (
    <tr>
      <th>No.</th> <th>지점번호</th> <th>지점명</th>
    </tr>
  )

  const onSelectButtonClick = useCallback(() => {
    if (selectedItem) {
      setBranchState({
        type: '지점',
        deviceNo: selectedItem.deviceNo,
        branchNo: selectedItem.branchNo,
        branchName: selectedItem.branchName,
      })
    } else alert('지점을 선택해주세요')
    onCloseModal()
  }, [selectedItem])

  const setBody = useCallback(() => {
    if (item === undefined) {
      return [<tr></tr>]
    } else {
      return item.map((value: ConciergeModalListType, index) => {
        return (
          <tr
            onClick={(e) => {
              for (let i = 0; i < e.currentTarget.parentElement.children.length; i += 1) {
                e.currentTarget.parentElement.children[i].className = ''
              }
              e.currentTarget.className = 'select'
              setSelectedItem({
                deviceNo: value.deviceNo,
                branchNo: value.branchNo,
                branchName: value.branchName,
              })
            }}
          >
            <td>{(currentPage - 1) * 10 + index + 1}</td>
            <td>{value.branchNo}</td>
            <td>{value.branchName}</td>
          </tr>
        )
      })
    }
  }, [item])

  return (
    <Modal onCloseModal={onCloseModal} show={show}>
      <div className="select_branch modal_popup">
        <div className="modal_stn">
          <div className="pop_top">
            <h3>지점 선택</h3>
          </div>
          <div className="pop_mid">
            <form action="">
              <div className="search_wrap w100p">
                <SelectBox
                  items={selectOptions}
                  onItemClick={(index) => {
                    setSelectedParam(index)
                  }}
                />
                <SearchBox
                  onInputKeyPress={(e) => onPressEnter(e)}
                  placeholder="지점코드나 지점명을 입력해주세요"
                  onInputChange={(e) => setEnteredParam(e.currentTarget.value)}
                  buttonImgAlt="지점코드나 지점명을 입력해주세요"
                  onButtonClick={onSearchClick}
                />
              </div>
              <div className="table_cont mt12">
                <PaginationTable
                  tableClass="table_branch"
                  heads={heads}
                  bodys={setBody()}
                  isScrollable={false}
                  style={{ height: '520.5px' }}
                  colLength={3}
                />
              </div>
              <ModalPaging
                pageLength={pageLength}
                currentPage={currentPage}
                onClickPage={(index) => {
                  onPageClick(index)
                }}
              />
            </form>
          </div>
          <div className="pop_btm">
            <button
              type="button"
              className="btn_cancel btn_gray btn_md_close"
              onClick={() => {
                onCloseModal()
              }}
            >
              취소
            </button>
            <button
              type="button"
              className="btn_ok btn_blue_type03 btn_md_close"
              onClick={() => {
                onSelectButtonClick()
              }}
            >
              선택완료
            </button>
          </div>
        </div>
      </div>
    </Modal>
  )
}

export default ConciergeBranchInfoModal
