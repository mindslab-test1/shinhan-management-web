export interface selectBoxProps {
  items: string[]
  initialIndex?: number
  disableMenuClick?: boolean
  enableGrayText?: boolean
  onMenuClick?: () => void
  onItemClick?: (index: number) => void
  setOuterIndex?: React.Dispatch<React.SetStateAction<number>>
}

export interface groupSelectBoxProps extends selectBoxProps {
  selectBoxLength: number
  groupMenuState: [boolean[], React.Dispatch<React.SetStateAction<boolean[]>>]
  openIndex: number
}
