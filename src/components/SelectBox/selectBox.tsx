import React, { useState, VFC } from 'react'
import { selectBoxProps } from './selectBoxType'

const SelectBox: VFC<selectBoxProps> = ({
  items,
  onMenuClick,
  onItemClick,
  setOuterIndex,
  disableMenuClick = false,
  enableGrayText = false,
  initialIndex = 0,
}) => {
  const [isMenuOpen, setIsMenuOpen] = useState(false)
  const [index, setIndex] = useState(initialIndex)

  if (setOuterIndex !== undefined) setOuterIndex = setIndex

  return (
    <div className="select_box">
      <div className="custom_select">
        <div
          role="button"
          tabIndex={index}
          className="select-selected "
          onClick={() => {
            if (onMenuClick !== undefined) onMenuClick()
            if (!disableMenuClick) setIsMenuOpen((isMenuOpen) => !isMenuOpen)
          }}
        >
          {enableGrayText ? (
            <span className={index === 0 ? 'color_lightGray' : ''}>{items[index]}</span>
          ) : (
            <span>{items[index]}</span>
          )}
        </div>
        <ul className={'select-items ' + (isMenuOpen ? '' : 'select-hide')}>
          {items.map((itemValue, itemIndex) => {
            return (
              <li
                key={itemValue}
                onClick={() => {
                  if (onItemClick !== undefined) onItemClick(itemIndex)
                  setIsMenuOpen((isMenuOpen) => !isMenuOpen)
                  setIndex(itemIndex)
                }}
              >
                {enableGrayText ? (
                  <span className={itemIndex === 0 ? 'color_lightGray' : ''}>{items[itemIndex]}</span>
                ) : (
                  <span>{items[itemIndex]}</span>
                )}
              </li>
            )
          })}
        </ul>
      </div>
    </div>
  )
}

export default SelectBox
