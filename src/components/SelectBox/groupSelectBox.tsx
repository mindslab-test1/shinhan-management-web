import React, { useEffect, useState, VFC } from 'react'
import { groupSelectBoxProps } from './selectBoxType'

const GroupSelectBox: VFC<groupSelectBoxProps> = ({
  items,
  selectBoxLength,
  onMenuClick,
  onItemClick,
  setOuterIndex,
  groupMenuState,
  disableMenuClick = false,
  enableGrayText = false,
  initialIndex = 0,
  openIndex = 0,
}) => {
  const [isMenuOpen, setIsMenuOpen] = groupMenuState

  const [index, setIndex] = useState<number>(0)

  useEffect(() => {
    setIndex(initialIndex)
  }, [initialIndex])

  if (setOuterIndex !== undefined) setOuterIndex = setIndex

  return (
    <div className="select_box">
      <div className="custom_select">
        <div
          role="button"
          tabIndex={initialIndex}
          className="select-selected "
          onClick={() => {
            if (onMenuClick !== undefined) onMenuClick()
            if (!disableMenuClick) {
              setIsMenuOpen((isMenuOpen) => {
                const newIsMenuOpen = isMenuOpen.slice()
                for (let i = 0; i < selectBoxLength; i += 1) {
                  if (i === openIndex) {
                    newIsMenuOpen[i] = !newIsMenuOpen[openIndex]
                  } else {
                    newIsMenuOpen[i] = false
                  }
                }
                return newIsMenuOpen
              })
            }
          }}
        >
          {enableGrayText ? (
            <span className={index === 0 ? 'color_lightGray' : ''}>{items[index ?? 0]}</span>
          ) : (
            <span>{items[index ?? 0]}</span>
          )}
        </div>
        <ul className={'select-items ' + (isMenuOpen[openIndex] ? '' : 'select-hide')}>
          {items.map((itemValue, itemIndex) => {
            return (
              <li
                key={itemValue}
                onClick={() => {
                  if (onItemClick !== undefined) onItemClick(itemIndex)
                  setIsMenuOpen((isMenuOpen) => {
                    const newIsMenuOpen = isMenuOpen.slice()
                    for (let i = 0; i < selectBoxLength; i += 1) {
                      if (i === openIndex) {
                        newIsMenuOpen[i] = !newIsMenuOpen[openIndex]
                      } else {
                        newIsMenuOpen[i] = false
                      }
                    }
                    return newIsMenuOpen
                  })
                  setIndex(itemIndex)
                }}
              >
                {enableGrayText ? (
                  <span className={index === 0 ? 'color_lightGray' : ''}>{items[itemIndex]}</span>
                ) : (
                  <span>{items[itemIndex]}</span>
                )}
              </li>
            )
          })}
        </ul>
      </div>
    </div>
  )
}

export default GroupSelectBox
