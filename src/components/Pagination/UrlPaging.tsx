import React, { useEffect, useState, VFC } from 'react'

interface PagingProps {
  pageLength: number
  currentPage?: number
  onClickPage: (index: number) => void
}

const UrlPaging: VFC<PagingProps> = React.memo(
  ({ pageLength, currentPage, onClickPage }) => {
    const [pageNum, setPageNum] = useState([1, 2, 3, 4, 5, 6, 7, 8, 9, 10]) // current page indexes
    const [currentIndex, setCurrentIndex] = useState(0) // current page index (where to place number)

    useEffect(() => {
      setPageNum((val) => val.map((_, index) => index + 1))
      setCurrentIndex(0)
    }, [pageLength])

    useEffect(() => {
      setCurrentIndex(currentPage)
      setPageNum((val) => val.map((_, index) => index + 1))
    }, [currentPage])

    const onPageNextClick = () => {
      // when next button clicked and add 10 to every page
      // when there are not enough pages to show, if current page + 10 doesn't exists, set it to last page
      if (pageNum[currentIndex] + 10 > pageLength) {
        onPageLastClick()
      } else {
        setPageNum((val) => {
          return val.map((val) => val + 10)
        })
        onClickPage(pageNum[currentIndex] + 10)
      }
    }

    const onPagePrevClick = () => {
      // if it is first page bundle and clicked prev button
      if (pageNum[currentIndex] <= 10) {
        onClickPage(1)
        setCurrentIndex(0)
      } else {
        onClickPage(pageNum[currentIndex] - 10)
        // if it is not first page bundle
        setPageNum((val) => {
          return val.map((val) => val - 10)
        })
      }
    }
    const onPageFirstClick = () => {
      onClickPage(1)
      setPageNum((val) => {
        return val.map((_, index) => index + 1)
      })
      setCurrentIndex(0)
    }
    const onPageLastClick = () => {
      setPageNum((val) => {
        return val.map((_, index) => Math.floor((pageLength - 1) / 10) * 10 + index + 1)
      })
      onClickPage(pageLength)
      setCurrentIndex(((pageLength % 10) - 1 + 10) % 10)
    }

    const onPageNumberClick = (page: number) => {
      onClickPage(pageNum[page])
      setCurrentIndex(page)
    }

    return (
      <div>
        {pageLength !== 0 && (
          <div className="paging_cont">
            <div className="paging">
              <button
                type="button"
                className={'btn_paging first ' + (pageNum[currentIndex] === 1 ? 'disabled' : '')} // if it is first page bundle (1, 2, ... 9 , 10)
                style={{ cursor: pageNum[currentIndex] === 1 ? 'default' : 'pointer' }}
                onClick={(e) => {
                  if (pageNum[currentIndex] === 1) {
                    e.preventDefault()
                  } else {
                    onPageFirstClick()
                  }
                }}
              >
                <span className="blind">처음</span>
              </button>
              <button
                className={'btn_paging prev ' + (pageNum[currentIndex] === 1 ? 'disabled' : '')}
                type="button"
                style={{ cursor: pageNum[currentIndex] === 1 ? 'default' : 'pointer' }}
                onClick={(e) => {
                  if (pageNum[currentIndex] === 1) {
                    e.preventDefault()
                  } else {
                    onPagePrevClick()
                  }
                }}
              >
                <span className="blind">이전</span>
              </button>
              {pageNum.map((val, index) => {
                if (val > pageLength) {
                  return null
                } else {
                  return (
                    <button
                      type="button"
                      key={val}
                      className={pageNum[currentIndex] === val ? 'on' : ''} // while mapping, if current page's turn(query.index === mapping value), then give effect to number
                      onClick={() => {
                        onPageNumberClick(index)
                      }}
                    >
                      <span>{val}</span>
                    </button>
                  )
                }
              })}
              <button
                type="button"
                className={'btn_paging next ' + (pageNum[currentIndex] === pageLength ? 'disabled' : '')} // currentIndex === 10 && pageNum[pageCount-1] === pageLength => if it is last page
                style={{ cursor: pageNum[currentIndex] === pageLength ? 'default' : 'pointer' }}
                onClick={(e) => {
                  if (pageNum[currentIndex] === pageLength) {
                    e.preventDefault()
                  } else {
                    onPageNextClick()
                  }
                }}
              >
                <span className="blind">다음</span>
              </button>
              <button
                type="button"
                className={'btn_paging last ' + (pageNum[currentIndex] === pageLength ? 'disabled' : '')}
                style={{ cursor: pageNum[currentIndex] === pageLength ? 'default' : 'pointer' }}
                onClick={(e) => {
                  if (pageNum[currentIndex] === pageLength) {
                    e.preventDefault()
                  } else {
                    onPageLastClick()
                  }
                }}
              >
                <span className="blind">끝</span>
              </button>
            </div>
          </div>
        )}
      </div>
    )
  },
  (prev, next) => {
    return prev.pageLength === next.pageLength
  },
)

export default UrlPaging
