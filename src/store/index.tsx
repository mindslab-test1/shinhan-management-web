import { createStore, applyMiddleware } from 'redux'
import { createLogger } from 'redux-logger'
import ReduxThunk from 'redux-thunk'
import { composeWithDevTools } from 'redux-devtools-extension'
import rootReducer from './module/index'

const middleware = [ReduxThunk]
const store = createStore(rootReducer, composeWithDevTools(applyMiddleware(...middleware)))

export default store

export type rootState = ReturnType<typeof rootReducer>
