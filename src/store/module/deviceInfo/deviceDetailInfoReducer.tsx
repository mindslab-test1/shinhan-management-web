import { DeviceListInfo } from 'typings/deviceDTO'

const SET_DEVICE_DETAIL = 'deviceDetailInfoReducer/SET_DEVICE_DETAIL'

interface initProps {
  deviceDetailInfo: DeviceListInfo
}

export const setDeviceInfoDetail = (deviceDetailInfo: DeviceListInfo) => ({
  type: SET_DEVICE_DETAIL,
  payload: {
    deviceDetailInfo: deviceDetailInfo,
  },
})

const initialState: initProps = {
  deviceDetailInfo: {
    avatarId: 1,
    outfitId: 1,
    backgroundId: 1,
    aiHumanId: 1,
    deviceNo: '1',
    branchNo: '1',
    branchNm: '1',
    avatarNm: '',
    outfitNm: '',
    backgroundNm: '',
    manager: '1',
  },
}

export default function deviceDetailInfoReducer(state = initialState, action: any) {
  const { type, payload } = action
  switch (type) {
    case SET_DEVICE_DETAIL:
      return {
        ...state,
        deviceDetailInfo: payload.deviceDetailInfo,
      }
    default:
      return state
  }
}
