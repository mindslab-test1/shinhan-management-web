import { DeviceListInfo } from 'typings/deviceDTO'

const SET_DEVICE_LIST = 'deviceListInfoReducer/SET_DEVICE_LIST'
const SET_DEVICE_CHECKED = 'deviceListInfoReducer/SET_DEVICE_CHECKED'

interface initProps {
  isChecked: boolean[]
  deviceListInfo: DeviceListInfo[]
}

export const setDeviceInfoList = (deviceListInfo: DeviceListInfo[]) => ({
  type: SET_DEVICE_LIST,
  payload: {
    deviceListInfo: deviceListInfo,
  },
})

export const setDeviceInfoChecked = (isChecked: boolean[]) => ({
  type: SET_DEVICE_CHECKED,
  payload: {
    isChecked: isChecked,
  },
})

const initialState: initProps = {
  isChecked: [true],
  deviceListInfo: [
    {
      avatarId: 1,
      outfitId: 1,
      backgroundId: 1,
      aiHumanId: 1,
      deviceNo: '1',
      branchNo: '1',
      branchNm: '1',
      avatarNm: '',
      outfitNm: '',
      backgroundNm: '',
      manager: '1',
    },
  ],
}

export default function deviceListInfoReducer(state = initialState, action: any) {
  const { type, payload } = action
  switch (type) {
    case SET_DEVICE_LIST:
      return {
        ...state,
        deviceListInfo: payload.deviceListInfo,
      }
    case SET_DEVICE_CHECKED:
      return {
        ...state,
        isChecked: payload.isChecked,
      }
    default:
      return state
  }
}
