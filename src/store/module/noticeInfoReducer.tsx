const SET_NOTICE = 'noticeInfoReducer/SET_NOTICE'
const SET_DATE = 'noticeInfoReducer/SET_DATE'
const CHANGE_NOTICE = 'noticeInfoReducer/CHANGE_NOTICE'
const SET_BRANCH = 'noticeInfoReducer/SET_BRANCH'
const SET_INITIAL = 'noticeInfoReducer/SET_INITIAL'

type noticeInfo = {
  id: number
  type: noticeType
  deviceNo: string
  branchNo: string
  branchName: string
  text: string
  isUsing: string
  searchDate: searchDate
  enrollDate: string
  previousNotice: string
  nextNotice: string
}

type searchDate = {
  startDate: Date
  endDate: Date
}

type noticeType = '전체' | '지점' | 'UNDEFINED'

type branchInfo = {
  deviceNo: string
  branchNo: string
  branchName: string
  type: noticeType
}

interface initProps {
  noticeInfo: noticeInfo
}

export const setNoticeInfo = (noticeInfo: noticeInfo) => ({
  type: SET_NOTICE,
  payload: {
    noticeInfo: noticeInfo,
  },
})

export const changeNoticeInfo = (noticeType: noticeType, text: string, isUsing: string) => ({
  type: CHANGE_NOTICE,
  payload: {
    noticeType: noticeType,
    text: text,
    isUsing: isUsing,
  },
})

export const setDate = (searchDate: searchDate) => ({
  type: SET_DATE,
  payload: {
    searchDate: searchDate,
  },
})

export const setBranch = (branchInfo: branchInfo) => ({
  type: SET_BRANCH,
  payload: {
    branchInfo: branchInfo,
  },
})

export const setInitial = () => ({
  type: SET_INITIAL,
})

const initialState: initProps = {
  noticeInfo: {
    id: -1,
    type: 'UNDEFINED',
    deviceNo: '-1',
    branchNo: '-1',
    branchName: 'null',
    text: 'null',
    isUsing: 'N',
    searchDate: {
      startDate: new Date(Date.now()),
      endDate: new Date(Date.now()),
    },
    enrollDate: '1900/01/01',
    previousNotice: 'null',
    nextNotice: 'null',
  },
}

export default function noticeInfoReducer(state = initialState, action: any) {
  const { type, payload } = action
  switch (type) {
    case SET_NOTICE:
      return {
        ...state,
        noticeInfo: payload.noticeInfo,
      }
    case CHANGE_NOTICE:
      return {
        ...state,
        noticeInfo: {
          ...state.noticeInfo,
          noticeType: payload.noticeType,
          text: payload.text,
          isUsing: payload.isUsing,
        },
      }
    case SET_DATE:
      return {
        ...state,
        noticeInfo: {
          ...state.noticeInfo,
          searchDate: payload.searchDate,
        },
      }
    case SET_BRANCH:
      return {
        ...state,
        noticeInfo: {
          ...state.noticeInfo,
          deviceNo: payload.branchInfo.deviceNo,
          branchNo: payload.branchInfo.branchNo,
          branchName: payload.branchInfo.branchName,
          type: payload.branchInfo.type,
        },
      }
    case SET_INITIAL:
      return {
        ...state,
        noticeInfo: {
          id: -1,
          type: 'UNDEFINED',
          deviceNo: '',
          branchNo: '',
          branchName: '',
          text: '',
          isUsing: 'N',
          searchDate: {
            startDate: new Date(Date.now()),
            endDate: new Date(Date.now()),
          },
          enrollDate: '1900/01/01',
          previousNotice: '',
          nextNotice: '',
        },
      }
    default:
      return state
  }
}
