import { GuideList } from 'typings/conciergeDTO'

const SET_BRANCH = 'branchInfoReducer/SET_BRANCH'
const SET_DEVICEID = 'branchInfoReducer/SET_DEVICEID'
const CHANGE_BRANCH = 'branchInfoReducer/CHANGE_BRANCH'
const SET_DETAILINFO = 'branchInfoReducer/SET_DETAILINFO'
const SET_BRANCH_DETAIL_INFO = 'branchInfoReducer/SET_BRANCH_DETAIL_INFO'

type branchInfo = {
  deviceId: number
  branchName: string
  branchNo: string
  deviceNo: string
  guideList: GuideList[]
}

type branchDetailInfo = {
  branchName: string
  branchNo: string
  deviceNo: string
  guideList: GuideList[]
}

interface initProps {
  branchInfo: branchInfo
}

export const setBranchInfo = (branchInfo: branchInfo) => ({
  type: SET_BRANCH,
  payload: {
    branchInfo: branchInfo,
  },
})

export const changeBranchInfo = (branchInfo: branchInfo) => ({
  type: CHANGE_BRANCH,
  payload: {
    branchInfo: branchInfo,
  },
})

export const setDeviceId = (deviceId: number) => ({
  type: SET_DEVICEID,
  payload: {
    deviceId: deviceId,
  },
})

export const setDetailInfo = (detailInfo: { branchName: string; branchNo: string; deviceNo: string }) => ({
  type: SET_DETAILINFO,
  payload: {
    branchName: detailInfo.branchName,
    branchNo: detailInfo.branchNo,
    deviceNo: detailInfo.deviceNo,
  },
})

export const setBranchDetailInfo = (branchDetailInfo: branchDetailInfo) => ({
  type: SET_BRANCH_DETAIL_INFO,
  payload: {
    branchName: branchDetailInfo.branchName,
    branchNo: branchDetailInfo.branchNo,
    deviceNo: branchDetailInfo.deviceNo,
    guideList: branchDetailInfo.guideList,
  },
})

const initialState: initProps = {
  branchInfo: {
    deviceId: -1,
    branchName: '',
    branchNo: '',
    deviceNo: '',
    guideList: [
      {
        id: -1,
        floor: -1,
        image: '',
        imageName: '',
        created: '',
        text: [
          {
            id: -1,
            title: '',
            content: '',
          },
        ],
      },
    ],
  },
}

export default function branchInfoReducer(state = initialState, action: any) {
  const { type, payload } = action
  switch (type) {
    case SET_BRANCH:
      return {
        ...state,
        branchInfo: payload.branchInfo,
      }
    case CHANGE_BRANCH:
      return {
        ...state,
        branchInfo: payload.branchInfo,
      }
    case SET_DEVICEID:
      return {
        ...state,
        branchInfo: {
          ...state.branchInfo,
          deviceId: payload.deviceId,
        },
      }
    case SET_DETAILINFO:
      return {
        ...state,
        branchInfo: {
          ...state.branchInfo,
          branchName: payload.branchName,
          branchNo: payload.branchNo,
          deviceNo: payload.deviceNo,
        },
      }
    case SET_BRANCH_DETAIL_INFO:
      return {
        ...state,
        branchInfo: {
          ...state.branchInfo,
          branchName: payload.branchName,
          branchNo: payload.branchNo,
          deviceNo: payload.deviceNo,
          guideList: payload.guideList,
        },
      }
    default:
      return state
  }
}
