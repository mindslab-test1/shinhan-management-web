const SET_DTBT_INFO = 'videoDistributionReducer/SET_DTBT_INFO'
const SET_DTBT_VIDEO_CNT = 'videoDistributionReducer/SET_DTBT_VIDEO_CNT'

type dtbtInfo = {
  deviceId: number
  branchName: string
  branchNo: number
  deviceNo: number
  scenarioType: number
  totalCnt: number
  cnt: number
  status: number
  dtbtCreated: string
  dtbtVideoCnt: dtbtVideoCnt
}

type dtbtVideoCnt = {
  dtbtWaitCnt: number
  dtbtReqCnt: number
  dtbtIngCnt: number
  dtbtCompCnt: number
}

interface initProps {
  dtbtInfo: dtbtInfo
}

export const setDtbtInfo = (dtbtInfo: dtbtInfo) => ({
  type: SET_DTBT_INFO,
  payload: {
    dtbtInfo: dtbtInfo,
  },
})

export const setDtbtVideoCnt = (dtbtVideoCnt: dtbtVideoCnt) => ({
  type: SET_DTBT_VIDEO_CNT,
  payload: {
    dtbtVideoCnt: dtbtVideoCnt,
  },
})

const initialState: initProps = {
  dtbtInfo: {
    deviceId: -1,
    branchName: '',
    branchNo: -1,
    deviceNo: -1,
    scenarioType: -1,
    totalCnt: -1,
    cnt: -1,
    status: -1,
    dtbtCreated: '',
    dtbtVideoCnt: {
      dtbtWaitCnt: -1,
      dtbtReqCnt: -1,
      dtbtIngCnt: -1,
      dtbtCompCnt: -1,
    },
  },
}

export default function videoDistributionReducer(state = initialState, action: any) {
  const { type, payload } = action
  switch (type) {
    case SET_DTBT_INFO:
      return {
        ...state,
        dtbtInfo: payload.dtbtInfo,
      }
    case SET_DTBT_VIDEO_CNT:
      return {
        ...state,
        dtbtInfo: {
          ...state.dtbtInfo,
          dtbtVideoCnt: payload.dtbtVideoCnt,
        },
      }
    default:
      return state
  }
}
