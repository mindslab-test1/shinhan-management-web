const SET_UPLOAD_INFO = 'videoCreationReducer/SET_UPLOAD_INFO'
const SET_VIDEO_ID = 'videoCreationReducer/SET_VIDEO_ID'

type uploadInfo = {
  excel: File
  scenarioVersion: string
}

interface initProps {
  uploadInfo: uploadInfo
  videoId: number
}

export const setUploadInfo = (excel: File, scenarioVersion: string) => ({
  type: SET_UPLOAD_INFO,
  payload: {
    excel: excel,
    scenarioVersion: scenarioVersion,
  },
})

export const setVideoId = (videoId: number) => ({
  type: SET_VIDEO_ID,
  payload: {
    videoId: videoId,
  },
})

const initialState: initProps = {
  uploadInfo: { excel: undefined, scenarioVersion: '' },
  videoId: -1,
}

export default function videoCreationReducer(state = initialState, action: any) {
  const { type, payload } = action
  switch (type) {
    case SET_UPLOAD_INFO:
      return {
        ...state,
        uploadInfo: {
          ...state.uploadInfo,
          excel: payload.excel,
          scenarioVersion: payload.scenarioVersion,
        },
      }
    case SET_VIDEO_ID:
      return {
        ...state,
        videoId: payload.videoId,
      }
    default:
      return state
  }
}
