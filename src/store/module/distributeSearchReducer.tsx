const SET_PARAM_KEY_INDEX = 'distributeSearchReducer/SET_PARAM_KEY_INDEX'
const SET_PARAM_VALUE = 'distributeSearchReducer/SET_PARAM_VALUE'
const SET_DATE = 'distributeSearchReducer/SET_DATE'

type searchDate = {
  startDate: Date
  endDate: Date
}

interface initProps {
  paramKeyIndex: number
  paramValue: string
  searchDate: searchDate
}

export const setParamKeyIndex = (paramKeyIndex: number) => ({
  type: SET_PARAM_KEY_INDEX,
  payload: {
    paramKeyIndex: paramKeyIndex,
  },
})

export const setParamValue = (paramValue: string) => ({
  type: SET_PARAM_VALUE,
  payload: {
    paramValue: paramValue,
  },
})

export const setDate = (searchDate: searchDate) => ({
  type: SET_DATE,
  payload: {
    searchDate: searchDate,
  },
})

const initialState: initProps = {
  paramKeyIndex: 0,
  paramValue: 'null',
  searchDate: {
    startDate: new Date(Date.now()),
    endDate: new Date(Date.now()),
  },
}

export default function distributeSearchReducer(state = initialState, action: any) {
  const { type, payload } = action
  switch (type) {
    case SET_PARAM_KEY_INDEX:
      return {
        ...state,
        paramKeyIndex: payload.paramKeyIndex,
      }
    case SET_PARAM_VALUE:
      return {
        ...state,
        paramValue: payload.paramValue,
      }
    case SET_DATE:
      return {
        ...state,
        searchDate: payload.searchDate,
      }
    default:
      return state
  }
}
