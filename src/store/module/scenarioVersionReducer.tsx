import { ScenarioVersionInfo } from 'typings/commonDTO'

const SET_SCENARIO_VERSION_INFO = 'scenarioVersionReducer/SET_SCENARIO_VERSION_INFO'

interface initProps {
  scenarioVersionInfo: ScenarioVersionInfo
}

export const setScenarioVersionInfo = (scenarioVersionInfo: ScenarioVersionInfo) => ({
  type: SET_SCENARIO_VERSION_INFO,
  payload: {
    scenarioVersionInfo: scenarioVersionInfo,
  },
})

const initialState: initProps = {
  scenarioVersionInfo: {
    id: -1,
    version: '',
    created: '',
    deviceType: -1,
  },
}

export default function scenarioVersionReducer(state = initialState, action: any) {
  const { type, payload } = action
  switch (type) {
    case SET_SCENARIO_VERSION_INFO:
      return {
        ...state,
        scenarioVersionInfo: payload.scenarioVersionInfo,
      }
    default:
      return state
  }
}
