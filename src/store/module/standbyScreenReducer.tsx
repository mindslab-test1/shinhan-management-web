import { ConciergeStandbyDetailInfo } from 'typings/conciergeDTO'

const SET_SCREEN_INFO = 'standbyScreenReducer/SET_SCREEN_INFO'
const SET_SCREEN_DETAIL_INFOS = 'standbyScreenReducer/SET_SCREEN_DETAIL_INFOS'
const CHANGE_SCREEN_INFO = 'standbyScreenReducer/CHANGE_SCREEN_INFO'

type screenInfo = {
  deviceNumber: number
  branchNumber: number
  branchName: string
}

interface initProps {
  screenInfo: screenInfo
  screenDetailInfo: ConciergeStandbyDetailInfo
}

type screenType = 'VIDEO' | 'IMAGE' | 'UNDEFINED'

export const setScreenInfo = (screenInfo: screenInfo) => ({
  type: SET_SCREEN_INFO,
  payload: {
    screenInfo: screenInfo,
  },
})

export const setScreenDetailInfos = (screenDetailInfo: ConciergeStandbyDetailInfo) => ({
  type: SET_SCREEN_DETAIL_INFOS,
  payload: {
    screenDetailInfo: screenDetailInfo,
  },
})

const initialState: initProps = {
  screenInfo: {
    deviceNumber: -1,
    branchNumber: -1,
    branchName: 'null',
  },
  screenDetailInfo: {
    typeList: [false, false, false],
    standbyList: [],
  },
}

export default function standbyScreenReducer(state = initialState, action: any) {
  const { type, payload } = action
  switch (type) {
    case SET_SCREEN_INFO:
      return {
        ...state,
        screenInfo: payload.screenInfo,
      }
    case SET_SCREEN_DETAIL_INFOS:
      return {
        ...state,
        screenDetailInfo: payload.screenDetailInfo,
      }
    case CHANGE_SCREEN_INFO:
      return {
        ...state,
        screenInfo: payload.screenInfo,
        screenDetailInfo: payload.screenDetailInfo,
      }

    default:
      return state
  }
}
