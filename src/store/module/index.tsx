import { createTransform, persistReducer } from 'redux-persist'
import storage from 'redux-persist/lib/storage'

import { combineReducers } from 'redux'
import branchInfoReducer from './branchInfoReducer'
// import deviceEnrollInfoReducer from './deviceInfo/deviceEnrollInfoReducer'
import deviceDetailInfoReducer from './DeviceInfo/deviceDetailInfoReducer'
import deviceListInfoReducer from './DeviceInfo/deviceListInfoReducer'
import tooltipInfoReducer from './tooltipInfoReducer'
import noticeInfoReducer from './noticeInfoReducer'
import standbyScreenReducer from './standbyScreenReducer'
import videoCreationReducer from './videoCreationReducer'
import videoDistributionReducer from './videoDistributionReducer'
import deviceSearchReducer from './DeviceInfo/deviceSearchReducer'
import distributeSearchReducer from './distributeSearchReducer'
import scenarioVersionReducer from './scenarioVersionReducer'

const replacer = (key: any, value: any) => (value instanceof Date ? value.toISOString() : value)

const reviver = (key: any, value: any) =>
  typeof value === 'string' && value.match(/^\d{4}-\d{2}-\d{2}T\d{2}:\d{2}:\d{2}/) ? new Date(value) : value

export const encode = (toDeshydrate: any) => JSON.stringify(toDeshydrate, replacer)

export const decode = (toRehydrate: string) => JSON.parse(toRehydrate, reviver)

const persistConfig = {
  key: 'root',
  // localStorage에 저장합니다.
  storage,
  transforms: [createTransform(encode, decode)],
}

const rootReducer = persistReducer(
  persistConfig,
  combineReducers({
    deviceListInfoReducer,
    branchInfoReducer,
    deviceDetailInfoReducer,
    // deviceEnrollInfoReducer,
    tooltipInfoReducer,
    noticeInfoReducer,
    standbyScreenReducer,
    videoCreationReducer,
    videoDistributionReducer,
    deviceSearchReducer,
    distributeSearchReducer,
    scenarioVersionReducer,
  }),
)
export default rootReducer
