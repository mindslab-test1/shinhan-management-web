const SET_TOOLTIP = 'tooltipInfoReducer/SET_TOOLTIP'
const CHANGE_TOOLTIP = 'tooltipInfoReducer/CHANGE_TOOLTIP'

interface initProps {
  text: string[]
}

export const setTooltipInfo = (text: string[]) => ({
  type: SET_TOOLTIP,
  payload: {
    text: text,
  },
})

const initialState: initProps = {
  text: [],
}

export default function tooltipInfoReducer(state = initialState, action: any) {
  const { type, payload } = action
  switch (type) {
    case SET_TOOLTIP:
      return {
        ...state,
        text: payload.text,
      }
    default:
      return state
  }
}
