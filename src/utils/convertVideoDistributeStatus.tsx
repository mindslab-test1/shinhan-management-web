const convertVideoDistributeStatus = (status: number) => {
  switch (status) {
    case 0: {
      return '배포 대기중'
    }
    case 1: {
      // 하나라도 배포요청시
      return '배포 요청'
    }
    case 2: {
      // 하나라도 배포중이면
      return '배포중'
    }
    case 3: {
      // 전체 배포가 완료되면
      return '배포 완료'
    }
    default: {
      return ''
    }
  }
}

export default convertVideoDistributeStatus
