const convertVideoAvatarName = (status: string) => {
  switch (status) {
    case 'man': {
      return '남자'
    }
    case 'woman': {
      return '여자'
    }
    default: {
      return ''
    }
  }
}

export default convertVideoAvatarName
