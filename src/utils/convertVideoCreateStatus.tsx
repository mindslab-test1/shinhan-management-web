const convertVideoCreateStatus = (status: number) => {
  switch (status) {
    case 0: {
      return '생성 대기'
    }
    case 1: {
      return '생성 중'
    }
    case 2: {
      return '생성 완료'
    }
    default: {
      return '생성 대기'
    }
  }
}

export default convertVideoCreateStatus
