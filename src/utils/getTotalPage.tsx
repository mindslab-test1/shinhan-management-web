const getTotalPage = (totalCnt: number, perPage: number) => {
  return Math.floor((totalCnt - 1) / perPage + 1)
}

export default getTotalPage
