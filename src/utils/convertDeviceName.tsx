const convertDeviceName = (device: number) => {
  switch (device) {
    case 1: {
      return 'AI 컨시어지'
    }
    case 2: {
      return 'AI 뱅커 디지털데스크'
    }
    case 3: {
      return '스마트 키오스크'
    }
    case 4: {
      return '스마트 카드업무 키오스크'
    }
    default: {
      return ''
    }
  }
}

export default convertDeviceName
