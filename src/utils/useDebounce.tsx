import React, { useEffect, useState } from 'react'

const useDebounce = (
  value: {
    r: number
    g: number
    b: number
    a: number
  },
  delay: number,
  onChange: (color: { r: number; g: number; b: number; a: number }) => void,
) => {
  const [debouncedValue, setDebouncedValue] = useState(value)

  useEffect(() => {
    const timer = setTimeout(() => {
      setDebouncedValue(value)
      onChange(value)
    }, delay)

    return () => {
      clearTimeout(timer)
    }
  }, [value])

  return debouncedValue
}

export default useDebounce
