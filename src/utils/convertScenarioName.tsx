const convertScenarioName = (scenarioType: number, langType: string) => {
  if (langType !== 'en') {
    switch (scenarioType) {
      case 0: {
        return '시나리오'
      }
      case 1: {
        return '시스템'
      }
      default: {
        return ''
      }
    }
  } else {
    switch (scenarioType) {
      case 0: {
        return 'scenario'
      }
      case 1: {
        return 'system'
      }
      default: {
        return ''
      }
    }
  }
}

export default convertScenarioName
