const getPageRowNumber = (row: number, page: number) => {
  return (page - 1) * 10 + 1 + row
}

export default getPageRowNumber
