import React, { VFC } from 'react'
import { Route, Switch, Redirect } from 'react-router'
import DashBoard from 'pages/DashBoard'
import ManageVideoCreate from 'pages/ManageVideoCreate'
import ManageDevice from 'pages/ManageDevice'
import ManageVideoDistribute from 'pages/ManageVideoDistribute'
import ManageConcierge from 'pages/ManageConcierge'
import ManageAiEngine from 'pages/ManageAiEngine'
import ManageAuth from 'pages/ManageAuth'
import Sidebar from 'components/sidebar'

const Web: VFC = () => {
  return (
    <>
      <Sidebar />
      <Switch>
        <Route path="/web/dashBoard" component={DashBoard} />
        <Route path="/web/manageVideoCreate" component={ManageVideoCreate} />
        <Route path="/web/manageDevice" component={ManageDevice} />
        <Route path="/web/manageVideoDistribute" component={ManageVideoDistribute} />
        <Route path="/web/manageConcierge" component={ManageConcierge} />
        <Route path="/web/manageAiEngine" component={ManageAiEngine} />
        <Route path="/web/manageAuth" component={ManageAuth} />
        <Redirect exact path="/web" to="/web/dashBoard" />
      </Switch>
    </>
  )
}

export default Web
