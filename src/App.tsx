import React from 'react'
import { Switch, Route, Redirect } from 'react-router-dom'
import Login from 'pages/Login'
import Web from 'layouts/Web'

const App = () => {
  return (
    <div id="wrap" className="mindslab">
      <Switch>
        <Redirect exact path="/" to="/web" />
        <Route path="/login" component={Login} />
        <Route path="/web" component={Web} />
      </Switch>
    </div>
  )
}

export default App
