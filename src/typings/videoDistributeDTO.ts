export interface VideoDistributeGroupInfo {
  deviceId: number
  branchName: string
  branchNo: number
  deviceNo: number
  scenarioType: number
  totalCnt: number
  cnt: number
  status: number
  dtbtCreated: string
}

export interface ReleasedVideoInfo {
  videoId: number
  scenarioType: number
  intent: string
  utter: string
  avatarDisplayNm: string
  outfitDisplayNm: string
  gestureDisplayNm: string
  size: number
  status: number
  created: string
}
