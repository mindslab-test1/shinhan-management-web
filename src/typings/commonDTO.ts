export interface ResponseVideoInfo {
  id: number
  branchName?: string
  deviceType?: string
  aiHumanUuid?: string
  branchNo?: string
  intent?: string
  utter?: string
  status?: number
  fileName?: string
}

export interface ListEnrollPlace {
  id: number
  code: string
  branchOffice: string
}

export interface ScenarioVersionInfo {
  id: number
  version: string
  created: string
  deviceType: number
}
