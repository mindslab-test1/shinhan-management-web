export interface CreateVideoListInfo {
  videoId: number
  scenarioType: number
  intent: string
  utter: string
  avatarDisplayNm: string
  outfitDisplayNm: string
  gestureDisplayNm: string
  status: number
  size: number
  created: string
  background: string
}

export interface CreateVideoEnrollUploadListInfo {
  id: number
  utter: string
  ttsInputText: string
  intent: string
  gestureNm: string
  scenarioKey: string
}

export interface VideoDetailInfo {
  intent: string
  utter: string
  avatar: number
  avatarDisplayNm: string
  outfit: number
  outfitDisplayNm: string
  gesture: number
  gestureDisplayNm: string
  background: number
  backgroundDisplayNm: string
  ttsInputText: string
  scenarioType: number
  scenarioKey: string
  created: string
  name: string
  size: number
}
