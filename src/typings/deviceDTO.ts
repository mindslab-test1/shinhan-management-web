export interface DeviceListInfo {
  aiHumanId: number
  deviceNo: string
  branchNo: string
  branchNm: string
  avatarId: number
  avatarNm: string
  outfitId: number
  outfitNm: string
  backgroundId: number
  backgroundNm: string
  manager: string
}

export interface DeviceListReturnInfo {
  aiHumanId: number
  avatarId: number
  avatarNm: string
  backgroundId: number
  backgroundNm: string
  branchNm: string
  branchNo: string
  created: string
  deviceId: number
  deviceNo: string
  outfitId: number
  outfitNm: string
  versionNm: string
}

export interface DeviceEnrollInfo {
  avatarId: number
  backgroundId: number
  outfitId: number
  branchNo: string | undefined | null
  deviceNo: string | undefined | null
  deviceType: number
  versionId: number
}
