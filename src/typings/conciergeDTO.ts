export interface ConciergeNoticeInfo {
  id: number
  type: string
  branchName: string
  branchNo: string
  deviceNo: string
  text: string
  isUsing: string
  searchDate: {
    startDate: Date
    endDate: Date
  }
  enrollDate: string
}

export interface ConciergeBranchInfo {
  deviceId: number
  branchName: string
  branchNo: string
  deviceNo: string
  created: string
}

export interface ConciergeStandbyListInfo {
  standbyList: {
    id: number
    branchName: string
    branchNo: string
    deviceNo: string
    imageCnt: number
    videoCnt: number
    created: string
  }[]
}

export interface ConciergeStandbySetInfo {
  deviceNo: string
  branchNo: string
  typeList: boolean[]
  standbyList: {
    name: string
    textR: number
    textG: number
    textB: number
  }[]
}

export interface ConciergeStandbyInfo {
  id: number
  branchName: string
  branchNo: string
  deviceNo: string
  imageCnt: number
  videoCnt: number
  created: string
  typeA: boolean
  typeB: boolean
  typeC: boolean
}

export interface ConciergeStandbyDetailInfo {
  typeList: boolean[]
  standbyList: {
    imageYn: string
    name: string
    textRGB: string
  }[]
}

export interface GuideList {
  id: number
  floor: number
  image: string
  imageName: string
  created: string
  text: {
    id: number
    title: string
    content: string
  }[]
}

export interface ModifyGuideList {
  floor: number
  image: string
  extension: string
  imageName: string
  text: {
    title: string
    content: string
  }[]
}

export interface StandbySetInfo {
  branchNo: string
  deviceNo: string
  typeList: boolean[]
  standbyList: {
    name: string
    textR: number
    textG: number
    textB: number
  }[]
}
