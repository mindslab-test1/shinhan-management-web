export enum VideoStatus {
  FINISHED = 'status_finished',
  RUNNING = 'status_ing',
  ERROR = 'btn_retry',
  WAITING = 'status_wait',
}
