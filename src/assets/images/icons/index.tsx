const icoCalendar = process.env.PUBLIC_URL + '/images/icons/ico_calendar_24px.svg'
const icoChart = process.env.PUBLIC_URL + '/images/icons/ico_chart_20px.svg'
const icoCheck = process.env.PUBLIC_URL + '/images/icons/ico_check_16px.svg'
const icoCheckboxChecked = process.env.PUBLIC_URL + '/images/icons/ico_checkbox_checked.svg'
const icoCheckNotAll = process.env.PUBLIC_URL + '/images/icons/ico_checkbox_not_all.svg'
const icoDashboard = process.env.PUBLIC_URL + '/images/icons/ico_dashboard_20px.svg'
const icoDistribution = process.env.PUBLIC_URL + '/images/icons/ico_distribution_20px.svg'
const icoDistributionActive = process.env.PUBLIC_URL + '/images/icons/ico_distribution_active_20px.svg'
const icoDownBlue = process.env.PUBLIC_URL + '/images/icons/ico_down_arrow_24px_blue.svg'
const icoDown = process.env.PUBLIC_URL + '/images/icons/ico_down_arrow_24px.svg'
const icoEdit = process.env.PUBLIC_URL + '/images/icons/ico_edit_24px.svg'
const icoExcelGreen = process.env.PUBLIC_URL + '/images/icons/ico_excel_green_20px.svg'
const icoExcelGray = process.env.PUBLIC_URL + '/images/icons/ico_excel_gray_20px.svg'
const icoHuman = process.env.PUBLIC_URL + '/images/icons/ico_human_20px.svg'
const icoHumanActive = process.env.PUBLIC_URL + '/images/icons/ico_human_active_20px.svg'
const icoPauseGold = process.env.PUBLIC_URL + '/images/icons/ico_pause_gold_24px.svg'
const icoPauseWhite = process.env.PUBLIC_URL + '/images/icons/ico_pause_white_24px.svg'
const icoLeftDisabled = process.env.PUBLIC_URL + '/images/icons/ico_left_arrow_36px_disabled.svg'
const icoMonitor = process.env.PUBLIC_URL + '/images/icons/ico_monitor_20px.svg'
const icoPlay = process.env.PUBLIC_URL + '/images/icons/ico_play_24px.svg'
const icoPlayGold = process.env.PUBLIC_URL + '/images/icons/ico_play_gold_24px.svg'
const icoPlayWhite = process.env.PUBLIC_URL + '/images/icons/ico_play_white_24px.svg'
const icoRight24px = process.env.PUBLIC_URL + '/images/icons/ico_right_arrow_24px.svg'
const icoRightCopy = process.env.PUBLIC_URL + '/images/icons/ico_right_arrow_36px_copy.svg'
const icoRight36px = process.env.PUBLIC_URL + '/images/icons/ico_right_arrow_36px.svg'
const icoScene = process.env.PUBLIC_URL + '/images/icons/ico_scene_20px.svg'
const icoSearch = process.env.PUBLIC_URL + '/images/icons/ico_search_24px.svg'
const icoSetting = process.env.PUBLIC_URL + '/images/icons/ico_setting_20px.svg'
const icoSlash = process.env.PUBLIC_URL + '/images/icons/ico_slash_24px.svg'
const icoSort = process.env.PUBLIC_URL + '/images/icons/ico_sort_16px.svg'
const icoUpArrowBlue = process.env.PUBLIC_URL + '/images/icons/ico_up_arrow_24px_blue.svg'
const icoUp = process.env.PUBLIC_URL + '/images/icons/ico_up_arrow_24px.svg'
const icoProfile = process.env.PUBLIC_URL + '/images/icons/img_profile_empty_32px.svg'
const icoAlarmOn = process.env.PUBLIC_URL + '/images/icons/ico_alarm_off_20px.svg'
const icoAlarmOff = process.env.PUBLIC_URL + '/images/icons/ico_alarm_on_20px.svg'
const icoArrowBlackDown = process.env.PUBLIC_URL + '/images/icons/ico_arrow_down_black_24px.svg'
const icoArrowBlue = process.env.PUBLIC_URL + '/images/icons/ico_arrow_down_blue_24px.svg'
const icoArrowWhite = process.env.PUBLIC_URL + '/images/icons/ico_arrow_down_white_24px.svg'
const icoArrowNextActive = process.env.PUBLIC_URL + '/images/icons/ico_arrow_next_active_24px.svg'
const icoArrowNextDisabled = process.env.PUBLIC_URL + '/images/icons/ico_arrow_next_disabled_24px.svg'
const icoArrowNext2Acitve = process.env.PUBLIC_URL + '/images/icons/ico_arrow_next2_active_24px.svg'
const icoArrowNext2Disabled = process.env.PUBLIC_URL + '/images/icons/ico_arrow_next2_disabled_24px.svg'
const icoPrevActive = process.env.PUBLIC_URL + '/images/icons/ico_arrow_prev_active_24px.svg'
const icoPrevDisabled = process.env.PUBLIC_URL + '/images/icons/ico_arrow_prev_disabled_24px.svg'
const icoPrev2Active = process.env.PUBLIC_URL + '/images/icons/ico_arrow_prev2_active_24px.svg'
const icoPrev2Disabled = process.env.PUBLIC_URL + '/images/icons/ico_arrow_prev2_disabled_24px.svg'
const icoArrowUpBlue = process.env.PUBLIC_URL + '/images/icons/ico_arrow_up_blue_24px.svg'
const icoChange = process.env.PUBLIC_URL + '/images/icons/ico_change_32px.svg'
const icoChartActive = process.env.PUBLIC_URL + '/images/icons/ico_chart_active_20px.svg'
const icoDashboardActive = process.env.PUBLIC_URL + '/images/icons/ico_dashboard_active_20px.svg'
const icoError = process.env.PUBLIC_URL + '/images/icons/ico_error_32px.svg'
const icoLogout = process.env.PUBLIC_URL + '/images/icons/ico_logout_24px.svg'
const icoMonitorActive = process.env.PUBLIC_URL + '/images/icons/ico_monitor_active_20px.svg'
const icoPause = process.env.PUBLIC_URL + '/images/icons/ico_pause_24px.svg'
const icoSceneActive = process.env.PUBLIC_URL + '/images/icons/ico_scene_active_20px.svg'
const icoSettingActive = process.env.PUBLIC_URL + '/images/icons/ico_setting_active_20px.svg'
const icoUpArrowWhite = process.env.PUBLIC_URL + '/images/icons/ico_up_arrow_white_24px.svg'
const icoUpdate = process.env.PUBLIC_URL + '/images/icons/ico_update_32px.svg'
const icoEye = process.env.PUBLIC_URL + '/images/icons/ico_eye_24px.svg'
const icoClose = process.env.PUBLIC_URL + '/images/icons/ico_close_40px.svg'
const icoPlus = process.env.PUBLIC_URL + '/images/icons/ico_plus_20px.svg'
const icoPlusWhite = process.env.PUBLIC_URL + '/images/icons/ico_plus_white_32px.svg'
const icoMinus = process.env.PUBLIC_URL + '/images/icons/ico_minus_20px.svg'
const icoMinusWhite = process.env.PUBLIC_URL + '/images/icons/ico_minus_white_32px.svg'
const icoDelivery = process.env.PUBLIC_URL + '/images/icons/ico_delivery_24px.svg'
const icoArrowBlackUp = process.env.PUBLIC_URL + '/images/icons/ico_arrow_up_black_36px.svg'

export {
  icoPlus,
  icoPlusWhite,
  icoMinus,
  icoMinusWhite,
  icoClose,
  icoAlarmOn,
  icoAlarmOff,
  icoArrowBlackDown,
  icoArrowBlackUp,
  icoArrowBlue,
  icoArrowWhite,
  icoArrowNextActive,
  icoArrowNextDisabled,
  icoArrowNext2Acitve,
  icoArrowNext2Disabled,
  icoPrevActive,
  icoPrevDisabled,
  icoPrev2Active,
  icoPrev2Disabled,
  icoChange,
  icoChartActive,
  icoDashboardActive,
  icoError,
  icoLogout,
  icoMonitorActive,
  icoPause,
  icoSceneActive,
  icoSettingActive,
  icoUpArrowWhite,
  icoUpdate,
  icoUpArrowBlue,
  icoCalendar,
  icoChart,
  icoCheckNotAll,
  icoCheckboxChecked,
  icoDashboard,
  icoDistribution,
  icoDown,
  icoDownBlue,
  icoEdit,
  icoHuman,
  icoLeftDisabled,
  icoMonitor,
  icoPlay,
  icoProfile,
  icoRight24px,
  icoRightCopy,
  icoScene,
  icoSearch,
  icoSetting,
  icoSlash,
  icoSort,
  icoUp,
  icoRight36px,
  icoPlayGold,
  icoPlayWhite,
  icoHumanActive,
  icoPauseGold,
  icoPauseWhite,
  icoExcelGreen,
  icoExcelGray,
  icoDistributionActive,
  icoCheck,
  icoArrowUpBlue,
  icoEye,
  icoDelivery,
}
