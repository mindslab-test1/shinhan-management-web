const imgClothesMan1 = process.env.PUBLIC_URL + '/images/img_clothes_man_1.jpg'
const imgClothesMan2 = process.env.PUBLIC_URL + '/images/img_clothes_man_2.jpg'
const imgClothesMan3 = process.env.PUBLIC_URL + '/images/img_clothes_man_3.jpg'
const imgClothesMan4 = process.env.PUBLIC_URL + '/images/img_clothes_man_4.jpg'
const imgClothesMan5 = process.env.PUBLIC_URL + '/images/img_clothes_man_5.jpg'
const imgModelMan = process.env.PUBLIC_URL + '/images/img_model_man.jpg'
const imgModelWoman = process.env.PUBLIC_URL + '/images/img_model_woman.jpg'
const imgLogoShinhan = process.env.PUBLIC_URL + '/images/img_logo_shinhan.png'
const imgPreviewFlag = process.env.PUBLIC_URL + '/images/img_preview_flag.png'
const icoLogo = process.env.PUBLIC_URL + '/images/logo.png'

export {
  icoLogo,
  imgClothesMan1,
  imgClothesMan2,
  imgClothesMan3,
  imgClothesMan4,
  imgClothesMan5,
  imgModelMan,
  imgModelWoman,
  imgLogoShinhan,
  imgPreviewFlag,
}
