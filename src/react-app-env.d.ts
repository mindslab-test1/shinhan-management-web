/* eslint-disable no-unused-vars */
/// <reference types="react-scripts" />
declare module '*.mp4' {
  const src: string
  export default src
}
function createRef<T>(): RefObject<T>
interface RefObject<T> {
  // immutable
  readonly current: T | null
}
