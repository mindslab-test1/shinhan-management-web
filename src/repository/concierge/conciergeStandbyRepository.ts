import axios from 'axios'
import { StandbySetInfo } from 'typings/conciergeDTO'

const baseURL = process.env.REACT_APP_BASE_URL + '/v1/concierge/standby'
const instance = axios.create({
  baseURL,
  headers: {
    Pragma: 'no-cache',
    Expires: '0',
  },
})

const config = {
  headers: {
    'Content-Type': 'multipart/form-data',
  },
}

instance.interceptors.request.use(
  (config) => {
    return config
  },
  (error) => {
    return Promise.reject(error)
  },
)

class ConciergeStandbyRepository {
  static getDetail = (id: number) => {
    return instance.get('/detail/' + id.toString())
  }

  static getPages = () => {
    return instance.get(`/list/page`)
  }

  static getStandbyList = (pageNum: number) => {
    return instance.get(`/list/${pageNum}`)
  }

  static uploadStandby = (formData: FormData) => {
    return instance.post(`/upload`, formData, config)
  }

  static getStandbyFileList = (imageYn: string) => {
    return instance.get(`/file/list`, {
      params: {
        imageYn: imageYn,
      },
    })
  }

  static setStandbyList = (standbySetInfo: StandbySetInfo) => {
    return instance.post('/set', standbySetInfo)
  }
}

export default ConciergeStandbyRepository
