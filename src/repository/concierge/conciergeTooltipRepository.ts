import axios from 'axios'

const baseURL = process.env.REACT_APP_BASE_URL + '/v1/concierge/tooltip'
const instance = axios.create({
  baseURL,
  headers: {
    Pragma: 'no-cache',
    Expires: '0',
  },
})

instance.interceptors.request.use(
  (config) => {
    return config
  },
  (error) => {
    return Promise.reject(error)
  },
)

class ConciergeTooltipRepository {
  static getList = () => {
    return instance.get('/list')
  }

  static setList = (list: string[]) => {
    return instance.post('/set', {
      textList: list,
    })
  }
}

export default ConciergeTooltipRepository
