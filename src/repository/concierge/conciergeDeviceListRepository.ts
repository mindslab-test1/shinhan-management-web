import axios from 'axios'

const baseURL = process.env.REACT_APP_BASE_URL + '/v1/device/list'
const instance = axios.create({
  baseURL,
  headers: {
    Pragma: 'no-cache',
    Expires: '0',
  },
})

instance.interceptors.request.use(
  (config) => {
    return config
  },
  (error) => {
    return Promise.reject(error)
  },
)

class ConciergeDeviceListRepository {
  static getConciergeDeviceList = (
    pageNum: number,
    deviceType: number,
    paramKey?: string,
    paramValue?: string,
    releaseDateStart?: string,
    releaseDateEnd?: string,
  ) => {
    return instance.get(`/${pageNum}`, {
      params: {
        deviceType: deviceType,
        ...(!paramKey === false && { paramKey: paramKey }),
        ...(!paramValue === false && { paramValue: paramValue }),
        ...(!releaseDateStart === false && { releaseDateStart: releaseDateStart }),
        ...(!releaseDateEnd === false && { releaseDateEnd: releaseDateEnd }),
      },
    })
  }

  static getConciergeDeviceListPage = (
    deviceType: number,
    paramKey?: string,
    paramValue?: string,
    releaseDateStart?: string,
    releaseDateEnd?: string,
  ) => {
    return instance.get('/page', {
      params: {
        deviceType: deviceType,
        ...(!paramKey === false && { paramKey: paramKey }),
        ...(!paramValue === false && { paramValue: paramValue }),
        ...(!releaseDateStart === false && { releaseDateStart: releaseDateStart }),
        ...(!releaseDateEnd === false && { releaseDateEnd: releaseDateEnd }),
      },
    })
  }
}

export default ConciergeDeviceListRepository
