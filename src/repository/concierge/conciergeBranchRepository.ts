import axios from 'axios'
import { ModifyGuideList } from 'typings/conciergeDTO'

const baseURL = process.env.REACT_APP_BASE_URL + '/v1/concierge/guide'
const instance = axios.create({
  baseURL,
  headers: {
    Pragma: 'no-cache',
    Expires: '0',
  },
})

instance.interceptors.request.use(
  (config) => {
    return config
  },
  (error) => {
    return Promise.reject(error)
  },
)

class ConciergeBranchRepository {
  static getPages = () => {
    return instance.get('/list/page')
  }

  static getBranchList = (list: number) => {
    return instance.get('/list/' + list.toString())
  }

  static getBranchDetail = (deviceId: number) => {
    return instance.get('/detail/' + deviceId.toString())
  }

  static modifyBranchDetail = (guideList: ModifyGuideList[], id: number) => {
    return instance.put('/modify/' + id.toString(), {
      guideList: guideList,
    })
  }
}

export default ConciergeBranchRepository
