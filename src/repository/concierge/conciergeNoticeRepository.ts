import axios from 'axios'

const baseURL = process.env.REACT_APP_BASE_URL + '/v1/concierge/notice'
const instance = axios.create({
  baseURL,
  headers: {
    Pragma: 'no-cache',
    Expires: '0',
  },
})

instance.interceptors.request.use(
  (config) => {
    return config
  },
  (error) => {
    return Promise.reject(error)
  },
)

class ConciergeNoticeRepository {
  static getPages = () => {
    return instance.get('/list/page')
  }

  static getNoticeList = (list: number) => {
    return instance.get('/list/' + list.toString())
  }

  static setEnroll = (enrollData: {
    deviceNo: string
    branchNo: string
    text: string
    branchYn: string
    startDate: string
    endDate: string
    useYn: string
  }) => {
    return instance.post('/set', enrollData)
  }

  static deleteOnly = (id: number) => {
    return instance.delete('/delete/' + id.toString())
  }

  static deleteList = (ids: number[]) => {
    return instance.post('/delete/list', {
      noticeIdList: ids,
    })
  }

  static modifyData = (id: number, text: string, startDate: string, endDate: string, useYn: string) => {
    return instance.put('/modify/' + id.toString(), {
      text: text,
      startDate: startDate,
      endDate: endDate,
      useYn: useYn,
    })
  }
}

export default ConciergeNoticeRepository
