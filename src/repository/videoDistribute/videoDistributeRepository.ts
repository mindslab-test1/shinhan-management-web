import axios from 'axios'

const baseURL = process.env.REACT_APP_BASE_URL + '/v1/dtbt'
const instance = axios.create({
  baseURL,
  headers: {
    Pragma: 'no-cache',
    Expires: '0',
  },
})

// TODO: 나중에 로그인 시 토근 적용시
instance.interceptors.request.use(
  (config) => {
    return config
  },
  (error) => {
    return Promise.reject(error)
  },
)

class VideoDistributeRepository {
  static getDistributeGroupList = (pageNum: number, deviceType: number, scenarioVersionId: number) => {
    return instance.get(`/admin/list/${pageNum}`, {
      params: {
        deviceType: deviceType,
        scenarioVersionId: scenarioVersionId,
      },
    })
  }

  static getDistributeGroupTotalPage = (deviceType: number, scenarioVersionId: number) => {
    return instance.get(`/admin/list/page`, {
      params: {
        deviceType: deviceType,
        scenarioVersionId: scenarioVersionId,
      },
    })
  }

  static getDistributeList = (
    pageNum: number,
    deviceId: number,
    deviceType: number,
    scenarioType: number,
    scenarioVersionId: number,
  ) => {
    return instance.get(`/list/${pageNum}`, {
      params: {
        deviceId: deviceId,
        deviceType: deviceType,
        scenarioType: scenarioType,
        scenarioVersionId: scenarioVersionId,
      },
    })
  }

  static getDistributeTotalPage = (
    deviceId: number,
    deviceType: number,
    scenarioType: number,
    scenarioVersionId: number,
  ) => {
    return instance.get(`/list/page`, {
      params: {
        deviceId: deviceId,
        deviceType: deviceType,
        scenarioType: scenarioType,
        scenarioVersionId: scenarioVersionId,
      },
    })
  }

  // 개별 배포
  static saveIndiDistribution = (
    deviceId: number,
    deviceType: number,
    scenarioType: number,
    dtbtType: number,
    videoId: number,
  ) => {
    return instance.post(`/request`, {
      deviceId: deviceId,
      deviceType: deviceType,
      scenarioType: scenarioType,
      dtbtType: dtbtType,
      videoId: videoId,
    })
  }

  // 기기별 전체 배포
  static saveTotalDistribution = (
    deviceId: number,
    deviceType: number,
    scenarioType: number,
    dtbtType: number,
    scenarioVersionId: number,
  ) => {
    return instance.post(`/request`, {
      deviceId: deviceId,
      deviceType: deviceType,
      scenarioType: scenarioType,
      dtbtType: dtbtType,
      scenarioVersionId: scenarioVersionId,
    })
  }

  // 전체 배포
  static saveTotalAllDistribution = (deviceType: number, dtbtType: number, scenarioVersionId: number) => {
    return instance.post(`/request`, {
      deviceType: deviceType,
      dtbtType: dtbtType,
      scenarioVersionId: scenarioVersionId,
    })
  }

  static getDistributeCountInfo = (
    deviceId: number,
    deviceType: number,
    scenarioType: number,
    scenarioVersionId: number,
  ) => {
    return instance.get(`/stts`, {
      params: {
        deviceId: deviceId,
        deviceType: deviceType,
        scenarioType: scenarioType,
        scenarioVersionId: scenarioVersionId,
      },
    })
  }
}

export default VideoDistributeRepository
