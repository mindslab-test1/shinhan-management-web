import axios from 'axios'

const baseURL = process.env.REACT_APP_BASE_URL + '/v1/video/admin'
const instance = axios.create({
  baseURL,
  headers: {
    Pragma: 'no-cache',
    Expires: '0',
  },
})

// TODO: 나중에 로그인 시 토근 적용시
instance.interceptors.request.use(
  (config) => {
    return config
  },
  (error) => {
    return Promise.reject(error)
  },
)

class VideoCreateRepository {
  static getCreatedGroupByPage = (
    pageNum: number,
    deviceType: number,
    scenarioType: number,
    scenarioVersionId: number,
  ) => {
    return instance.get(`/list/${pageNum}`, {
      params: {
        deviceType: deviceType,
        scenarioType: scenarioType,
        scenarioVersionId: scenarioVersionId,
      },
    })
  }

  static getCreatedGroupTotalPage = (
    deviceType: number,
    scenarioType: number,
    status: number,
    scenarioVersionId: number,
  ) => {
    return instance.get(`/list/page`, {
      params: {
        deviceType: deviceType,
        scenarioType: scenarioType,
        status: status,
        scenarioVersionId: scenarioVersionId,
      },
    })
  }

  static createVideoExcelUpload = (formData: FormData) => {
    return instance.post(`/create`, formData)
  }

  static getVideoDetailInfo = (videoId: number) => {
    return instance.get(`/page/${videoId}`)
  }
}

export default VideoCreateRepository
