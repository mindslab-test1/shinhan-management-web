import axios from 'axios'

const baseURL = process.env.REACT_APP_BASE_URL + '/v1/scenario'
const instance = axios.create({
  baseURL,
  headers: {
    Pragma: 'no-cache',
    Expires: '0',
  },
})

// TODO: 나중에 로그인 시 토근 적용시
instance.interceptors.request.use(
  (config) => {
    return config
  },
  (error) => {
    return Promise.reject(error)
  },
)

class ScenarioVersionRepository {
  static getVideoExcelList = (formData: FormData) => {
    return instance.post(`/view/excel`, formData)
  }

  static getScenarioVersionList = (pageNum: number, deviceType: number) => {
    return instance.get(`/version/list/${pageNum}`, {
      params: {
        deviceType: deviceType,
      },
    })
  }
}

export default ScenarioVersionRepository
