import axios from 'axios'

const baseURL = process.env.REACT_APP_BASE_URL + '/v1'
const instance = axios.create({
  baseURL,
  headers: {
    Pragma: 'no-cache',
    Expires: '0',
  },
})

instance.interceptors.request.use(
  (config) => {
    return config
  },
  (error) => {
    return Promise.reject(error)
  },
)

class AiHumanOutlineRepository {
  static getAvatar = () => {
    return instance.get('/avatar/list')
  }

  static getOutfit = (avatarId: number) => {
    return instance.get('/outfit/list', {
      params: {
        avatarId: avatarId,
      },
    })
  }

  static getBackground = (deviceType: number) => {
    return instance.get('/background/list', {
      params: {
        deviceType: deviceType,
      },
    })
  }
}

export default AiHumanOutlineRepository
