import axios from 'axios'

const baseURL = process.env.REACT_APP_BASE_URL + '/v1/device/'
const instance = axios.create({
  baseURL,
  headers: {
    Pragma: 'no-cache',
    Expires: '0',
  },
})
// instance.defaults.timeout = 30000

// TODO: 나중에 로그인 시 토근 적용시
instance.interceptors.request.use(
  (config) => {
    return config
  },
  (error) => {
    return Promise.reject(error)
  },
)

class DeviceRepository {
  static postHuman = (request: any) => {
    return instance.post('', request)
  }

  static putHuman = (deviceNo: any, request: any) => {
    return instance.put('', deviceNo, request)
  }

  static getHuman = (deviceId: any) => {
    return instance.get('' + deviceId)
  }

  static editHuman = (deviceId: string, request: any) => {
    return instance.put('', deviceId, request)
  }

  static getHumanPreview = (deviceId: string, avatar: string, outfit: string, background: string) => {
    return instance.get('preview/' + deviceId, {
      params: {
        avatar: avatar,
        outfit: outfit,
        background: background,
      },
    })
  }
}

export default DeviceRepository
