import axios from 'axios'
import { DeviceEnrollInfo } from 'typings/deviceDTO'

const baseURL = process.env.REACT_APP_BASE_URL + '/v1/device/admin'
const instance = axios.create({
  baseURL,
  headers: {
    Pragma: 'no-cache',
    Expires: '0',
  },
})

instance.interceptors.request.use(
  (config) => {
    return config
  },
  (error) => {
    return Promise.reject(error)
  },
)

class DeviceListRepository {
  static getDeviceList = (page: number, deviceType: number) => {
    return instance.get(`/list/${page}`, {
      params: {
        deviceType: deviceType,
      },
    })
  }

  static updateDeviceList = (aiHumanId: string, deviceEnrollInfo: DeviceEnrollInfo) => {
    return instance.put(`/update/` + aiHumanId, {
      ...deviceEnrollInfo,
    })
  }

  static enrollDeviceList = (deviceEnrollInfo: DeviceEnrollInfo) => {
    return instance.post(`/create`, {
      ...deviceEnrollInfo,
    })
  }

  // static getDeviceList = (deviceType: number, branchName: string) => {
  //   return instance.get('/device', {
  //     params: {
  //       deviceType: deviceType,
  //       branchName: branchName,
  //     },
  //   })
  // }
}

export default DeviceListRepository
