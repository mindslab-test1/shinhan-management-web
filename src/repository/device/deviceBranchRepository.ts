import axios from 'axios'

const baseURL = process.env.REACT_APP_BASE_URL + '/v1/branch'
const instance = axios.create({
  baseURL,
  headers: {
    Pragma: 'no-cache',
    Expires: '0',
  },
})

instance.interceptors.request.use(
  (config) => {
    return config
  },
  (error) => {
    return Promise.reject(error)
  },
)

class DeviceBranchRepository {
  static getDeviceBranchList = (pageNum: number, filterNm?: string, filterVal?: string) => {
    return instance.get(`/list/${pageNum}`, {
      params: {
        ...(!filterNm === false && { filterNm: filterNm }),
        ...(!filterVal === false && { filterVal: filterVal }),
      },
    })
  }
}

export default DeviceBranchRepository
